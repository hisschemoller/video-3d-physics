# Blender

`tab` - Toggle between Object and Edit Mode.\
`backtick` - Viewpoint menu (Left, Right, Top etc.)\
`space` - Toggle animation playback.\
`/` - Local View\
`e` - Extrude\
`g` - Move\
`k` - Knife tool (+ `a` for angle constraint)\
`l` - Select Linked menu.\
`m` - Merge menu.\
`n` - Transform menu (slide out).\
`p` - Separate menu.\
`r` - Rotate\
`s` - Scale\
`u` - UV Mapping menu.\
`v` - Rip\
`x` - Delete menu.\
`z` - View Mode menu (Wireframe, Solid etc.)\ 
`1 2 3` - Select Mode Vertex, Edge, Plane.

`shift space` - Toolbar menu.\
`shift a` - Mesh menu (Plane, Cube etc.)\
`shift d` - Duplicate.\
`shift g` - Select Similar menu (Material etc.)

`control a` - Apply transform.\
`control a` - Scale.\
`control j` - Join meshes.\
`control m` - Mirror, gevolgd door x|y|z.\
`control p` - Set Parent To... menu.\
`control r` - Loop cut.

`option leftclick` - Loop Select.\
`option n` - Normals menu.\
`option z` - Toggle X-Ray.
