import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { createServer as createViteServer } from 'vite';

const port = 5173;
const __dirname = path.dirname(fileURLToPath(import.meta.url));

async function createServer() {
  const app = express();

  // Create Vite server in middleware mode and configure the app type as
  // 'custom', disabling Vite's own HTML serving logic so parent server
  // can take control
  const vite = await createViteServer({
    server: { middlewareMode: true },
    appType: 'custom',
  });

  // Use vite's connect instance as middleware. If you use your own
  // express router (express.Router()), you should use router.use
  // When the server restarts (for example after the user modifies
  // vite.config.js), `vite.middlewares` is still going to be the same
  // reference (with a new internal stack of Vite and plugin-injected
  // middlewares). The following is valid even after restarts.
  app.use(vite.middlewares);

  app.use(cors());
  app.use(bodyParser.json({ limit: '25mb' }));

  app.get('/', async (req, res, next) => {
    const url = req.originalUrl;
  
    try {
      // 1. Read index.html
      let template = fs.readFileSync(
        path.resolve(__dirname, 'index.html'),
        'utf-8',
      )
  
      // 2. Apply Vite HTML transforms. This injects the Vite HMR client,
      //    and also applies HTML transforms from Vite plugins, e.g. global
      //    preambles from @vitejs/plugin-react
      template = await vite.transformIndexHtml(url, template);
  
      // 3. Load the server entry. ssrLoadModule automatically transforms
      //    ESM source code to be usable in Node.js! There is no bundling
      //    required, and provides efficient invalidation similar to HMR.
      const { render } = await vite.ssrLoadModule('/src/entry-server.ts');
  
      // 4. render the app HTML. This assumes entry-server.js's exported
      //     `render` function calls appropriate framework SSR APIs,
      //    e.g. ReactDOMServer.renderToString()
      const appHtml = await render(url);
  
      // 5. Inject the app-rendered HTML into the template.
      const html = template.replace(`<!--ssr-outlet-->`, appHtml);
  
      // 6. Send the rendered HTML back.
      res.status(200).set({ 'Content-Type': 'text/html' }).end(html);
    } catch (e) {
      // If an error is caught, let Vite fix the stack trace so it maps back
      // to your actual source code.
      vite.ssrFixStacktrace(e);
      next(e);
    }
  });

  app.get('/fs-img', (req, res) => {
    const url = `${req.query.dir}${req.query.img}`;
    res.sendFile(path.resolve(url));
  });

  app.post('/capture', async (req, res) => {
    const { img, frame } = req.body;

    try {
      const f = (`0000${frame}`).slice(-5);

      // get rid of the data:image/png;base64 at the beginning of the file data
      const imageClean = img.split(',')[1];
      const buffer = Buffer.from(imageClean, 'base64');
      fs.writeFile(`rendered/frame_${f}.png`,
      // fs.writeFile(`/Volumes/Samsung_X5/weesperplein3-rendered/frame_${f}.png`,
        buffer.toString('binary'),
        'binary',
        (err) => {
          if (err) {
            console.log('An error occurred: ', err);
            throw err;
          }
        });

      if (frame > 0) {
        process.stdout.moveCursor(0, -1); // up one line
        process.stdout.clearLine(1); // from cursor to end
        console.log('write frame', f);
      }
    } finally {
      res.send();
    }
  });

  app.listen(port, () => {
    console.log('');
    console.log('\x1b[46m%s\x1b[0m', `                                        `);
    console.log('\x1b[46m%s\x1b[0m', `     V3DPh on http://localhost:${port}     `);
    console.log('\x1b[46m%s\x1b[0m', `                                        `);
    console.log('');
  });
}

createServer();
