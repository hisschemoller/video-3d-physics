/* eslint-disable object-curly-newline */
import MainScene from '@app/mainscene';
import { renderBackground, setupCanvasBackground } from '@app/background';
import { createAnimation1 } from './animation1';
import { createAnimation2 } from './animation2';

export interface Updatable {
  update: () => void;
}

export default class Scene extends MainScene {
  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  drawingCanvasContext: CanvasRenderingContext2D;

  updatables: Updatable[] = [];

  constructor() {
    super();

    this.width = 1920;
    this.canvasHeight = 1080;
    this.viewportHeight = 1080;
    this.width3d = 16;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 30;
    this.captureFps = 30;
    this.captureThrottle = 5; // wait 5 raf ticks
    this.captureDuration = 10; // in seconds
    this.clearColor = 0x000033;

    this.drawingCanvasContext = setupCanvasBackground(this.width, this.canvasHeight) as CanvasRenderingContext2D;

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = this.width;
    this.captureCanvas.height = this.viewportHeight;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;
  }

  async create() {
    await super.create();

    this.renderer.setSize(this.width, this.canvasHeight);
    this.renderer.autoClear = false;

    // createAnimation1(this.drawingCanvasContext, this.updatables);
    createAnimation2(this.drawingCanvasContext, this.updatables);

    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    super.updateAsync(time, delta);
  }

  captureImage() {
    this.captureCanvasContext.drawImage(
      this.renderer.domElement, 0, 0, this.width, this.viewportHeight, 0, 0, this.width, this.viewportHeight,
    );
    return this.captureCanvas.toDataURL();
  }

  preRender() {
    this.drawingCanvasContext.clearRect(0, 0, this.width, this.viewportHeight);
    this.updatables.forEach((u) => u.update());
    renderBackground(this.renderer);
  }
}
