import { Lissajous } from './lissajous';
import { Roulette } from './roulette';
import { Updatable } from './scene';

export function createAnimation1(
  drawingCanvasContext: CanvasRenderingContext2D,
  updatables: Updatable[]
) {
  updatables.push(
    new Lissajous({
      ctx: drawingCanvasContext,
      centerX: drawingCanvasContext.canvas.width / 2,
      centerY: drawingCanvasContext.canvas.height / 2,
      amplitudeX: 350,
      amplitudeY: 250,
      a: 6,
      b: 7,
      d: 0.3,
      speed: 0.01,
      strokeStyle: '#00ff00',
      lineWidth: 3,
    })
  );

  updatables.push(
    new Lissajous({
      ctx: drawingCanvasContext,
      centerX: 600,
      centerY: 500,
      amplitudeX: 400,
      amplitudeY: 400,
      a: 1,
      b: 2,
      d: 0.5,
      speed: 0.02,
      strokeStyle: '#ff6633',
      lineWidth: 10,
    })
  );

  updatables.push(
    new Roulette({
      ctx: drawingCanvasContext,
      centerX: 400,
      centerY: 700,
      centerCircleRadius: 100,
      movingCircleRadius: 20,
      distanceFromCenter: 50,
      distanceFromCenterChangeAmount: 100,
      distanceFromCenterChangeSpeed: 0.02,
      rotationSpeed: 0.01,
      xSpeed: 5,
      strokeStyle: '#cc9999',
      lineWidth: 10,
    })
  );
}
