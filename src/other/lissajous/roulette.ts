import { Updatable } from './scene';

const DOUBLE_PI = 2 * Math.PI;

export interface RouletteProps {
  ctx: CanvasRenderingContext2D;
  centerX: number;
  centerY: number;
  centerCircleRadius: number;
  movingCircleRadius: number;
  distanceFromCenter: number; // of the moving circle
  distanceFromCenterChangeAmount: number;
  distanceFromCenterChangeSpeed: number;
  rotationSpeed: number;
  xSpeed?: number;
  strokeStyle: string;
  lineWidth: number;
}

/**
 * https://www.bit-101.com/2017/2022/12/coding-curves-09-roulette-curves/
 */
export class Roulette implements Updatable {
  ctx: CanvasRenderingContext2D;
  centerCircleRadius: number;
  movingCircleRadius: number;
  distanceFromCenter: number;
  distanceFromCenterChange: number;
  distanceFromCenterChangeAmount: number;
  distanceFromCenterChangeSpeed: number;
  rotation: number = 0;
  rotationSpeed: number;
  xSpeed: number;
  strokeStyle: string;
  lineWidth: number;
  cxLocal: number;
  cyLocal: number;
  translateX: number = 0;

  constructor(props: RouletteProps) {
    this.ctx = props.ctx;
    this.cxLocal = props.centerX;
    this.cyLocal = props.centerY;
    this.centerCircleRadius = props.centerCircleRadius;
    this.movingCircleRadius = props.movingCircleRadius;
    this.distanceFromCenter = props.distanceFromCenter;
    this.distanceFromCenterChangeAmount = props.distanceFromCenterChangeAmount;
    this.distanceFromCenterChangeSpeed = props.distanceFromCenterChangeSpeed;
    this.rotationSpeed = props.rotationSpeed;
    this.xSpeed = props.xSpeed??0;

    this.strokeStyle = props.strokeStyle;
    this.lineWidth = props.lineWidth;

    this.distanceFromCenterChange = 0;
    this.rotation = 0;
  }

  update() {
    this.ctx.strokeStyle = this.strokeStyle;
    this.ctx.lineWidth = this.lineWidth;
    this.ctx.beginPath();

    const resolution = 0.01;
    const r1 = this.centerCircleRadius;
    const r2 = this.movingCircleRadius;

    let d = 0;
    if (this.distanceFromCenterChangeAmount) {
      d = this.distanceFromCenter + (this.distanceFromCenterChangeAmount * Math.sin(this.distanceFromCenterChange));
    } else {
      d = this.distanceFromCenter;
    }
    this.distanceFromCenterChange += this.distanceFromCenterChangeSpeed;

    if (this.rotationSpeed) {
      this.rotation += this.rotationSpeed;
      this.rotation = (this.rotation + this.rotationSpeed) % DOUBLE_PI;
    }

    for (let t = 0, n = DOUBLE_PI ; t < n; t += resolution) {
      const x1 = (r1 + r2) * Math.cos(t) - d * Math.cos(((r1 + r2) * t) / r2);
      const y1 = (r1 + r2) * Math.sin(t) - d * Math.sin(((r1 + r2) * t) / r2);

      const angleRadians = Math.atan2(y1 - 0, x1 - 0) - this.rotation;
      const distance = Math.sqrt(x1 ** 2 + y1 ** 2);

      const x = this.cxLocal + Math.sin(angleRadians) * distance;
      const y = this.cyLocal + Math.cos(angleRadians) * distance;

      this.ctx.lineTo(x, y);
    }
    this.ctx.closePath();
    this.ctx.stroke();

    this.cxLocal += this.xSpeed;
  }
}