# Lissajous

* STER Reclame, Lissajous figuren 1981
  * https://www.youtube.com/watch?v=WIgU8jDHqMY
* STER Lissajous figuren
  * https://www.youtube.com/watch?v=nrpTnX_fSpQ
* Bit-101! Coding Curves 04: Lissajous Curves
  * https://www.bit-101.com/2017/2022/11/coding-curves-04-lissajous-curves/

## Animatie 1

```bash
# render, laatste frame is 251
ffmpeg -framerate 30 -start_number 1 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p lissajous1-video.mp4
# video en audio samenvoegen
ffmpeg -i lissajous1-video.mp4 -i lissajous1-audio.wav -vcodec copy lissajous1.mp4
```

## Animatie 2

```bash
# render, laatste frame is 251
ffmpeg -framerate 30 -start_number 1 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p lissajous2-video.mp4
# video en audio samenvoegen
ffmpeg -i lissajous2-video.mp4 -i lissajous2-audio.wav -vcodec copy lissajous2.mp4
```
