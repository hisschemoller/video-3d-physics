import { Lissajous } from './lissajous';
import { Roulette } from './roulette';
import { Updatable } from './scene';

export function createAnimation2(
  drawingCanvasContext: CanvasRenderingContext2D,
  updatables: Updatable[]
) {
  updatables.push(
    new Lissajous({
      ctx: drawingCanvasContext,
      centerX: 400,
      centerY: 400,
      amplitudeX: 300,
      amplitudeY: 200,
      a: 10,
      b: 9,
      d: 0,
      speed: 0.01,
      strokeStyle: '#666600',
      lineWidth: 2,
      heightLFOAmount: 0,
      heightLFOSpeed: 0,
      widthLFOAmount: -20,
      widthLFOSpeed: 0.05,
      xSpeed: 4,
      ySpeed: 1,
    })
  );
  
  updatables.push(
    new Lissajous({
      ctx: drawingCanvasContext,
      centerX: drawingCanvasContext.canvas.width / 2,
      centerY: drawingCanvasContext.canvas.height / 2,
      amplitudeX: 450,
      amplitudeY: 250,
      a: 2,
      b: 3,
      d: 0.8,
      speed: 0.03,
      strokeStyle: '#3399ff',
      lineWidth: 8,
      heightLFOAmount: 200,
      heightLFOSpeed: 0.05,
      widthLFOAmount: -380,
      widthLFOSpeed: 0.06,
    })
  );

  updatables.push(
      new Roulette({
        ctx: drawingCanvasContext,
        centerX: 400,
        centerY: 700,
        centerCircleRadius: 40,
        movingCircleRadius: 20,
        distanceFromCenter: 40,
        distanceFromCenterChangeAmount: 60,
        distanceFromCenterChangeSpeed: 0.05,
        rotationSpeed: 0.08,
        xSpeed: 0,
        strokeStyle: '#cc33cc',
        lineWidth: 14,
      })
    );
}
