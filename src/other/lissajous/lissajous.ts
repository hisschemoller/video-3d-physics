import { Updatable } from './scene';

const DOUBLE_PI = 2 * Math.PI;

export interface LissajousProps {
  ctx: CanvasRenderingContext2D;
  centerX: number;
  centerY: number;
  amplitudeX: number;
  amplitudeY: number;
  a: number;
  b: number;
  d: number;
  speed: number;
  strokeStyle: string;
  lineWidth: number;
  heightLFOAmount?: number;
  heightLFOSpeed?: number;
  widthLFOAmount?: number;
  widthLFOSpeed?: number;
  xSpeed?: number;
  ySpeed?: number;
}

/**
 * https://www.bit-101.com/2017/2022/11/coding-curves-04-lissajous-curves/
 */
export class Lissajous implements Updatable {
  ctx: CanvasRenderingContext2D;
  cxLocal: number;
  cyLocal: number;
  amplitudeX: number;
  amplitudeY: number;
  a: number;
  b: number;
  d: number;
  speed: number;
  strokeStyle: string;
  lineWidth: number;
  heightLFOAmount: number;
  heightLFOSpeed: number;
  heightLFOValue: number;
  widthLFOAmount: number;
  widthLFOSpeed: number;
  widthLFOValue: number;
  xSpeed: number;
  ySpeed: number;

  constructor(props: LissajousProps) {
    this.ctx = props.ctx;
    this.strokeStyle = props.strokeStyle;
    this.lineWidth = props.lineWidth;

    this.cxLocal = props.centerX;
    this.cyLocal = props.centerY;
    this.amplitudeX = props.amplitudeX;
    this.amplitudeY = props.amplitudeY;
    this.a = props.a;
    this.b = props.b;
    this.d = props.d;
    this.speed = props.speed;

    this.heightLFOAmount = props.heightLFOAmount??0;
    this.heightLFOSpeed = props.heightLFOSpeed??0;
    this.heightLFOValue = 0;
    this.widthLFOAmount = props.widthLFOAmount??0;
    this.widthLFOSpeed = props.widthLFOSpeed??0;
    this.widthLFOValue = 0;
  
    this.xSpeed = props.xSpeed??0;
    this.ySpeed = props.ySpeed??0;
  }

  update() {
    this.ctx.strokeStyle = this.strokeStyle;
    this.ctx.lineWidth = this.lineWidth;
    this.ctx.beginPath();

    let h = this.amplitudeY;
    if (this.heightLFOAmount !== 0 && this.heightLFOSpeed !== 0) {
      h += (this.heightLFOAmount * Math.sin(this.heightLFOValue));
    }
    this.heightLFOValue += this.heightLFOSpeed;

    let w = this.amplitudeX;
    if (this.widthLFOAmount !== 0 && this.widthLFOSpeed !== 0) {
      w += (this.widthLFOAmount * Math.sin(this.widthLFOValue));
    }
    this.widthLFOValue += this.widthLFOSpeed;

    const resolution = 0.01;
    for (let t = 0; t < DOUBLE_PI; t += resolution) {
      const x = this.cxLocal + Math.sin(this.a * t + this.d) * w;
      const y = this.cyLocal + Math.sin(this.b * t) * h;
      this.ctx.lineTo(x, y);
    }
    this.ctx.closePath();
    this.ctx.stroke();

    this.d += this.speed;
    this.cxLocal += this.xSpeed;
    this.cyLocal += this.ySpeed;
  }
}
