import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import MainScene from '@app/mainscene';
import { Timeline } from '@app/timeline';

export interface ProjectSettings {
  height: number;
  height3d: number;
  scene3d: MainScene;
  isPreview: boolean;
  measures: number;
  patternDuration: number;
  previewScale: number;
  stepDuration: number;
  timeline: Timeline;
  width: number;
  width3d: number;
}

export interface MediaData {
  height: number;
  width: number;
}

export interface ImageData extends MediaData {
  imgSrc: string;
}

export interface VideoData extends MediaData {
  fps: number;
  imgSrcPath: string;
}

export interface MediaLibrary {
  [key: string]: ImageData | VideoData;
}

export interface TextureLibrary {
  [key: string]: THREE.Texture;
}

export interface GltfLibrary {
  [key: string]: GLTF;
}
