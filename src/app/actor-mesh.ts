import { ExtendedMesh, THREE } from 'enable3d';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader';
import { getMatrix4 } from '@app/utils';

export const BASE_COLOR = 0xffffff; // 0xD6C49C;

/**
 * Create 3D box.
 */
export function createRectangle(
  width: number,
  height: number,
  texture : THREE.Texture,
  depth = 0.02,
  color: number | string | 'transparant' = BASE_COLOR,
) {
  const geometry = new THREE.BoxGeometry(width, height, depth);

  // move registration point to left top corner
  geometry.translate(width * 0.5, height * -0.5, 0);
  geometry.groups.forEach((group, index) => {
    // set index so that only the front has index 1
    /* eslint-disable no-param-reassign */
    group.materialIndex = index === 4 ? 1 : 0;
  });

  const isTransparant = color === 'transparant';
  const colOrTrans = isTransparant ? {} : { color };
  const materials = [
    new THREE.MeshPhongMaterial({
      ...colOrTrans,
      opacity: isTransparant ? 0 : 1,
      transparent: true,
      side: THREE.FrontSide,
    }),
    new THREE.MeshPhongMaterial({
      map: texture,
      opacity: 1,
      transparent: true,
      side: THREE.FrontSide,
      shininess: 0,
    }),
  ];
  const mesh = new ExtendedMesh(geometry, materials);
  return mesh;
}

/**
 * Create ExtrudeGeometry from Shape.
 */
function createMeshFromShape(
  shape: THREE.Shape,
  scale: number,
  depth: number,
  color: number | string | 'transparant',
  materialType: 'basic' | 'phong' = 'phong',
  texture?: THREE.Texture,
) {
  const isTransparant = color === 'transparant';
  const colOrTrans = isTransparant ? {} : { color };
  const surface = texture ? { map: texture } : colOrTrans;
  const materials: THREE.Material[] = [];

  if (materialType === 'phong') {
    materials.push(new THREE.MeshPhongMaterial({
      ...colOrTrans,
      flatShading: false,
      opacity: isTransparant ? 0 : 1,
      side: THREE.BackSide,
      transparent: true,
    }));
    materials.push(new THREE.MeshPhongMaterial({
      ...surface,
      flatShading: false,
      opacity: 1,
      side: THREE.BackSide,
      transparent: true,
    }));
  } else {
    materials.push(new THREE.MeshBasicMaterial({
      ...colOrTrans,
      opacity: isTransparant ? 0 : 1,
      side: THREE.BackSide,
      transparent: true,
    }));
    materials.push(new THREE.MeshBasicMaterial({
      ...surface,
      opacity: 1,
      side: THREE.BackSide,
      transparent: true,
    }));
  }

  const geometry = new THREE.ExtrudeGeometry(shape, {
    bevelEnabled: false,
    depth,
  });
  geometry.groups.forEach((group, index) => {
    group.materialIndex = index === 0 ? 1 : 0;
  });
  geometry.applyMatrix4(getMatrix4({
    sx: scale,
    sy: scale * -1,
  }));
  geometry.computeVertexNormals();
  const mesh = new ExtendedMesh(geometry, materials);
  return mesh;
}

/**
 * Convert regular Mesh from a Blender model into an ExtendedMesh
 */
export function createMeshFromModel(
  model: THREE.Mesh,
  texture?: THREE.Texture,
) {
  model.material = new THREE.MeshPhongMaterial({ map: texture });
  model.material.needsUpdate = true;
  return model as ExtendedMesh;
}

/**
 * Create Mesh from points.
 */
export function createMeshFromPoints(
  points: [number, number][],
  texture?: THREE.Texture,
  depth: number = 0.02,
  color: number = BASE_COLOR,
  material?: 'basic' | 'phong',
) {
  const shape = new THREE.Shape();
  shape.moveTo(points[0][0], points[0][1]);
  for (let i = 1; i < points.length; i += 1) {
    shape.lineTo(points[i][0], points[i][1]);
  }
  const mesh = createMeshFromShape(shape, 1, depth, color, material, texture);
  return mesh;
}

/**
 * Load SVG file and create extrude from it.
 */
export function createSVG(
  svgUrl: string,
  svgScale: number,
  texture?: THREE.Texture,
  depth: number = 0.02,
  color: number | string | 'transparant' = BASE_COLOR,
  material?: 'basic' | 'phong',
) {
  return new Promise<ExtendedMesh>(
    (resolve, reject) => {
      new SVGLoader().load(
        svgUrl,
        (data) => {
          const { paths } = data;
          paths.forEach((path) => {
            const shapes = SVGLoader.createShapes(path);
            if (shapes.length > 0) {
              const shape = shapes[0];
              const mesh = createMeshFromShape(shape, svgScale, depth, color, material, texture);
              resolve(mesh);
            }
          });
        },
        () => {},
        () => {
          reject();
        },
      );
    },
  );
}
