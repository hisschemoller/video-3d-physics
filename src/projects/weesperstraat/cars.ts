/* eslint-disable object-curly-newline */
import { Scene3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import createTween from '@app/tween';
import Car, { CarProps } from './car';

const SCALE = 1.2;
const Z_TO_RIGHT = 0;
const CAR_Y = 1;
const NUM_STEPS = 30;
let cars: Car[] = [];

const carData: { [prop: string]: CarProps } = {
  black: {
    axisPositionBack: -1.17,
    axisPositionFront: 1.55,
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/weesperstraat/car-black-body-texture.jpg',
    gltf: undefined,
    physicsBox: { width: 2, height: 1, depth: 4.6, y: 0.6, z: 0.15 },
    position: new THREE.Vector3(0, CAR_Y, Z_TO_RIGHT),
    rotationY: Math.PI / 2,
    scale: 1 * SCALE,
    wheelAxisHeight: 0.05,
    wheelDiameter: 0.65,
    wheelHalfTrack: 0.88,
    wheelScale: 0.9,
    wheelTexturePath: '../assets/projects/weesperstraat/car-black-wheel-texture.jpg',
  },
  red: {
    axisPositionBack: -1.7,
    axisPositionFront: 1.55,
    bodyMass: 800,
    bodyTexturePath: '../assets/projects/weesperstraat/car-red-body-texture.jpg',
    gltf: undefined,
    physicsBox: { width: 2.2, height: 1, depth: 4.8, y: 0.6, z: 0.0 },
    position: new THREE.Vector3(0, CAR_Y, Z_TO_RIGHT),
    rotationY: Math.PI / 2,
    scale: 0.8 * SCALE,
    wheelAxisHeight: 0,
    wheelDiameter: 0.721,
    wheelHalfTrack: 1.05,
    wheelScale: 0.9,
    wheelTexturePath: '../assets/projects/weesperstraat/car-red-wheel-texture.jpg',
  },
  white: {
    axisPositionBack: -1.45,
    axisPositionFront: 1.5,
    bodyMass: 800,
    bodyTexturePath: '../assets/projects/weesperstraat/car-white-body-texture.jpg',
    gltf: undefined,
    physicsBox: { width: 2.3, height: 1, depth: 4.45, y: 0.6, z: 0.1 },
    position: new THREE.Vector3(0, CAR_Y, Z_TO_RIGHT),
    rotationY: Math.PI / 2,
    scale: 0.75 * SCALE,
    wheelAxisHeight: 0,
    wheelDiameter: 0.71,
    wheelHalfTrack: 0.99,
    wheelScale: 1,
    wheelTexturePath: '../assets/projects/weesperstraat/car-white-wheel-texture.jpg',
  },
  blue: {
    axisPositionBack: -1.4,
    axisPositionFront: 1.65,
    bodyMass: 1500,
    bodyTexturePath: '../assets/projects/weesperstraat/car-blue-body-texture.jpg',
    gltf: undefined,
    physicsBox: { width: 1.6, height: 1.5, depth: 4.5, y: 0.8, z: 0.0 },
    position: new THREE.Vector3(0, CAR_Y, Z_TO_RIGHT),
    rotationY: Math.PI / 2,
    scale: 1.15 * SCALE,
    wheelAxisHeight: 0,
    wheelDiameter: 0.56,
    wheelHalfTrack: 0.75,
    wheelScale: 0.95,
    wheelTexturePath: '../assets/projects/weesperstraat/car-blue-wheel-texture.jpg',
  },
  orange: {
    axisPositionBack: -3.2,
    axisPositionFront: 2.7,
    bodyMass: 800,
    bodyTexturePath: '../assets/projects/weesperstraat/car-orange-body-texture.jpg',
    gltf: undefined,
    physicsBox: { width: 4, height: 3, depth: 7.6, y: 1.5, z: -0.1 },
    position: new THREE.Vector3(0, CAR_Y, Z_TO_RIGHT),
    rotationY: Math.PI / 2,
    scale: 0.4 * SCALE,
    wheelAxisHeight: 0,
    wheelDiameter: 1.2,
    wheelHalfTrack: 1.8,
    wheelScale: 1.0,
    wheelTexturePath: '../assets/projects/weesperstraat/car-orange-wheel-texture.jpg',
  },
};

export async function createCars(
  projectSettings: ProjectSettings,
) {
  const { patternDuration, scene3d, timeline } = projectSettings;

  carData.black.gltf = await scene3d.load.gltf('../assets/projects/weesperstraat/car-black.glb');
  carData.red.gltf = await scene3d.load.gltf('../assets/projects/weesperstraat/car-red.glb');
  carData.white.gltf = await scene3d.load.gltf('../assets/projects/weesperstraat/car-white.glb');
  carData.blue.gltf = await scene3d.load.gltf('../assets/projects/weesperstraat/car-blue.glb');
  carData.orange.gltf = await scene3d.load.gltf('../assets/projects/weesperstraat/car-orange.glb');
  const carDataArray = Object.values(carData);

  let previousProgress = -1;
  let currentProgress = -1;

  // const d = carDataArray[4];
  // d.position.z = 3;
  // const c = new Car(scene3d as unknown as Scene3D);
  // await c.createCar(d);
  // cars.push(c);
  // return;

  const tween = createTween({
    delay: 2,
    duration: patternDuration,
    ease: 'linear',
    onStart: () => {},
    onUpdate: async (progress: number) => {
      previousProgress = currentProgress;
      currentProgress = Math.floor(progress * NUM_STEPS);
      if (currentProgress !== previousProgress) {
        const isFromLeft = currentProgress % 2 === 0;
        const isOuterLane = currentProgress % 4 <= 1;
        const carIndex = (
          Math.floor(currentProgress / 2) + (isFromLeft ? 0 : 2)) % carDataArray.length;
        const data = carDataArray[carIndex];
        data.position.x = isFromLeft ? -17 : 17;
        // eslint-disable-next-line no-nested-ternary
        data.position.z = isFromLeft ? (isOuterLane ? 2.6 : -3) : (isOuterLane ? -14 : -8.8);
        data.rotationY = isFromLeft ? Math.PI / 2 : Math.PI / -2;
        const car = new Car(scene3d as unknown as Scene3D);
        await car.createCar(data);
        cars.push(car);
      }
    },
    onComplete: () => {},
  });

  timeline.add(tween);
}

export function updateCars() {
  cars = cars.reduce((accumulator, car) => {
    car.update();
    if (car.getY() < -10) {
      car.destroy();
      return [...accumulator];
    }
    return [...accumulator, car];
  }, [] as Car[]);
}
