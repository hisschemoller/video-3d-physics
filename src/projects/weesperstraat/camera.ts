/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-param-reassign */
import { THREE } from 'enable3d';
import createTween from '@app/tween';
import { ProjectSettings } from '@app/interfaces';

// eslint-disable-next-line prefer-const
let isCameraPaused = false;

function moveCamera(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  const DELAY = 0.2;
  const DURATION = patternDuration / 8;
  const DISTANCE = 20;

  timeline.add(createTween({
    delay: DELAY + (DURATION * 0),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.position.x = DISTANCE * progress;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 2),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.position.x = DISTANCE * (1 - progress);
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 4),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.position.x = -DISTANCE * progress;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 6),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.position.x = -DISTANCE * (1 - progress);
    },
    onComplete: () => {},
  }));
}

function moveCamera2(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  const tween = createTween({
    delay: 2,
    duration: patternDuration,
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.position.x = Math.sin(progress * Math.PI * 2) * 18;
    },
    onComplete: () => {},
  });
  timeline.add(tween);
}

function rotateCamera(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  const DELAY = 2;
  const DURATION = patternDuration / 4;
  const ROTATION = Math.PI * -0.1;

  timeline.add(createTween({
    delay: DELAY + (DURATION * 0),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.rotation.y = ROTATION * progress;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 1),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.rotation.y = ROTATION * (1 - progress);
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 2),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.rotation.y = -ROTATION * progress;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 3),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      if (isCameraPaused) {
        return;
      }

      cameraGroup.rotation.y = -ROTATION * (1 - progress);
    },
    onComplete: () => {},
  }));
}

function rotateCamera2(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  const tween = createTween({
    delay: 2,
    duration: patternDuration,
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = Math.sin(progress * Math.PI * 2) * 0.8;
    },
    onComplete: () => {},
  });
  timeline.add(tween);
}

export default function animateCamera(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
) {
  const { scene } = projectSettings;

  const cameraGroup = new THREE.Group();
  cameraGroup.add(camera);
  cameraGroup.position.set(0, 0, 0);
  scene.add(cameraGroup);

  moveCamera2(projectSettings, cameraGroup);
  // rotateCamera2(projectSettings, cameraGroup);
}
