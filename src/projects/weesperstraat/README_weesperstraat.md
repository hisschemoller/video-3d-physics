# Weesperstraat

Weesperstraat 2 2022-08-19.mov<br>
1920 x 1080<br>
0:55

```bash
# extract frame 595 as an image
ffmpeg -i 'Weesperstraat 2 2022-08-19.mov' -vf "select=eq(n\,599)" -vframes 1 'Weesperstraat 2 2022-08-19 frame 600.png'
# perspective correct 1920 x 1080
ffmpeg -hide_banner -i 'Weesperstraat 2 2022-08-19.mov' -lavfi "perspective=x0=88:y0=0:x1=1843:y1=0:x2=0:y2=1054:x3=1920:y3=1080:interpolation=linear" weesperstraat-2-perspective.mov
# extract frame 595 as an image
ffmpeg -i 'weesperstraat-2-perspective.mov' -vf "select=eq(n\,599)" -vframes 1 'weesperstraat-2-perspective_frame_500.png'
# convert to png sequence
ffmpeg -i weesperstraat-2-perspective.mov '/Volumes/Samsung_X5/weesperstraat-2/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i weesperstraat-2-perspective.mov -vf scale=480:270 weesperstraat-2_preview.mov
# convert preview to png sequence
ffmpeg -i weesperstraat-2_preview.mov '/Volumes/Samsung_X5/weesperstraat-2/frames_preview/frame_%05d.png'
```

Weesperstraat 3 2022-08-19.mov<br>
1920 x 1080<br>
2:42

```bash
# extract frame 600 as an image
ffmpeg -i 'Weesperstraat 3 2022-08-19.mov' -vf "select=eq(n\,594)" -vframes 1 'Weesperstraat 3 2022-08-19 frame 595.png'
# perspective correct 1920 x 1080
ffmpeg -hide_banner -i 'Weesperstraat 3 2022-08-19.mov' -lavfi "perspective=x0=128:y0=0:x1=1812:y1=0:x2=0:y2=1080:x3=1920:y3=1080:interpolation=linear" weesperstraat-3-perspective.mov
# extract frame 595 as an image
ffmpeg -i 'weesperstraat-3-perspective.mov' -vf "select=eq(n\,599)" -vframes 1 'weesperstraat-3-perspective_frame_595.png'
# convert to png sequence
ffmpeg -i weesperstraat-3-perspective.mov '/Volumes/Samsung_X5/weesperstraat-3/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i weesperstraat-3-perspective.mov -vf scale=480:270 weesperstraat-3_preview.mov
# convert preview to png sequence
ffmpeg -i weesperstraat-3_preview.mov '/Volumes/Samsung_X5/weesperstraat-3/frames_preview/frame_%05d.png'
```

* Black car
* Red car
* White car
* Blue van
* Silver car
* Orange car

* Enable3d raycast vehicle:
  * https://enable3d.io/examples/raycast-vehicle.html
  * https://github.com/enable3d/enable3d-website/blob/master/src/examples/raycast-vehicle.html
* Vehicle physics with Cannon.js
  * https://discourse.threejs.org/t/vehicle-physics-with-cannon-js/11769
  * https://jblaha.art/sketchbook/0.4/
  * https://github.com/swift502/Sketchbook
* Car modelling
  * Low Poly Vehicles | Easy Beginner | Blender Tutorial
    * https://www.youtube.com/watch?v=Zkg7Ol2jEjs
    * Tot 20:53 // 4:27
  * Car in Blender - PART I - Modelling [ Beginners ]
    * https://www.youtube.com/watch?v=VGPvxIrobFE
  * Modeling a LOW POLY NISSAN GT-R R35 | Blender 2.83 Complete Tutorial
    * https://www.youtube.com/watch?v=5cM_lhRmiQI
* Texture
  * Texture Painting - Quick Start Guide - Blender (een hamer)
    * https://www.youtube.com/watch?v=WjS_zNQNVlw
  * Blender Beginner UV Unwrapping Tutorial (Chair Part 7)
    * https://www.youtube.com/watch?v=XeBUfMKKZDo


### Video 1, de chaos versie

256 16de noten in 16 maten
256 / 30 = 8.533333333333333 16de per stap.
1 patroon duurt 32.82051282051282 seconden.
1 stap is 32.82051282051282 / 30 = 1.094017094017094 seconden.

De muziek voor video 1, de chaos versie, duurt 6 patronen, dus 6 * 16 maten lang, en daarvoor nog
twee seconden intro voor de camera animatie begint.

Een patroon duurt 32.82051282051282 seconden.
Het geheel dus 2 + (32.82051282051282 * 6) = 198.9230769230769 seconden.
Bij 30 FPS zijn dat 198.9230769230769 * 30 = 5967.692307692307 frames.

De beste render was versie 2, `weesperstraat-chaos-rendered-2`. Maar die duurde met 8 patronen te lang, dus gebruik ik er alleen de eerste 6 patronen van, en dat is de folder `weesperstraat-chaos-rendered-2-5968-frames`.

```bash
# png to mp4 (from index 0 met 30 FPS
ffmpeg -framerate 30 -start_number 0 -i /Volumes/Samsung_X5/weesperstraat-chaos-rendered-2-5968-frames/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperstraat-chaos-4-video-x1.mp4
# video en audio samenvoegen
ffmpeg -i weesperstraat-chaos-video-x1.mp4 -i weesperstraat-chaos-audio-x1.wav -vcodec copy weesperstraat-chaos-x1.mp4
# scale to 50%, 960 * 540
ffmpeg -i weesperstraat-chaos-x1.mp4 -vf scale=960:540 weesperstraat-chaos-x1_halfsize.mp4
```

5968 frames lang.
5968 / 30 fps = 198.93333333333334 sec.
3:18.93333333333334 

### Video 2, de oranje auto's versie

Versie `weesperstraat-orange-rendered-3-5908-frames` is de beste. De eerste 5 frames overslaan want daar staan de stukjes bomen tekening onhandig los op.

 ```bash  
# png to mp4 (from index 5 met 30 FPS
ffmpeg -framerate 30 -start_number 5 -i /Volumes/Samsung_X5/weesperstraat-orange-rendered-3-5908-frames/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperstraat-orange-2-video-x1.mp4
# video en audio samenvoegen
ffmpeg -i weesperstraat-orange-2-video-x1.mp4 -i weesperstraat-orange-2-audio-x1.wav -vcodec copy weesperstraat-orange-x1.mp4
# scale to 50%, 960 * 540
ffmpeg -i weesperstraat-orange-x1.mp4 -vf scale=960:540 weesperstraat-orange-x1_halfsize.mp4
 ```

5908 - 4 = 5904 frames lang.
5904 / 30 fps = 196.8 sec.
3:16.8  

### Video 3, de bedoelde versie 

Drie keer gerenderd. De patronen gaan nooit precies hetzelfde.
Van de langste 4982 frames video passen links nummers 1 en 4 het beste.
Rechts alles wat minder.
De andere twee renders hebben dezelfde volgorde van dingen dus die maken niet uit.
Loop 1 frame_00801
Loop 4 frame_03754

#### Volledige video

```bash
# png to mp4 (from index 0 met 30 FPS
ffmpeg -framerate 30 -start_number 0 -i /Volumes/Samsung_X5/weesperstraat-rendered-4982-frames/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperstraat-video-x1-4982-frames.mp4
```

#### Loop video

```bash
# png to mp4 (from index 801 met 30 FPS
ffmpeg -framerate 30 -start_number 801 -i /Volumes/Samsung_X5/weesperstraat-rendered-loop/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperstraat-video-x1.mp4
# repeat 2 times, 2953 frames, video only
ffmpeg -i weesperstraat-3-video-x1.mp4 -filter_complex "loop=loop=2:size=2953:start=0" weesperstraat-3-video-x3.mp4
# take second half of the 2 loop audio file
ffmpeg -ss 00:01:38.43333333333334 -i weesperstraat-3-bitwig-render.wav -c copy -t 00:01:38.43333333333334 weesperstraat-3-audio-x1.wav
## check audio duration
ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 weesperstraat-3-bitwig-render-stretched.wav
ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 weesperstraat-3-audio-x1.wav
# repeat 2 times, audio 
ffmpeg -stream_loop 2 -i weesperstraat-3-audio-x1.wav -c copy weesperstraat-3-audio-x3.wav
# video en audio samenvoegen
ffmpeg -i weesperstraat-3-video-x3.mp4 -i weesperstraat-3-audio-x3.wav -vcodec copy weesperstraat-3-x3.mp4
# scale to 50%, 960 * 540
ffmpeg -i weesperstraat-3-x3.mp4 -vf scale=960:540 weesperstraat-3-x3_halfsize.mp4
```
ffmpeg -i dirk-koy-trip-zwischen-realer-und-virtueller-welt-video.mp4 -i dirk-koy-trip-zwischen-realer-und-virtueller-welt-audio.mp4 -vcodec copy dirk-koy-trip-zwischen-realer-und-virtueller-welt.mp4
2953 frames lang.
2953 / 30 fps = 98.43333333333334 sec.
1:38.43333333333334

98.433356
