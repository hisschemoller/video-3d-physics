import { ExtendedObject3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import Vehicle from './vehicle';

function createWheel(gltf: GLTF, texturePath: string) {
  const wheel = (gltf.scene.getObjectByName('Wheel') as ExtendedObject3D).clone();
  const wheelTexture = new THREE.TextureLoader().load(texturePath);
  wheelTexture.flipY = false;
  wheel.material = new THREE.MeshPhongMaterial({
    map: wheelTexture,
    shininess: 0,
  });
  wheel.receiveShadow = true;
  wheel.castShadow = true;
  wheel.geometry.center();
  return wheel;
}

export type CarProps = {
  axisPositionBack: number;
  axisPositionFront: number;
  bodyMass: number;
  bodyTexturePath: string;
  gltf: GLTF | undefined;
  physicsBox: { [property: string]: any },
  position: THREE.Vector3;
  rotationY: number;
  scale?: number;
  wheelAxisHeight: number;
  wheelDiameter: number;
  wheelHalfTrack: number;
  wheelScale?: number;
  wheelTexturePath: string;
};

export default class Car extends Vehicle {
  async createCar({
    axisPositionBack,
    axisPositionFront,
    bodyMass,
    bodyTexturePath,
    gltf,
    physicsBox,
    position,
    rotationY,
    scale = 1,
    wheelAxisHeight,
    wheelDiameter,
    wheelHalfTrack,
    wheelScale = 1,
    wheelTexturePath,
  }: CarProps) {
    if (!gltf) {
      return;
    }

    // BODY
    const body = (gltf.scene.getObjectByName('Body') as ExtendedObject3D).clone();
    const bodyTexture = new THREE.TextureLoader().load(bodyTexturePath);
    bodyTexture.flipY = false;
    body.material = new THREE.MeshPhongMaterial({
      map: bodyTexture,
      shininess: 1,
    });
    body.receiveShadow = true;
    body.castShadow = true;
    body.position.copy(position);
    body.rotation.y = rotationY;
    body.scale.set(scale, scale, scale);
    this.scene3d.add.existing(body);
    this.physics.add.existing(body, {
      compound: [{
        mass: bodyMass, shape: 'box', ...physicsBox,
      }],
    });

    // WHEEL
    const wScale = wheelScale * scale;
    const wheel = createWheel(gltf, wheelTexturePath);
    wheel.scale.set(wScale, wScale, wScale);

    const wheelRadius = (wheelDiameter / 2) * wScale;
    super.create(body, wheel,
      wheelRadius, wheelRadius, axisPositionBack * scale, axisPositionFront * scale,
      wheelHalfTrack * scale, wheelHalfTrack * scale,
      wheelAxisHeight * scale, wheelAxisHeight * scale);

    // const FRONT_LEFT = 0;
    // const FRONT_RIGHT = 1;
    const BACK_LEFT = 2;
    const BACK_RIGHT = 3;
    let engineForce = 0;
    const maxEngineForce = 4;
    engineForce = maxEngineForce / 4;
    this.vehicle.applyEngineForce(engineForce, BACK_LEFT);
    this.vehicle.applyEngineForce(engineForce, BACK_RIGHT);

    setTimeout(() => {
      this.isCreated = true;
    }, 100);
  }

  getY() {
    return this.chassis.position.y;
  }
}
