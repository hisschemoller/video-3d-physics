/* eslint-disable object-curly-newline */
import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import { getMatrix4 } from '@app/utils';
// import carPhysicsExample from './car-physics-enable3d';
// import carRaycastExample from './car-raycast-enable3d';
import createWalls, { createGround, createSky } from './background';
import { createCars, updateCars } from './cars';
import { createTweenGroup } from './actor';
import animateCamera from './camera';

const PROJECT_PREVIEW_SCALE = 0.25;
const BPM = 117;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 16;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  constructor() {
    super();

    this.width = 1920;
    this.height = 1080;
    this.width3d = 16;
    this.height3d = (this.height / this.width) * this.width3d;
    this.fps = 15;
    this.captureFps = 30;
    this.captureThrottle = 10;
    this.captureDuration = PATTERN_DURATION * 6;
    this.clearColor = 0x71a1db;
    this.shadowSize = 10;
  }

  async create() {
    await super.create();

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();

    this.renderer.autoClear = false;

    // CAMERA & ORBIT_CONTROLS
    this.cameraTarget.set(0, 2.3, 0);
    this.pCamera.position.set(0, 0, 10);
    this.pCamera.lookAt(this.cameraTarget);
    this.pCamera.updateProjectionMatrix();
    this.orbitControls.target = this.cameraTarget;
    this.orbitControls.update();
    this.orbitControls.saveState();

    // AMBIENT LIGHT
    this.ambientLight.intensity = 0.6;

    // DIRECTIONAL LIGHT
    const dirLightIntensity = 0.55;
    const dirLightTarget = new THREE.Object3D();
    dirLightTarget.position.x = -10;
    this.scene.add(dirLightTarget);
    this.directionalLight.target = dirLightTarget;
    this.directionalLight.position.set(10 - 10, 20, 10);
    this.directionalLight.color.setHSL(37 / 360, 1, 0.96);
    this.directionalLight.intensity = dirLightIntensity; // 1.1;

    // DIRECTIONAL LIGHT 2
    const SHADOW_MAP_SIZE = 1024;
    const SHADOW_FAR = 13500;
    const dirLightTarget2 = new THREE.Object3D();
    dirLightTarget2.position.x = 10;
    this.scene.add(dirLightTarget2);
    const directionalLight2 = new THREE.DirectionalLight(0xffffff, dirLightIntensity);
    directionalLight2.target = dirLightTarget2;
    directionalLight2.position.set(10 + 10, 20, 10);
    directionalLight2.color.setHSL(37 / 360, 1, 0.96);
    directionalLight2.castShadow = true;
    directionalLight2.shadow.mapSize.width = SHADOW_MAP_SIZE;
    directionalLight2.shadow.mapSize.height = SHADOW_MAP_SIZE;
    directionalLight2.shadow.camera.left = -this.shadowSize;
    directionalLight2.shadow.camera.right = this.shadowSize;
    directionalLight2.shadow.camera.top = this.shadowSize;
    directionalLight2.shadow.camera.bottom = -this.shadowSize;
    directionalLight2.shadow.camera.far = SHADOW_FAR;
    this.scene.add(directionalLight2);
    // const shadowCamHelper2 = new THREE.CameraHelper(directionalLight2.shadow.camera);
    // this.scene.add(shadowCamHelper2);

    // TWEENS
    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    // MEDIA
    const media = {
      video2: {
        fps: 30,
        height: 1080,
        scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
        width: 1920,
        imgSrcPath: isPreview
          ? '../assets/projects/weesperstraat/weesperstraat-2-frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/weesperstraat-2/frames/&img=frame_#FRAME#.png',
      },
      video3: {
        fps: 30,
        height: 1080,
        scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
        width: 1920,
        imgSrcPath: isPreview
          ? '../assets/projects/weesperstraat/weesperstraat-3-frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/weesperstraat-3/frames/&img=frame_#FRAME#.png',
      },
      image2: {
        height: 1080,
        imgSrc: '../assets/projects/weesperstraat/Weesperstraat 2 frame_00142.png',
        width: 1920,
      },
      image3: {
        height: 1080,
        imgSrc: '../assets/projects/weesperstraat/Weesperstraat 3 frame_04512.png',
        width: 1920,
      },
      gebouwen: {
        height: 1024,
        imgSrc: '../assets/projects/weesperstraat/gebouwen.jpg',
        width: 1024,
      },
      tekeningen: {
        height: 1024,
        imgSrc: '../assets/projects/weesperstraat/tekeningen.jpg',
        width: 1024,
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.height,
      height3d: this.height3d,
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene: this.scene,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    // GROUP
    const group = createTweenGroup(projectSettings);
    group.setStaticPosition(getMatrix4({ rx: 0.23 }));

    createSky(projectSettings);
    await createGround(projectSettings);
    await createWalls(projectSettings, media, group.getGroup());
    await createCars(projectSettings);
    animateCamera(projectSettings, this.pCamera);

    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    updateCars();
    super.updateAsync(time, delta);
  }
}
