/* eslint-disable object-curly-newline */
import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import { getMatrix4 } from '@app/utils';
import { createTweenGroup } from '@app/actor';
import createWalls, { createGround, createSky } from './background';
import { createCars, updateCars } from './cars';
import animateCamera from './camera';

const PROJECT_PREVIEW_SCALE = 0.25;
const BPM = 120;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 5;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  constructor() {
    super();

    this.width = 1080;
    this.canvasHeight = 1920;
    this.viewportHeight = 1920;
    this.width3d = 9;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 15;
    this.captureFps = 25;
    this.captureThrottle = 10;
    this.captureDuration = PATTERN_DURATION * 3;
    this.clearColor = 0x71a1db;
    this.shadowSize = 32;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = this.width;
    this.captureCanvas.height = this.viewportHeight;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;
  }

  /**
   * Set the html viewport and canvas height. For adjusted horizon height.
   */
  adjustHorizon() {
    const canvasContainer = document.getElementById('canvas-container');
    const canvas = this.renderer.domElement;

    if (canvasContainer && canvas) {
      canvasContainer.style.height =  `${(this.viewportHeight / this.width) * 100}vw`;
      canvas.setAttribute('style', `${(this.canvasHeight / this.viewportHeight) * 100}% !important`);
    }
  }

  async create() {
    await super.create();

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();

    this.renderer.autoClear = false;

    // CAMERA & ORBIT_CONTROLS
    this.cameraTarget.set(0, 2.3, 0);
    this.pCamera.position.set(0, 0, 10);
    this.pCamera.lookAt(this.cameraTarget);
    this.pCamera.updateProjectionMatrix();
    this.orbitControls.target = this.cameraTarget;
    this.orbitControls.update();
    this.orbitControls.saveState();

    // AMBIENT LIGHT
    this.ambientLight.intensity = 0.4 * Math.PI;

    // DIRECTIONAL LIGHT
    const dirLightIntensity = 0.8 * Math.PI;
    const dirLightTarget = new THREE.Object3D();
    this.scene.add(dirLightTarget);
    this.directionalLight.target = dirLightTarget;
    this.directionalLight.position.set(10, 20, 10);
    this.directionalLight.color.setHSL(37 / 360, 1, 0.96);
    this.directionalLight.intensity = dirLightIntensity; // 1.1;

    // TWEENS
    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    // MEDIA
    const media = {
      video2: {
        fps: 30,
        height: 1080,
        scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
        width: 1920,
        imgSrcPath: isPreview
          ? '../assets/projects/weesperstraat/weesperstraat-2-frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/weesperstraat-2/frames/&img=frame_#FRAME#.png',
      },
      video3: {
        fps: 30,
        height: 1080,
        scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
        width: 1920,
        imgSrcPath: isPreview
          ? '../assets/projects/weesperstraat/weesperstraat-3-frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/weesperstraat-3/frames/&img=frame_#FRAME#.png',
      },
      image2: {
        height: 1080,
        imgSrc: '../assets/projects/weesperstraat/Weesperstraat 2 frame_00142.png',
        width: 1920,
      },
      image3: {
        height: 1080,
        imgSrc: '../assets/projects/weesperstraat/Weesperstraat 3 frame_04512.png',
        width: 1920,
      },
      gebouwen: {
        height: 1024,
        imgSrc: '../assets/projects/weesperstraat/gebouwen.jpg',
        width: 1024,
      },
      tekeningen: {
        height: 1024,
        imgSrc: '../assets/projects/weesperstraat/tekeningen.jpg',
        width: 1024,
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.width * (9 / 16),
      height3d: this.width3d * (9 / 16),
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene: this.scene,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    // GROUP
    const group = createTweenGroup(projectSettings);
    group.setStaticPosition(getMatrix4({ rx: 0.23 }));

    createSky(projectSettings);
    await createGround(projectSettings);
    await createWalls(projectSettings, media, group.getGroup());
    await createCars(projectSettings);
    animateCamera(projectSettings, this.pCamera);

    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    updateCars();
    super.updateAsync(time, delta);
  }
}
