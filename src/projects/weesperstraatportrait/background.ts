/* eslint-disable object-curly-newline */
import { ExtendedObject3D, THREE } from 'enable3d';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import { createActor } from '@app/actor';

async function createBomen2Achter(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 4.5;
  const actor = await createActor(projectSettings, media.video2, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/bomen2achter.svg' },
    imageRect: { w: 723, h: 513 },
    depth: 0.005,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({
    x: -10, y: 7, z: -50, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 10,
    fromImagePosition: new THREE.Vector2(704, 288),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createBomen3Achter(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 4.5;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/bomen3achter.svg' },
    imageRect: { w: 721, h: 318 },
    depth: 0.005,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({
    x: -30, y: 3, z: -35, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 70,
    fromImagePosition: new THREE.Vector2(284, 550),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createBoom2Links(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 4;
  const actor = await createActor(projectSettings, media.video2, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/boom2links.svg' },
    imageRect: { w: 223, h: 571 },
    depth: 0.005,
    color: 0xdec5a9,
  });
  actor.setStaticPosition(getMatrix4({
    x: -30, y: 13.5, z: -19, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 2,
    fromImagePosition: new THREE.Vector2(0, 233),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createBoom2Links2(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 4;
  const actor = await createActor(projectSettings, media.video2, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/boom2links2.svg' },
    imageRect: { w: 351, h: 536 },
    depth: 0.005,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({
    x: -32, y: 10, z: -24, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 20,
    fromImagePosition: new THREE.Vector2(100, 270),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createBoom3Links2(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 4;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/boom3links2.svg' },
    imageRect: { w: 377, h: 490 },
    depth: 0.005,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({
    x: -30, y: 7, z: -29, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 7,
    fromImagePosition: new THREE.Vector2(0, 379),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createBoom3Rechts(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 3.8;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/boom3rechts.svg' },
    imageRect: { w: 371, h: 625 },
    depth: 0.005,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({
    x: 4, y: 12, z: -26, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 50,
    fromImagePosition: new THREE.Vector2(1049, 268),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createBrugrailing(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, scene3d, stepDuration } = projectSettings;
  const scale = 2.8;
  const actor = await createActor(projectSettings, media.video2, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/brugrailing.svg' },
    imageRect: { w: 1726, h: 129 },
    depth: 0.005,
    color: 0xdec5a9,
  });
  actor.getMesh().geometry.computeBoundingBox();

  if (actor.getMesh().geometry.boundingBox) {
    const bb = actor.getMesh().geometry.boundingBox as THREE.Box3;
    const x = (bb.max.x - bb.min.x) / -2;
    const y = (bb.max.y - bb.min.y) / 2;
    const z = (bb.max.z - bb.min.z) / -2;
    // actor.setStaticPosition(getMatrix4({ x, y, z, sx: scale, sy: scale }));
    actor.setStaticPosition(getMatrix4({ x, y, z }));
  }

  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 0,
    fromImagePosition: new THREE.Vector2(0, 673),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  const wallExObj = new ExtendedObject3D();
  wallExObj.add(actor.getMesh());
  wallExObj.scale.set(scale, scale, scale);
  wallExObj.position.set(-4.2, 0, -18);
  wallExObj.rotation.copy(group.rotation);
  scene3d.scene.add(wallExObj);
  scene3d.physics.add.existing(wallExObj, { mass: 0, shape: 'convex' });
}

async function createGebouwen(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const settings = { depth: 0.005, color: 0xdec5a9 };

  {
    const s = 2.5;
    const linksOnder = await createActor(projectSettings, media.gebouwen, {
      svg: { scale: svgScale, url: '../assets/projects/weesperstraat/gebouw-links-zij.svg' },
      imageRect: { w: 170, h: 621 },
      ...settings,
    });
    linksOnder.setStaticPosition(getMatrix4({
      x: -30, y: 12, z: -17, ry: Math.PI * 0.35, sx: s, sy: s }));
    linksOnder.setStaticImage(0, 4);
    group.add(linksOnder.getMesh());
  }

  {
    const s = 1.7;
    const onder = await createActor(projectSettings, media.gebouwen, {
      svg: { scale: svgScale, url: '../assets/projects/weesperstraat/gebouw-links-onder.svg' },
      imageRect: { w: 456, h: 336 },
      ...settings,
    });
    onder.setStaticPosition(getMatrix4({ x: -36, y: -1, z: -19, sx: s, sy: s }));
    onder.setStaticImage(0, 688);
    group.add(onder.getMesh());
  }

  {
    const s = 2.35;
    const onder = await createActor(projectSettings, media.gebouwen, {
      svg: { scale: svgScale, url: '../assets/projects/weesperstraat/gebouw-links-voor.svg' },
      imageRect: { w: 282, h: 653 },
      ...settings,
    });
    onder.setStaticPosition(getMatrix4({ x: -35.3, y: 12, z: -17, sx: s, sy: s }));
    onder.setStaticImage(174, 4);
    group.add(onder.getMesh());
  }

  {
    const s = 2.3;
    const onder = await createActor(projectSettings, media.gebouwen, {
      svg: { scale: svgScale, url: '../assets/projects/weesperstraat/gebouw-rechts.svg' },
      imageRect: { w: 488, h: 1012 },
      ...settings,
    });
    onder.setStaticPosition(getMatrix4({ x: 23.8, y: 14, z: -18, sx: s, sy: s }));
    onder.setStaticImage(536, 3);
    group.add(onder.getMesh());
  }
}

async function createGevels2Rechts(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 4;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/gevels2rechts.svg' },
    imageRect: { w: 355, h: 348 },
    depth: 0.005,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({
    x: 8, y: 5, z: -30, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 7,
    fromImagePosition: new THREE.Vector2(1129, 431),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

export async function createGround(
  projectSettings: ProjectSettings,
) {
  const { scene3d } = projectSettings;
  const actor = await createActor(projectSettings, {
    width: 2048,
    height: 730,
    imgSrc: '../assets/projects/weesperstraat/straat-google-7.jpg',
  }, {
    box: { w: 40, h: 25, d: 0.1 }, // w: 70.111
    imageRect: { w: 2048, h: 730 },
    depth: 0.1,
    color: 0xbe916d,
  });
  actor.setStaticPosition(getMatrix4({ x: -20, y: 12.5 }));
  actor.setStaticImage(0, 0);
  actor.getMesh().receiveShadow = true;

  const groundExObj = new ExtendedObject3D();
  groundExObj.add(actor.getMesh());
  groundExObj.position.set(0, -1.2, -7);
  groundExObj.rotation.set(Math.PI / 2, Math.PI, 0);
  scene3d.scene.add(groundExObj);
  scene3d.physics.add.existing(groundExObj, { collisionFlags: 1, mass: 0 });
}

async function createLantarenpaal(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
  x: number,
) {
  const scale = 2.65;
  const actor = await createActor(projectSettings, media.image3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/lantarenpaal.svg' },
    imageRect: { w: 116, h: 794 },
    depth: 0.005,
    color: 0xdec5a9,
  });
  actor.setStaticPosition(getMatrix4({ x, y: 12.5, z: -16, sx: scale, sy: scale }));
  actor.setStaticImage(217, 83);
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

export function createSky(
  projectSettings: ProjectSettings,
) {
  const { scene } = projectSettings;

  scene.background = new THREE.TextureLoader().load(
    '../assets/projects/weesperstraat/lucht.jpg',
  );
  scene.background.colorSpace = THREE.SRGBColorSpace;
  scene.background.needsUpdate = true;
}

async function createTekeningen(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const settings = { depth: 0.005, color: 0xdec5a9 };

  {
    const s = 2.3;
    const linksOnder = await createActor(projectSettings, media.tekeningen, {
      svg: { scale: svgScale, url: '../assets/projects/weesperstraat/tekening-boom.svg' },
      imageRect: { w: 513, h: 595 },
      ...settings,
    });
    linksOnder.setStaticPosition(getMatrix4({ x: 2.5, y: 5, z: -33, sx: s, sy: s }));
    linksOnder.setStaticImage(11, 30);
    group.add(linksOnder.getMesh());
  }

  {
    const s = 1.8;
    const linksOnder = await createActor(projectSettings, media.tekeningen, {
      svg: { scale: svgScale, url: '../assets/projects/weesperstraat/tekening-gevel.svg' },
      imageRect: { w: 256, h: 599 },
      ...settings,
    });
    linksOnder.setStaticPosition(getMatrix4({ x: 15.6, y: 5, z: -25, sx: s, sy: s }));
    linksOnder.setStaticImage(591, 40);
    group.add(linksOnder.getMesh());
  }
}

async function createVerkeerspaaltjes(
  projectSettings: ProjectSettings,
  group: THREE.Group,
) {
  const { scene3d } = projectSettings;
  const scale = 0.4;
  const gltf = await scene3d.load.gltf('../assets/projects/weesperstraat/verkeerspaaltje.glb');

  const paaltjes = [
    { x: -19 },
    { x: -9 },
    { x: 14 },
  ];

  paaltjes.forEach(({ x }) => {
    const paal = (gltf.scene.getObjectByName('Verkeerspaaltje') as ExtendedObject3D).clone();

    const paalTexture = new THREE.TextureLoader().load('../assets/projects/weesperstraat/verkeerspaaltje.jpg');
    paalTexture.flipY = false;
    paal.material = new THREE.MeshPhongMaterial({
      map: paalTexture,
      shininess: 1,
    });

    paal.receiveShadow = true;
    paal.castShadow = true;
    paal.position.set(x, -1.1, -5.5);
    paal.scale.set(scale, scale, scale);
    paal.rotation.copy(group.rotation);
    scene3d.add.existing(paal);
    scene3d.physics.add.existing(paal, { shape: 'box', mass: 0 });
  });
}

async function createVoorgevel(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 2.6;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/voorgevel.svg' },
    imageRect: { w: 342, h: 866 },
    depth: 0.005,
    color: 0xdec5a9,
  });
  actor.setStaticPosition(getMatrix4({
    x: 16.5, y: (svgScale * 866) + 6, z: -18, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 17,
    fromImagePosition: new THREE.Vector2(1578, 0),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

async function createZijgevel(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
  svgScale: number,
) {
  const { patternDuration, stepDuration } = projectSettings;
  const scale = 2.6;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/weesperstraat/zijgevel.svg' },
    imageRect: { w: 197, h: 665 },
    depth: 0.005,
    color: 0xdec5a9,
  });
  actor.setStaticPosition(getMatrix4({
    x: 15.85, y: (svgScale * 866) + 1.5, z: -22, ry: Math.PI * -0.3, sx: scale, sy: scale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: 17,
    fromImagePosition: new THREE.Vector2(1387, 202),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());
  return actor.getMesh();
}

export default async function createWalls(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  group: THREE.Group,
) {
  const { width, width3d } = projectSettings;
  const svgScale = width3d / width;

  await createBomen2Achter(projectSettings, media, group, svgScale);
  await createBomen3Achter(projectSettings, media, group, svgScale);
  await createBoom2Links(projectSettings, media, group, svgScale);
  await createBoom2Links2(projectSettings, media, group, svgScale);
  await createBoom3Links2(projectSettings, media, group, svgScale);
  await createBoom3Rechts(projectSettings, media, group, svgScale);
  await createBrugrailing(projectSettings, media, group, svgScale);
  await createGebouwen(projectSettings, media, group, svgScale);
  await createGevels2Rechts(projectSettings, media, group, svgScale);
  await createLantarenpaal(projectSettings, media, group, svgScale, -10.4);
  await createLantarenpaal(projectSettings, media, group, svgScale, 18);
  await createLantarenpaal(projectSettings, media, group, svgScale, -22);
  await createTekeningen(projectSettings, media, group, svgScale);
  await createVerkeerspaaltjes(projectSettings, group);
  await createVoorgevel(projectSettings, media, group, svgScale);
  await createZijgevel(projectSettings, media, group, svgScale);
}
