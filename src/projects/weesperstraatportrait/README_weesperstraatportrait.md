# Weesperstraat Portrait

Portrait versie van Weesperstraat 3.

### Demo festival vereisten

* Size: 1080 x 1920px
* Orientation: Portrait
* Duration: Exactly 10sec
* Encoding: mp4, H.264
* Frames per second: 25fps
* Max file size: 25MB
* Do not add credits in your work: NO title, NO credit outro
* The credit will be communicated in a special DEMO caption on top of your work
* No sound: The screens will not play sound

## De Weesperstraat eigen opname video's

De twee video's moeten opnieuw omgezet worden want de image sequences had ik niet bewaard.
De video's met gecorrigeerd perspectief staan nog wel in de assets.
De 25% geschaalde preview versies zijn er ook nog.

Weesperstraat 2 2022-08-19.mov<br>
1920 x 1080<br>
0:55

```bash
# convert to png sequence
ffmpeg -i weesperstraat-2-perspective.mov '/Volumes/Samsung_X5/weesperstraat-2/frames/frame_%05d.png'
# convert preview to png sequence
ffmpeg -i weesperstraat-2_preview.mov '/Volumes/Samsung_X5/weesperstraat-2/frames_preview/frame_%05d.png'
```

Weesperstraat 3 2022-08-19.mov<br>
1920 x 1080<br>
2:42

```bash
# convert to png sequence
ffmpeg -i weesperstraat-3-perspective.mov '/Volumes/Samsung_X5/weesperstraat-3/frames/frame_%05d.png'
# convert preview to png sequence
ffmpeg -i weesperstraat-3_preview.mov '/Volumes/Samsung_X5/weesperstraat-3/frames_preview/frame_%05d.png'
```

## Tijd

10 seconden is 5 maten bij 120 BPM.

## Render

```bash
# png to mp4 (from index 502 met 25 FPS
ffmpeg -framerate 25 -start_number 502 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperstraatportrait-video-x1.mp4
# repeat 6 times, 250 frames, video only
ffmpeg -i weesperstraatportrait-video-x1.mp4 -filter_complex "loop=6:size=250:start=0" weesperstraatportrait-video-x6.mp4
```

250 frames van 502 t/m 751.\
Maar eigenlijk zou het beter zijn om met 543 te beginnen.\
Dan moeten frames 502 t/m 542 hernoemd worden naar 752 t/m 792.\
Dus het nummer 250 hoger.

```bash
# reset cnt to zero
cnt = 0
# batch rename
for f in *.png; do
  cnt+=1
  printf -v new "frame_%05d.png" $((542+cnt))
  echo Rename \"$f\" as \"$new\"
  # uncomment to do the actual renaming
  # mv "$f" "$new"
done
# png to mp4 (from index 681 met 25 FPS
ffmpeg -framerate 25 -start_number 543 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperstraatportrait-video-x1.mp4
# repeat 6 times, 250 frames, video only
ffmpeg -i weesperstraatportrait-video-x1.mp4 -filter_complex "loop=6:size=250:start=0" weesperstraatportrait-video-x6.mp4
```
