/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-param-reassign */
import { THREE } from 'enable3d';
import createTween from '@app/tween';
import { ProjectSettings } from '@app/interfaces';

// eslint-disable-next-line prefer-const
const START_DELAY = 2;
const ROTATION = -1.5;
let isCameraPaused = false;

function moveCamera2(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  const tween = createTween({
    delay: START_DELAY,
    duration: patternDuration,
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.position.x = Math.sin(progress * Math.PI * 2) * 23;
      cameraGroup.position.z = -2 + Math.sin(progress * Math.PI * 2) * 8;
    },
    onComplete: () => {},
  });
  timeline.add(tween);
}

function rotateCamera2(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  timeline.add(createTween({
    delay: START_DELAY + (patternDuration * 0.5),
    duration: patternDuration * 0.249,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = progress * ROTATION;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: START_DELAY + (patternDuration * 0.75),
    duration: patternDuration * 0.249,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = (1 - progress) * ROTATION;
    },
    onComplete: () => {},
  }));
}

export default function animateCamera(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
) {
  const { scene } = projectSettings;

  const cameraGroup = new THREE.Group();
  cameraGroup.add(camera);
  cameraGroup.position.set(0, 0, 20);
  scene.add(cameraGroup);

  moveCamera2(projectSettings, cameraGroup);
  rotateCamera2(projectSettings, cameraGroup);
}
