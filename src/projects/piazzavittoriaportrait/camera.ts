import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import createTween, { Ease } from '@app/tween';

const tweenParams: {
  ease?: Ease,
  onComplete?: () => void | undefined;
  onStart?: () => void | undefined;
} = {
  ease: 'sineInOut',
  onComplete: () => {},
  onStart: () => {},
};

let camera: THREE.PerspectiveCamera;
let patternDuration: number;
let xDist: number = 0;
let yDist: number = 0;
let zDist: number = 0;

/**
 * Dichtbij naar zee gericht, ver weg achter kademuur
 */
export function initCamera(projectSettings: ProjectSettings, cam: THREE.PerspectiveCamera) {
  const { patternDuration: p, timeline } = projectSettings;
  patternDuration = p;
  camera = cam;
  const adjustTime = 1.2;

  // naar dichtbij
  timeline.add(createTween({ ...tweenParams,
    delay: patternDuration * 0.25,
    duration: patternDuration * 0.499 - adjustTime,
    onUpdate: (progress: number) => {
      xDist = 25 - (progress * 23);
      zDist = 25 - (progress * 23);
      yDist = 2 - (progress * 2.2);
    },
  }));

  // naar ver weg, achter de kademuur
  timeline.add(createTween({ ...tweenParams,
    delay: patternDuration * 0.75 + adjustTime,
    duration: patternDuration * 0.499 - adjustTime,
    onUpdate: (progress: number) => {
      xDist = 2 + (progress * 23);
      zDist = 2 + (progress * 23);
      yDist = -0.2 + (progress * 2.2);
    },
  }));
}

export function updateCamera(carPosition: THREE.Vector3, time: number) {
  const { x, y, z } = carPosition;
  const ratio = (((time + 4) % patternDuration) / patternDuration) * Math.PI * 2;

  // move along x axis to better follow the car passengers
  const xMove = (Math.cos(ratio) * 1) - 1;

  const lookat = carPosition.clone();
  lookat.x += xMove;
  lookat.y += 1.25;

  const camX = (Math.cos(ratio) * xDist) + xMove;
  const camY = 1.5 + yDist;
  const camZ = Math.sin(ratio) * zDist;
  camera.position.x = x + camX; // 6;
  camera.position.y = y + camY; // 1.5;
  camera.position.z = z + camZ; // 3;
  camera.lookAt(lookat);
}
