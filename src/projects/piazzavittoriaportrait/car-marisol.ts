import { ExtendedObject3D, Scene3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import Car from './car';

let car: Car;

export default async function createCarMarisol(scene3d: Scene3D, gltf: GLTF) {
  const textureDriver= createTexture('../assets/projects/piazzavittoria/car-driver.jpg');
  const textureParts = createTexture('../assets/projects/piazzavittoria/car-parts.jpg');
  const textureWindshield = createTexture('../assets/projects/piazzavittoria/car-windshield.jpg');

  const data = {
    axisPositionBack: -1.05,
    axisPositionFront: 1.35,
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/piazzavittoria/car-body.jpg',
    gltf,
    kmhTarget: 30,
    physicsBox: { width: 1.4, height: 1, depth: 4.8, x:0 , y: 0.6, z: 0.0 },
    position: new THREE.Vector3(0, -1.5, -3),
    rotationY: Math.PI / 2,
    scale: 1,
    wheelAxisHeight: 0.05,
    wheelDiameter: 0.937,
    wheelHalfTrack: 0.5,
    wheelScale: 1,
    wheelTexturePath: '../assets/projects/piazzavittoria/car-parts.jpg',
  };

  car = new Car(scene3d);
  await car.createCar(data);

  if (!car.chassis) {
    return car;
  }

  const body = car.chassis;

  // ACHTERLICHT
  const lightRight = createPart('Backlight', textureParts, gltf);
  lightRight.position.set(0.466683, 0.559126, -2.43946);
  body.add(lightRight);

  const lightLeft = createPart('Backlight', textureParts, gltf);
  lightLeft.position.set(-0.466683, 0.559126, -2.43946);
  body.add(lightLeft);

  // ACHTERBUMPER
  const achterbumper = createPart('Achterbumper', textureParts, gltf);
  achterbumper.position.set(0, 0.144376, -2.37984);
  body.add(achterbumper);

  // VOORBUMPER
  const voorbumper = createPart('Voorbumper', textureParts, gltf);
  voorbumper.position.set(0, 0, 2.40825);
  body.add(voorbumper);

  // WINDSCREEN
  const windscreen = createPart('Windscreen', textureWindshield, gltf);
  windscreen.position.set(0, 1.1025, 1.03552);
  body.add(windscreen);

  // DRIVER
  const driver = createPart('Driver', textureDriver, gltf);
  driver.position.set(0.311659, 1.08621, 0.261997);
  body.add(driver);

  // MARISOL
  const marisol = createPart('Marisol', textureDriver, gltf);
  marisol.position.set(-0.368139, 0.714031, 0.195091);
  body.add(marisol);

  // PASSENGER
  const passenger = createPart('Passenger', textureWindshield, gltf);
  passenger.position.set(-0.360768, 1.09678, -0.900632);
  body.add(passenger);

  // HATMAN
  const hatman = createPart('Hatman', textureWindshield, gltf);
  hatman.position.set(0.380003, 1.4749, -0.796929);
  body.add(hatman);

  // GIRL
  const girl = createPart('Girl', textureWindshield, gltf);
  girl.position.set(-0.03206, 1.06496, -0.691171);
  body.add(girl);

  // HAND VOOR
  const handfront = createPart('HandVoor', textureWindshield, gltf);
  handfront.position.set(-0.383852, 1.12494, 0.533534);
  body.add(handfront);

  // HAND ACHTER
  const handback = createPart('HandAchter', textureWindshield, gltf);
  handback.position.set(0.567464, 1.10396, -0.600853);
  body.add(handback);

  // ARM
  const arm = createPart('Arm', textureWindshield, gltf);
  arm.position.set(0.436362, 1.06378, -1.16967);
  body.add(arm);

  // STUUR
  const stuur = createPart('Stuur', textureParts, gltf);
  stuur.position.set(0.379943, 1.08313, 0.676103 );
  body.add(stuur);

  // RADIO
  const radio = createPart('Radio', textureParts, gltf);
  radio.position.set(-0.248296, 1.09192, 0.755415 );
  body.add(radio);

  return car;
}

function createPart(
  modelName: string,
  texture: THREE.Texture | THREE.Texture[],
  gltf: GLTF,
) {
  const model = (gltf.scene.getObjectByName(modelName) as ExtendedObject3D).clone();
  if (Array.isArray(texture)) {
    model.children.forEach((child, index) => {
      child.material = new THREE.MeshPhongMaterial({ map: texture[index] });
      child.castShadow = true;
      child.receiveShadow = true;
    });
  } else {
    model.material = new THREE.MeshPhongMaterial({ map: texture  });
    model.receiveShadow = true;
    model.castShadow = true;
  }
  return model;
}

function createTexture(texturePath: string) {
  const texture = new THREE.TextureLoader().load(texturePath, () => {
    texture.flipY = false;
    texture.colorSpace = THREE.SRGBColorSpace;
    texture.needsUpdate = true;
  });
  return texture;
}