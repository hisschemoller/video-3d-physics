# Piazza Vittoria Portrait

Portrait versie van Piazza Vittoria.

### Demo festival vereisten

* Size: 1080 x 1920px
* Orientation: Portrait
* Duration: Exactly 10sec
* Encoding: mp4, H.264
* Frames per second: 25fps
* Max file size: 25MB
* Do not add credits in your work: NO title, NO credit outro
* The credit will be communicated in a special DEMO caption on top of your work
* No sound: The screens will not play sound

## Tijd

10 seconden is 5 maten bij 120 BPM.

## Render

Frame 180, 430, 680: Marisol in beeld\

```bash
# png to mp4 (from index 180 met 25 FPS
ffmpeg -framerate 25 -start_number 180 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p piazzavittoriaportrait-video-x1.mp4
# repeat 6 times, 250 frames, video only
ffmpeg -i piazzavittoriaportrait-video-x1.mp4 -filter_complex "loop=6:size=250:start=0" piazzavittoriaportrait-video-x6.mp4
```
