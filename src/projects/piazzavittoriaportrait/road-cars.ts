import { Scene3D, THREE } from 'enable3d';
import Car from './car';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';

let blueBusGltf: GLTF | undefined;
let orangeCarGltf: GLTF | undefined;
let redCarGltf: GLTF | undefined;
let whiteTaxtGltf: GLTF | undefined;

export async function addBlueBus(scene3d: Scene3D, x: number, z: number) {
  if (!blueBusGltf) {
    blueBusGltf = await scene3d.load.gltf('../assets/projects/piazzavittoria/car-blue.glb');
  }

  const data = {
    axisPositionBack: -1.4,
    axisPositionFront: 1.65,
    bodyMass: 1500,
    bodyTexturePath: '../assets/projects/piazzavittoria/car-blue-body-texture.jpg',
    gltf: blueBusGltf,
    kmhTarget: 28,
    physicsBox: { width: 1.6, height: 1.5, depth: 4.5, x: 0, y: 0.8, z: 0.0 },
    position: new THREE.Vector3(x, 1, z),
    rotationY: Math.PI / -2,
    scale: 1.15,
    wheelAxisHeight: 0,
    wheelDiameter: 0.56,
    wheelHalfTrack: 0.75,
    wheelScale: 0.95,
    wheelTexturePath: '../assets/projects/piazzavittoria/car-blue-wheel-texture.jpg',
  };
  const car = new Car(scene3d);
  await car.createCar(data);

  return car;
}

export async function addOrangeCar(scene3d: Scene3D, x: number, z: number) {
  if (!orangeCarGltf) {
    orangeCarGltf = await scene3d.load.gltf('../assets/projects/piazzavittoria/car-orange.glb');
  }

  const data = {
    axisPositionBack: -1.65,
    axisPositionFront: 1.34,
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/piazzavittoria/car-orange-body-texture.jpg',
    gltf: orangeCarGltf,
    kmhTarget: 29,
    physicsBox: { width: 2, height: 1.5, depth: 3.8, x: 0, y: 0.75, z: 0 },
    position: new THREE.Vector3(x, 0.5, z),
    rotationY: Math.PI / -2,
    scale: 1,
    wheelAxisHeight: 0.1,
    wheelDiameter: 0.6,
    wheelHalfTrack: 0.92,
    wheelScale: 1,
    wheelTexturePath: '../assets/projects/piazzavittoria/car-orange-wheel-texture.jpg',
  };
  const car = new Car(scene3d);
  await car.createCar(data);

  return car;
}

export async function addRedCar(scene3d: Scene3D, x: number, z: number) {
  if (!redCarGltf) {
    redCarGltf = await scene3d.load.gltf('../assets/projects/piazzavittoria/car-red.glb');
  }

  const data = {
    axisPositionBack: -1.7,
    axisPositionFront: 1.55,
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/piazzavittoria/car-red-body-texture.jpg',
    gltf: redCarGltf,
    kmhTarget: 30,
    physicsBox: { width: 2.2, height: 1, depth: 4.8, x: 0, y: 0.6, z: 0 },
    position: new THREE.Vector3(x, 1, z),
    rotationY: Math.PI / -2,
    scale: 0.8,
    wheelAxisHeight: 0,
    wheelDiameter: 0.721,
    wheelHalfTrack: 1.05,
    wheelScale: 0.9,
    wheelTexturePath: '../assets/projects/piazzavittoria/car-red-wheel-texture.jpg',
  };
  const car = new Car(scene3d);
  await car.createCar(data);

  return car;
}

export async function addWhiteTaxiCar(scene3d: Scene3D, x: number, z: number) {
  if (!whiteTaxtGltf) {
    whiteTaxtGltf = await scene3d.load.gltf('../assets/projects/piazzavittoria/car-taxi-napoli.glb');
  }

  const data = {
    axisPositionBack: -1.98,
    axisPositionFront: 1.25,
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/piazzavittoria/car-taxi-napoli.jpg',
    gltf: whiteTaxtGltf,
    kmhTarget: 27,
    physicsBox: { width: 2.1, height: 1.7, depth: 4.5, x: 0, y: 0.85, z: 0 },
    position: new THREE.Vector3(x, 1, z),
    rotationY: Math.PI / -2,
    scale: 1,
    wheelAxisHeight: 0.08,
    wheelDiameter: 0.744,
    wheelHalfTrack: 0.92,
    wheelScale: 1,
    wheelTexturePath: '../assets/projects/piazzavittoria/car-taxi-napoli.jpg',
  };
  const car = new Car(scene3d);
  await car.createCar(data);

  return car;
}
