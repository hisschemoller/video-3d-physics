
# _unnamed

* Create a Low Poly Person (Grant Abbitt)
  * https://www.youtube.com/watch?v=4OUYOKGl7x0
* Rigging a Low Poly Person (Grant Abbitt)
  * https://www.youtube.com/watch?v=srpOeu9UUBU
* How To Easily RIG Characters With RIGIFY in BLENDER 3
  * https://www.youtube.com/watch?v=snvlZcQUoXs
* Tutorial: Human Meta Rig | Blender 3.3
  * https://www.youtube.com/watch?v=GmvWOcqHEjw
