import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import { ALL_ENTITIES } from './figures-follow-leader';
import { getSpecialtyPart } from './figures';

type Clock = {
  bigHand: THREE.Mesh;
  smallHand: THREE.Mesh;
  direction: 1 | -1;
};

const clocks: Clock[] = [];

let patternDuration: number;

export function initClocks(projectSettings: ProjectSettings) {
  patternDuration = projectSettings.patternDuration;

  ALL_ENTITIES.forEach((entity) => {
    entity.mesh.children.forEach((object3d) => {
      switch (object3d.name) {
        case 'KienholzKlokHoofd': {
          const position = new  THREE.Vector3(-0.10, 0.33, 0);
          const bigHand = getSpecialtyPart('kienholzClockBigHand').mesh.clone();
          const smallHand = getSpecialtyPart('kienholzClockSmallHand').mesh.clone();
          bigHand.position.copy(position);
          smallHand.position.copy(position);
          object3d.add(bigHand);
          object3d.add(smallHand);
          clocks.push({ bigHand, smallHand, direction: 1 });
          break;
        }
        case 'KellyTireClockHead': {
          const position = new  THREE.Vector3(-0.14, 0.26, 0);
          const bigHand = getSpecialtyPart('kellyTireClockBigHand').mesh.clone();
          const smallHand = getSpecialtyPart('kellyTireClockSmallHand').mesh.clone();
          bigHand.position.copy(position);
          smallHand.position.copy(position);
          object3d.add(bigHand);
          object3d.add(smallHand);
          clocks.push({ bigHand, smallHand, direction: 1 });
          break;
        }
      }
    });
  });

  projectSettings.scene3d.scene.getObjectsByProperty('name', 'Stadsklok').forEach((object3d) => {
    for(let i = 0; i < 2; i++) {
      const position = new  THREE.Vector3(i === 0 ? -0.135 : 0.135, 3.69, 0);
      const bigHand = getSpecialtyPart('stadsklokClockBigHand').mesh.clone();
      const smallHand = getSpecialtyPart('stadsklokClockSmallHand').mesh.clone();
      bigHand.position.copy(position);
      smallHand.position.copy(position);
      object3d.add(bigHand);
      object3d.add(smallHand);
      clocks.push({ bigHand, smallHand, direction: i === 0 ? 1 : -1 });
    }
  });
}

export function updateClocks(time: number) {
  const rotation = (time / patternDuration) * Math.PI;
  clocks.forEach((clock) => {
    clock.smallHand.rotation.x = rotation * 8 * clock.direction;
    clock.bigHand.rotation.x = rotation * 96 * clock.direction;
  });
}
