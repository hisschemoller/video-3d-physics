import { THREE } from 'enable3d';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { ALL_ENTITIES } from './figures-follow-leader';
import { getSpecialtyPart } from './figures';
import { createActor } from '@app/actor';
import createTween from '@app/tween';

let patternDuration: number;

export async function initAnimatedHeads(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
) {
  patternDuration = projectSettings.patternDuration;

  ALL_ENTITIES.forEach((entity) => {
    entity.mesh.children.forEach(async (object3d) => {
      switch (object3d.name) {
        case 'OogHoofd': {
          await initOogHoofd(projectSettings, media, object3d);
          break;
        }
        case 'TlaxcalaHead': {
          await initTlaxcalaHead(projectSettings, object3d);
          break;
        }
      }
    });
  });
}

/**
 * 
 */
async function initOogHoofd(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  object3d: THREE.Object3D,
) {
  const oogHoofdOogbol = getSpecialtyPart('oogHoofdOogbol').mesh.clone();
  oogHoofdOogbol.position.set(0, 0, 0);
  object3d.add(oogHoofdOogbol);

  const oogHoofdHaar = getSpecialtyPart('oogHoofdHaar').mesh.clone();
  oogHoofdHaar.position.set(0, 0, 0);
  object3d.add(oogHoofdHaar);

  const oogHoofdOog = getSpecialtyPart('oogHoofdOog').mesh.clone();
  

  const videoData = media.mijn_oog_8776 as VideoData;
  const actor = await createActor(projectSettings, videoData, {
    model: oogHoofdOog,
    imageRect: { w: videoData.width, h: videoData.height },
    depth: 0.2,
  });
  actor.addTween({
    delay: 0.1,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 29,
    videoStart: 0.1,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.setStaticPosition(oogHoofdOog.matrix);
  actor.getMesh().position.set(0, 0, 0);
  object3d.add(actor.getMesh());
}

/**
 * 
 */
async function initTlaxcalaHead(
  projectSettings: ProjectSettings,
  object3d: THREE.Object3D,
) {
  const { patternDuration, timeline } = projectSettings;

  const eyes = getSpecialtyPart('tlaxcalaHeadEyes').mesh.clone();
  eyes.position.set(0, 0, 0);
  object3d.add(eyes);

  // tween eyes
  const tweenEyes = createTween({
    delay: 3,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 2,
    onStart: () => {},
    onUpdate: (progress) => {
      eyes.position.z = Math.sin(progress * Math.PI * 2) * -0.01;
    },
    onComplete: () => {},
  });
  timeline.add(tweenEyes);

  // tween head
  const tween = createTween({
    delay: 3,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 10,
    onStart: () => {},
    onUpdate: (progress) => {
      if (progress > 0 && progress <= 0.1) {
        object3d.rotation.y = Math.PI * progress * 10;
      } else if (progress > 0.5 && progress <= 0.6) {
        object3d.rotation.y = Math.PI + (((progress - 0.5) * 10) * Math.PI);
      }
    },
    onComplete: () => {},
  });
  timeline.add(tween);
}
