import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import { SteeringEntity } from './three-steer';
import { ALL_ENTITIES, AVOID_DISTANCE, ENTITY_RADIUS, MAX_SEPERATION, SEPERATION_RADIUS, TOO_CLOSE_DISTANCE } from './figures-follow-leader';
import { GROUND_Y } from './munt';
import { getSpecialtyPart } from './figures';

let entities: SteeringEntity[] = [];

function createStadsklok(
  projectSettings: ProjectSettings,
  position: THREE.Vector3,
  rotateY: number,
) {
  const { scene3d } = projectSettings;
  const  scale = 1.2;

  const stadsklok = getSpecialtyPart('stadsklok').mesh.clone();
  stadsklok.position.copy(position);
  stadsklok.scale.set(scale,  scale, scale);
  stadsklok.rotateY(rotateY);
  scene3d.scene.add(stadsklok);

  const entity = new SteeringEntity(stadsklok);
  entity.radius = ENTITY_RADIUS;
  entity.avoidDistance = AVOID_DISTANCE;
  entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
  scene3d.scene.add(entity);
  entities.push(entity);

  ALL_ENTITIES.push(entity);
}

export async function initStaticObjects(
  projectSettings: ProjectSettings,
) {
  createStadsklok(projectSettings, new THREE.Vector3(9, GROUND_Y, 10), Math.PI / 2);
  createStadsklok(projectSettings, new THREE.Vector3(8, GROUND_Y, -10), 0);
  createStadsklok(projectSettings, new THREE.Vector3(-11, GROUND_Y, -9), 0);
  createStadsklok(projectSettings, new THREE.Vector3(-9, GROUND_Y, 8), Math.PI / 2);

  createStadsklok(projectSettings, new THREE.Vector3(-1, GROUND_Y, -8), 0);
  createStadsklok(projectSettings, new THREE.Vector3(2, GROUND_Y, 8), Math.PI / 2);
  createStadsklok(projectSettings, new THREE.Vector3(7, GROUND_Y, 0), Math.PI / 2);
  createStadsklok(projectSettings, new THREE.Vector3(8, GROUND_Y, -1), 0);

  createStadsklok(projectSettings, new THREE.Vector3(13, GROUND_Y, 0), 0); // reguliers
  createStadsklok(projectSettings, new THREE.Vector3(-13, GROUND_Y, 0), 0); // munt
  createStadsklok(projectSettings, new THREE.Vector3(0, GROUND_Y, 12), Math.PI / 2); // vijzel
  createStadsklok(projectSettings, new THREE.Vector3(0, GROUND_Y, -12), Math.PI / 2); // rokin

  createStadsklok(projectSettings, new THREE.Vector3(11, GROUND_Y, -5), 0); // reguliers
  createStadsklok(projectSettings, new THREE.Vector3(-10, GROUND_Y, 5), 0); // munt
  createStadsklok(projectSettings, new THREE.Vector3(-5, GROUND_Y, 10), Math.PI / 2); // vijzel
  createStadsklok(projectSettings, new THREE.Vector3(-6, GROUND_Y, -10), Math.PI / 2); // rokin

  createStadsklok(projectSettings, new THREE.Vector3(-3.5, GROUND_Y, 5), Math.PI / 2); // munt
}

export function updateStaticObjects() {
  entities.forEach((entity) => entity.separation(ALL_ENTITIES, SEPERATION_RADIUS, MAX_SEPERATION));
}
