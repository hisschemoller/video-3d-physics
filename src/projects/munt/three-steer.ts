import { THREE } from 'enable3d';

class Velocity extends THREE.Vector3 {
  constructor(x: number, y: number, z: number) {
    super(x, y, z);
  }

  get angle() {
    return Math.atan2(this.z, this.x);
  }

  set angle(value: number) {
    this.x = Math.cos(value) * this.length();
    this.z = Math.sin(value) * this.length();
  }

  perp () {
    return new THREE.Vector3(-this.z, 0, this.x);
  }

  sign (vector: THREE.Vector3) {
    return this.perp().dot(vector) < 0 ? -1 : 1;
  }
}

export class Entity extends THREE.Group {
  box: THREE.Box3;

  lookAtDirection: boolean = false;

  mass: number = 1;

  maxSpeed: number = 10;

  mesh: THREE.Mesh | THREE.Group;

  numSamplesForSmoothing: number = 20;

  radius: number = 20;

  raycaster: THREE.Raycaster = new THREE.Raycaster();

  velocity: Velocity = new Velocity(0, 0, 0);

  velocitySamples: THREE.Vector3[] = [];

  constructor(mesh: THREE.Mesh | THREE.Group) {
    super();
    this.mesh = mesh;
    this.box = new THREE.Box3().setFromObject(mesh);
    this.position.set(0, 0, 0);
    this.add(this.mesh);
  }

  update(secondFraction: number) {
    this.velocity.clampLength(0, this.maxSpeed);
    this.velocity.setY(0);
    this.position.add(this.velocity.clone().multiplyScalar(secondFraction));
  }
  
  bounce (box: THREE.Box3) {
    if (this.position.x > box.max.x) {
      this.position.setX(box.max.x);
      this.velocity.angle = this.velocity.angle + 0.1;
    }

    if (this.position.x < box.min.x) {
      this.position.setX(box.min.x);
      this.velocity.angle = this.velocity.angle + 0.1;
    }

    if (this.position.z > box.max.z) {
      this.position.setZ(box.max.z);
      this.velocity.angle = this.velocity.angle + 0.1;
    }
    if (this.position.z < box.min.z) {
      this.position.setZ(box.min.z);
      this.velocity.angle = this.velocity.angle + 0.1;
    }

    if (this.position.y > box.max.y) {
      this.position.setY(box.max.y);
    }

    if (this.position.y < box.min.y) {
      this.position.setY(-box.min.y);
    }
  }

  wrap (box: THREE.Box3) {
    if (this.position.x > box.max.x) {
      this.position.setX(box.min.x + 1);
    } else if (this.position.x < box.min.x) {
      this.position.setX(box.max.x - 1);
    }

    if (this.position.z > box.max.z) {
      this.position.setZ(box.min.z + 1);
    } else if (this.position.z < box.min.z) {
      this.position.setZ(box.max.z - 1);
    }

    if (this.position.y > box.max.y) {
      this.position.setY(box.min.y + 1);
    } else if (this.position.y < box.min.y) {
      this.position.setY(box.max.y + 1);
    }
  }

  lookWhereGoing (smoothing: boolean) {
    var direction = this.position.clone().add(this.velocity).setY(this.position.y);
    if (smoothing) {
      if (this.velocitySamples.length == this.numSamplesForSmoothing) {
        this.velocitySamples.shift();
      }

      this.velocitySamples.push(this.velocity.clone().setY(this.position.y));
      direction.set(0, 0, 0);
      for (var v = 0; v < this.velocitySamples.length; v++) {
        direction.add(this.velocitySamples[v]);
      }
      direction.divideScalar(this.velocitySamples.length);
      direction = this.position.clone().add(direction).setY(this.position.y);
    }
    this.lookAt(direction);
  }

  get width() {
    return this.box.max.x - this.box.min.x;
  }

  get height() {
    return this.box.max.y - this.box.min.y;
  }

  get depth() {
    return this.box.max.z - this.box.min.z;
  }

  get forward() {
    return new THREE.Vector3(0, 0, -1).applyQuaternion(this.quaternion).negate();
  }

  get backward() {
    return this.forward.clone().negate();
  }

  get left() {
    return this.forward.clone().applyAxisAngle(new THREE.Vector3(0, 1, 0), Math.PI * 0.5);
  }

  get right() {
    return this.left.clone().negate();
  }
}

export class SteeringEntity extends Entity {
  arrivalThreshold: number = 400;

  avoidDistance: number = 400;

  inSightDistance: number = 200;

  maxForce: number = 5;

  pathIndex: number = 0;

  steeringForce = new THREE.Vector3(0, 0, 0);

  tooCloseDistance: number = 60;

  wanderAngle: number = 0;

  wanderDistance: number = 10;

  wanderRadius: number = 5;

  wanderRange: number = 1;

  constructor(mesh: THREE.Mesh | THREE.Group) {
    super(mesh);
  }

  seek (position: THREE.Vector3) {
    const desiredVelocity = position.clone().sub(this.position);
    desiredVelocity.normalize().setLength(this.maxSpeed).sub(this.velocity);
    this.steeringForce.add(desiredVelocity);
  }

  flee (position: THREE.Vector3) {
    const desiredVelocity = position.clone().sub(this.position);
    desiredVelocity.normalize().setLength(this.maxSpeed).sub(this.velocity);
    this.steeringForce.sub(desiredVelocity);
  }

  arrive (position: THREE.Vector3) {
    const desiredVelocity = position.clone().sub(this.position);
    desiredVelocity.normalize();
    const distance = this.position.distanceTo(position);

    if (distance > this.arrivalThreshold) {
      desiredVelocity.setLength(this.maxSpeed);
    } else {
      desiredVelocity.setLength((this.maxSpeed * distance) / this.arrivalThreshold);
    }

    desiredVelocity.sub(this.velocity);
    this.steeringForce.add(desiredVelocity);
  }

  pursue (target: Entity) {
    const lookAheadTime = this.position.distanceTo(target.position) / this.maxSpeed;
    const predictedTarget = target.position.clone().add(target.velocity.clone().setLength(lookAheadTime));
    this.seek(predictedTarget);
  }

  evade (target: Entity) {
    const lookAheadTime = this.position.distanceTo(target.position) / this.maxSpeed;
    const predictedTarget = target.position.clone().sub(target.velocity.clone().setLength(lookAheadTime));
    this.flee(predictedTarget);
  }

  idle () {
    this.velocity.setLength(0);
    this.steeringForce.set(0, 0, 0);
  }

  wander () {
    const center = this.velocity.clone().normalize().setLength(this.wanderDistance);
    const offset = new THREE.Vector3(1, 1, 1);
    offset.setLength(this.wanderRadius);
    offset.x = Math.sin(this.wanderAngle) * offset.length();
    offset.z = Math.cos(this.wanderAngle) * offset.length();
    offset.y = Math.sin(this.wanderAngle) * offset.length();

    this.wanderAngle += Math.random() * this.wanderRange - this.wanderRange * 0.5;
    center.add(offset);
    center.setY(0);
    this.steeringForce.add(center);
  }

  interpose (targetA: Entity, targetB: Entity) {
    let midPoint = targetA.position.clone().add(targetB.position.clone()).divideScalar(2);
    const timeToMidPoint = this.position.distanceTo(midPoint) / this.maxSpeed;
    const pointA = targetA.position.clone().add(targetA.velocity.clone().multiplyScalar(timeToMidPoint));
    const pointB = targetB.position.clone().add(targetB.velocity.clone().multiplyScalar(timeToMidPoint));
    midPoint = pointA.add(pointB).divideScalar(2);
    this.seek(midPoint);
  }

  separation (entities: Entity[], separationRadius: number = 300, maxSeparation: number = 100) {
    const force = new THREE.Vector3(0, 0, 0);
    let neighborCount = 0;

    entities.forEach((entity) => {
      if (entity != this && entity.position.distanceTo(this.position) <= separationRadius) {
        force.add(entity.position.clone().sub(this.position));
        neighborCount++;
      }
    });

    if (neighborCount != 0) {
      force.divideScalar(neighborCount);
      force.negate();
    }
  
    force.normalize();
    force.multiplyScalar(maxSeparation);
    this.steeringForce.add(force);
  }

  isOnLeaderSight (leader: Entity, ahead: THREE.Vector3, leaderSightRadius: number) {
    return ahead.distanceTo(this.position) <= leaderSightRadius || leader.position.distanceTo(this.position) <= leaderSightRadius;
  }

  followLeader (
    leader: Entity,
    entities: Entity[],
    distance: number = 400,
    separationRadius: number = 300,
    maxSeparation: number = 100,
    leaderSightRadius: number = 1600,
    arrivalThreshold: number = 200,
  ) {
    const tv = leader.velocity.clone();
    tv.normalize().multiplyScalar(distance);
    const ahead = leader.position.clone().add(tv);
    tv.negate();
    const behind = leader.position.clone().add(tv);

    if (this.isOnLeaderSight(leader, ahead, leaderSightRadius)) {
      // this.evade(leader);
    }
    this.arrivalThreshold = arrivalThreshold;
    this.arrive(behind);
    this.separation(entities, separationRadius, maxSeparation);
  }

  getNeighborAhead (entities: Entity[]) {
    const maxQueueAhead = 500;
    const maxQueueRadius = 500;
    let res: Entity | undefined;
    const qa = this.velocity.clone().normalize().multiplyScalar(maxQueueAhead);
    const ahead = this.position.clone().add(qa);

    for (var i = 0; i < entities.length; i++) {
      const distance = ahead.distanceTo(entities[i].position);
      if (entities[i] != this && distance <= maxQueueRadius) {
        res = entities[i];
        break;
      }
    }
    return res;
  }

  queue (entities: Entity[], maxQueueRadius: number = 500) {
    const neighbor = this.getNeighborAhead(entities);
    let brake = new THREE.Vector3(0, 0, 0);
    const v = this.velocity.clone();
    if (neighbor != null) {
      brake = this.steeringForce.clone().negate().multiplyScalar(0.8);
      v.negate().normalize();
      brake.add(v);
      if (this.position.distanceTo(neighbor.position) <= maxQueueRadius) {
        this.velocity.multiplyScalar(0.3);
      }
    }

    this.steeringForce.add(brake);
  }

  inSight (entity: Entity) {
    if (this.position.distanceTo(entity.position) > this.inSightDistance) return false;
    const heading = this.velocity.clone().normalize();
    const difference = entity.position.clone().sub(this.position);
    const dot = difference.dot(heading);
    if (dot < 0) return false;
    return true;
  }

  flock (entities: Entity[]) {
    const averageVelocity = this.velocity.clone();
    const averagePosition = new THREE.Vector3(0, 0, 0);
    let inSightCount = 0;
    for (var i = 0; i < entities.length; i++) {
      if (entities[i] != this && this.inSight(entities[i])) {
        averageVelocity.add(entities[i].velocity);
        averagePosition.add(entities[i].position);
        if (this.position.distanceTo(entities[i].position) < this.tooCloseDistance) {
          this.flee(entities[i].position);
        }
        inSightCount++;
      }
    }
    if (inSightCount > 0) {
      averageVelocity.divideScalar(inSightCount);
      averagePosition.divideScalar(inSightCount);
      this.seek(averagePosition);
      this.steeringForce.add(averageVelocity.sub(this.velocity));
    }
  }

  followPath (path: THREE.Vector3[], loop: boolean, thresholdRadius: number = 1) {
    const wayPoint = path[this.pathIndex];
    if (wayPoint == null) {
      return;
    }

    if (this.position.distanceTo(wayPoint) < thresholdRadius) {
      if (this.pathIndex >= path.length - 1) {
        if (loop) {
          this.pathIndex = 0;
        }
      } else {
        this.pathIndex++;
      }
    }

    if (this.pathIndex >= path.length - 1 && !loop) {
      this.arrive(wayPoint);
    } else {
      this.seek(wayPoint);
    }
  }

  /**
   * Avoid only considers entities ahead.
   */
  avoid (obstacles: Entity[]) {
    // get position ahead of entity
    const dynamic_length = this.velocity.length() / this.maxSpeed;
    const ahead = this.position.clone().add(this.velocity.clone().normalize().multiplyScalar(dynamic_length));
    const ahead2 = this.position.clone().add(
      this.velocity
        .clone()
        .normalize()
        .multiplyScalar(this.avoidDistance * 0.5)
    );
    // get most threatening
    let mostThreatening: Entity | undefined = undefined;
    for (let i = 0; i < obstacles.length; i++) {
      if (obstacles[i] === this) continue;
      const collision = obstacles[i].position.distanceTo(ahead) <= obstacles[i].radius || obstacles[i].position.distanceTo(ahead2) <= obstacles[i].radius;
      if (collision && (mostThreatening == null || this.position.distanceTo(obstacles[i].position) < this.position.distanceTo(mostThreatening.position))) {
        mostThreatening = obstacles[i];
      }
    }

    // obstacles.forEach((obstacle) => {
    //   if (obstacle !== this) {
    //     const collision = obstacle.position.distanceTo(ahead) <= obstacle.radius || obstacle.position.distanceTo(ahead2) <= obstacle.radius;
    //     if (collision && (mostThreatening == null || this.position.distanceTo(obstacle.position) < this.position.distanceTo(mostThreatening.position))) {
    //       mostThreatening = obstacle;
    //     }
    //   }
    // });

    // end
    let avoidance = new THREE.Vector3(0, 0, 0);
    if (mostThreatening !== undefined) {
      avoidance = ahead.clone().sub(mostThreatening.position).normalize().multiplyScalar(100);
    }
    this.steeringForce.add(avoidance);
  }

  update (secondFraction: number = 1) {
    this.steeringForce.clampLength(0, this.maxForce);
    this.steeringForce.divideScalar(this.mass);
    this.velocity.add(this.steeringForce);
    this.steeringForce.set(0, 0, 0);
    super.update(secondFraction);
  }
}

// /**
//  * Returns a random number between min (inclusive) and max (exclusive)
//  */
// Math.getRandomArbitrary = function (min: number, max: number) {
//   return Math.random() * (max - min) + min;
// };

// /**
//  * Returns a random integer between min (inclusive) and max (inclusive)
//  * Using Math.round() will give you a non-uniform distribution!
//  */
// Math.getRandomInt = function (min: number, max: number) {
//   return Math.floor(Math.random() * (max - min + 1)) + min;
// };
