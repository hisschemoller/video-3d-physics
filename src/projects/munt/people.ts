import { THREE } from 'enable3d';
import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';

type Person = {
  projectSettings: ProjectSettings;
  videoData: VideoData;
  x: number;
  y: number;
  z: number;
  delay: number;
  duration: number;
  loopDuration: number;
  videoStart: number;
  fromImagePosition: THREE.Vector2;
  toImagePosition: THREE.Vector2;
};

async function createPerson({
  projectSettings,
  videoData,
  x, y, z,
  delay, duration, loopDuration, videoStart,
  fromImagePosition, toImagePosition,
}: Person) {
  const { width, height } = videoData;
  const imgScale = (1 / height) * 2; // a person is 2 meters high
  const actor = await createActor(projectSettings, videoData, {
    imageRect: { w: width, h: height },
    depth: 0.05,
    color: 'transparant',
    box: { w: width * imgScale, h: height * imgScale, d: 1 },
  });
  actor.setStaticPosition(getMatrix4({x, y, z }));
  actor.addTween({
    delay,
    duration,
    isLooped: true,
    loopDuration,
    videoStart,
    fromImagePosition,
    toImagePosition,
  });
  actor.getMesh().castShadow = false;
}

export default async function createPeople(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  fps: number,
) {
  const { patternDuration } = projectSettings;

  // MAN WITH HAT
  await createPerson({
    projectSettings, videoData: media.manWithHat as VideoData,
    x: -1, y: 0.5, z: 6,
    delay: 1,
    duration: patternDuration,
    loopDuration:  43 * (1 / fps),
    videoStart: 1 * (1 / fps),
    fromImagePosition: new THREE.Vector2(200, 0),
    toImagePosition: new THREE.Vector2(-430, 20),
  });

  // TWO WOMEN
  await createPerson({
    projectSettings, videoData: media.vrouwenRechts as VideoData,
    x: -2, y: 0.5, z: 5,
    delay: 1,
    duration: patternDuration,
    loopDuration:  (67 - 32) * (1 / fps),
    videoStart: 32 * (1 / fps),
    fromImagePosition: new THREE.Vector2(0, 0),
    toImagePosition: new THREE.Vector2(400, 0),
  });

  // MAN MET KRUKKEN
  await createPerson({
    projectSettings, videoData: media.manKrukken as VideoData,
    x: -1, y: 0.5, z: 4,
    delay: 1,
    duration: patternDuration,
    loopDuration:  (62 - 13) * (1 / fps),
    videoStart: 13 * (1 / fps),
    fromImagePosition: new THREE.Vector2(-100, 0),
    toImagePosition: new THREE.Vector2(300, 0),
  });

  // MANNEN MET WITTE SHIRTS
  await createPerson({
    projectSettings, videoData: media.mannenWitteShirts as VideoData,
    x: -1, y: 0.5, z: 6,
    delay: 1,
    duration: patternDuration,
    loopDuration:  (49 - 13) * (1 / fps),
    videoStart: 13 * (1 / fps),
    fromImagePosition: new THREE.Vector2(-100, 0),
    toImagePosition: new THREE.Vector2(330, 20),
  });

  // DIKKE MAN
  await createPerson({
    projectSettings, videoData: media.dikkeMan as VideoData,
    x: -1, y: 0.8, z: 7,
    delay: 1,
    duration: patternDuration,
    loopDuration:  (58 - 22) * (1 / fps),
    videoStart: 22 * (1 / fps),
    fromImagePosition: new THREE.Vector2(250, -25),
    toImagePosition: new THREE.Vector2(0, 0),
  });

  // ZONNEBRIL EN PET
  await createPerson({
    projectSettings, videoData: media.zonnebrilPet as VideoData,
    x: 0, y: 0.8, z: 4,
    delay: 1,
    duration: patternDuration,
    loopDuration:  (43 - 11) * (1 / fps),
    videoStart: 11 * (1 / fps),
    fromImagePosition: new THREE.Vector2(0, 0),
    toImagePosition: new THREE.Vector2(520, 0),
  });
}
