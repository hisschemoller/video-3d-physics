import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import { BOUNDARIES, GROUND_FIGURES_SIZE, GROUND_Y } from './munt';
import { SteeringEntity } from './three-steer';
import createTween from '@app/tween';
import { createRandomFigure } from './figures';
import Figure from './figure';
import {
  ALL_ENTITIES,
  ARRIVAL_TRESHOLD,
  AVOID_DISTANCE,
  ENTITY_RADIUS,
  MAX_FORCE,
  MAX_SEPERATION,
  MAX_SPEED,
  SEPERATION_RADIUS,
  TOO_CLOSE_DISTANCE,
} from './figures-follow-leader';

const NUM_CROSSERS = 36;

const zebraCrossers: ZebraCrosser[] = [];

/**
 * Setup the zebraCrossers.
 */
export function createZebraCrossers(
  projectSettings: ProjectSettings,
) {
  const { patternDuration, timeline } = projectSettings;

  // BALL
  const ballGeometry = new THREE.SphereGeometry(0.2, 8, 8);
  const ballMaterial = new THREE.MeshBasicMaterial({ color: 0xff6600 });
  const ballMesh = new THREE.Mesh(ballGeometry, ballMaterial);

  // ENTITY MESH
  const entityGeometry = new THREE.BoxGeometry(1, 2, 0.5);
  const entityMaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00, wireframe: true });
  const entityMesh = new THREE.Mesh(entityGeometry, entityMaterial);

  // ZEBRA CROSSERS
  for (let i = 0, n = NUM_CROSSERS; i < n; i++) {
    const zebraCrosser = new ZebraCrosser(
      projectSettings,
      entityMesh,
      ballMesh,
      !!Math.round(Math.random()),
    );
    ALL_ENTITIES.push(zebraCrosser.entity);
    zebraCrossers.push(zebraCrosser);
  }

  // HEEN EN WEER
  let isTweenRunning: boolean = false;
  timeline.add(createTween({
    delay: 0,
    duration: patternDuration / 2,
    onStart: () => {
      if (!isTweenRunning) {
        isTweenRunning = true;
        console.log('zebra crossers onStart');
        zebraCrossers.forEach((zc) => zc.setTargetPosition(true));
      }
    },
    onUpdate: () => {},
    onComplete: () => {
      if (isTweenRunning) {
        isTweenRunning = false;
        console.log('zebra crossers onComplete');
        zebraCrossers.forEach((zc) => zc.setTargetPosition(false));
      }
    },
  }));

  return {
    crossersLeft: zebraCrossers.reduce((a, z) => (z.isLeft ? [...a, z] : a), [] as ZebraCrosser[]),
    crossersRight: zebraCrossers.reduce((a, z) => (z.isLeft ? a : [...a, z]), [] as ZebraCrosser[]),
  };
}

/**
 * Update the zebracrossers animation.
 */
export function updateZebracrossers() {
  zebraCrossers.forEach((zebraCrosser) => {
    zebraCrosser.update();
  });
}

/**
 * De ZebraCrosser moeten bij create niet-overlappend neergezet worden?
 * Is dat wel zo, of neemt separation() dat voor zijn rekening?
 */
class ZebraCrosser {
  entity: SteeringEntity;

  ball: THREE.Mesh;

  positionLeft: THREE.Vector3;

  positionRight: THREE.Vector3;

  // separationRadius: number = 0.4;

  // maxSeparation: number = 0.2;

  // maxSpeed: number = MAX_SPEED - 0.05;

  // maxForce: number = 1;

  isLeft: boolean;

  figure: Figure;

  constructor(
    projectSettings: ProjectSettings,
    entityMesh: THREE.Mesh | THREE.Group,
    ballMesh: THREE.Mesh,
    isLeft: boolean,
  ) {
    const { scene3d } = projectSettings;

    this.isLeft = isLeft;

    entityMesh.position.setY(1);

    // FIGURE
    this.figure = createRandomFigure(projectSettings);
    this.figure.group.rotateY(Math.PI / 2);
    this.figure.group.position.setY(GROUND_Y);

    this.positionRight = new THREE.Vector3(
      (GROUND_FIGURES_SIZE / 2) - (Math.random() * 3),
      GROUND_Y,
      (GROUND_FIGURES_SIZE / 2) - (Math.random() * GROUND_FIGURES_SIZE),
    );

    this.positionLeft = this.positionRight.clone();
    this.positionLeft.x = ((GROUND_FIGURES_SIZE / 2) - (Math.random() * 3)) * -1;

    this.entity = new SteeringEntity(this.figure.group);
    this.entity.maxSpeed = MAX_SPEED;
    this.entity.maxForce = MAX_FORCE;
    this.entity.lookAtDirection = true;
    this.entity.inSightDistance = 2;
    this.entity.radius = ENTITY_RADIUS;
    this.entity.avoidDistance = AVOID_DISTANCE;
    this.entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
    this.entity.position.copy(isLeft ? this.positionLeft : this.positionRight);
    scene3d.scene.add(this.entity);

    this.ball = ballMesh.clone();
    this.ball.position.copy(isLeft ? this.positionRight : this.positionLeft);
    // scene3d.scene.add(this.ball);
  }

  update() {
    this.entity.numSamplesForSmoothing = 50;
    this.entity.maxForce = MAX_FORCE;
    this.entity.maxSpeed = MAX_SPEED;
    this.entity.arrivalThreshold = ARRIVAL_TRESHOLD;
    this.entity.arrive(this.ball.position);

    this.entity.radius = ENTITY_RADIUS;
    this.entity.avoidDistance = AVOID_DISTANCE;
    this.entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
    this.entity.avoid(ALL_ENTITIES);
    this.entity.separation(ALL_ENTITIES, SEPERATION_RADIUS, MAX_SEPERATION);

    this.entity.lookWhereGoing(true);
    this.entity.update();
    this.entity.bounce(BOUNDARIES);

    this.figure.updateLimbs(this.entity.velocity.length());
  }

  setTargetPosition(isAtStartPosition: boolean) {
    setTimeout(() => {
      if (isAtStartPosition) {
        this.ball.position.copy(this.isLeft ? this.positionRight : this.positionLeft);
      } else {
        this.ball.position.copy(this.isLeft ? this.positionLeft : this.positionRight);
      }
  
      const zDeviation = 3 - (Math.random() * 6);
      const halfSize = GROUND_FIGURES_SIZE / 2;
  
      this.ball.position.z = Math.max(-halfSize, Math.min(this.ball.position.z + zDeviation, halfSize));
    }, Math.random() * 4000);
  }
}

