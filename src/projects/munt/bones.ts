import { Bone, Object3D, SkinnedMesh } from 'three';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ProjectSettings } from '@app/interfaces';
import createTween from '@app/tween';
import { Timeline } from '@app/timeline';

export async function initTutorialBones(
  projectSettings: ProjectSettings,
  gltf: GLTF,
) {
  const { patternDuration: p, scene3d, timeline: t } = projectSettings;

  const tutorialObject3d = gltf.scene.getObjectByName('BonesTutorial') as Object3D;
  tutorialObject3d.position.set(0, -2, 0);
  tutorialObject3d.scale.set(0.6, 0.6, 0.6);
  scene3d.scene.add(tutorialObject3d);
  console.log(tutorialObject3d);

  const skinnedMesh = tutorialObject3d.getObjectByName('Cylinder') as SkinnedMesh;
  skinnedMesh.castShadow = true;
  skinnedMesh.receiveShadow = true;
  console.log(skinnedMesh.skeleton);

  skinnedMesh.skeleton.bones[1].rotateZ(Math.PI * 0.2);
  skinnedMesh.skeleton.bones[3].rotateZ(Math.PI * -0.3);

  let bone = skinnedMesh.skeleton.bones[1];
  tweenBone(t, bone, Math.PI * 0.2, 1, p, 'z');

  bone = skinnedMesh.skeleton.bones[2];
  tweenBone( t, bone, Math.PI * 0.3, 3, p, 'x');

  bone = skinnedMesh.skeleton.bones[3];
  tweenBone( t, bone, Math.PI * -0.3, 2, p, 'z');
  
  bone = skinnedMesh.skeleton.bones[4];
  tweenBone(t, bone, Math.PI * 0.4, 4, p, 'z');
}

function tweenBone(
  timeline: Timeline,
  bone: Bone,
  angle: number,
  delay: number,
  duration: number,
  axis: 'x'|'z',
) {
  const tween = createTween({
    delay,
    duration,
    onStart: () => {},
    onUpdate: (progress) => {
      bone.rotation[axis] = Math.sin(progress * Math.PI * 2) * angle;
    },
    onComplete: () => {},
  });
  timeline.add(tween);
}
