/* eslint-disable object-curly-newline */
import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import initMunt from './munt';
// import { initTutorialBones } from './bones';
import { initFigures } from './figures';
import { LeaderWithFollowers } from './figures-follow-leader';
import { initCamera, updateCamera } from './camera';
import { createZebraCrossers, updateZebracrossers } from './figures-zebra-crossers';
import { initStaticObjects } from './static-objects';
import { initClocks, updateClocks } from './clocks';
import { initAnimatedHeads } from './figure-animated-heads';

const PROJECT_PREVIEW_SCALE = 0.25;
const BPM = 108;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 8 * 5 * 2;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  dirLightTarget = new THREE.Object3D();

  leaderWithFollowers: LeaderWithFollowers | undefined;

  constructor() {
    super();

    this.width = 1920;
    this.canvasHeight = 1440;
    this.viewportHeight = 1440;
    this.width3d = 16;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 15;
    this.captureFps = 30;
    this.captureThrottle = 15; // wait 15 raf ticks
    this.captureDuration = (PATTERN_DURATION * 2) + 1;
    this.clearColor = 0x5688cb;
    this.captureShadowMapSize = 4096 * 4;
    this.shadowSize = 24;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = this.width;
    this.captureCanvas.height = this.viewportHeight;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;
  }

  /**
   * Set the html viewport and canvas height. For adjusted horizon height.
   */
  adjustHorizon() {
    const canvasContainer = document.getElementById('canvas-container');
    const canvas = this.renderer.domElement;

    if (canvasContainer && canvas) {
      canvasContainer.style.height =  `${(this.viewportHeight / this.width) * 100}vw`;
      canvas.setAttribute('style', `${(this.canvasHeight / this.viewportHeight) * 100}% !important`);
    }
  }

  async create() {
    await super.create();

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();

    // this.physics.setGravity(0, 0, 0);

    this.renderer.setSize(this.width, this.canvasHeight);

    // DIRECTIONAL LIGHT
    this.directionalLight.intensity = 1.6 * Math.PI;
    this.directionalLight.position.set(-7, 30, 15);

    // AMBIENT LIGHT
    this.ambientLight.intensity = 0.5 * Math.PI;

    // MEDIA
    const mediaVideo = {
      fps: 30,
      height: 1080,
      scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
      width: 1920,
    };

    const getImageSrcPath = (dirName: string) => isPreview
      ? `../assets/projects/munt/${dirName}_frames_preview/frame_#FRAME#.png`
      : `fs-img?dir=/Volumes/Samsung_X5/${dirName}/frames/&img=frame_#FRAME#.png`;
    
    const media = {
      manWithHat: {
        ...mediaVideo,
        height: 819,
        width: 1026,
        imgSrcPath: getImageSrcPath('munt_man_hoed_greenscreen'),
      },
      vrouwenRechts: {
        ...mediaVideo,
        height: 968,
        width: 1280,
        imgSrcPath: getImageSrcPath('munt_vrouwenrechts_greenscreen'),
      },
      manKrukken: {
        ...mediaVideo,
        height: 654,
        width: 794,
        imgSrcPath: getImageSrcPath('munt_mankrukken_greenscreen'),
      },
      mannenWitteShirts: {
        ...mediaVideo,
        height: 808,
        width: 1280,
        imgSrcPath: getImageSrcPath('munt_mannenwitteshirts_greenscreen'),
      },
      dikkeMan: {
        ...mediaVideo,
        height: 840,
        width: 682,
        imgSrcPath: getImageSrcPath('munt_dikkeman_greenscreen'),
      },
      zonnebrilPet: {
        ...mediaVideo,
        height: 840,
        width: 682,
        imgSrcPath: getImageSrcPath('munt_zonnebrilpet_greenscreen'),
      },
      munt_7909_reguliers: {
        ...mediaVideo,
        imgSrcPath: getImageSrcPath('munt_7909'),
      },
      munt_7914_europe: {
        ...mediaVideo,
        imgSrcPath: getImageSrcPath('munt_7914'),
      },
      munt_8004_toren: {
        ...mediaVideo,
        imgSrcPath: getImageSrcPath('munt_8004'),
      },
      munt_8739_vijzelstraat: {
        ...mediaVideo,
        imgSrcPath: getImageSrcPath('munt_8739'),
      },
      mijn_oog_8776: {
        ...mediaVideo,
        height: 352,
        width: 352,
        imgSrcPath: isPreview
          ? '../assets/projects/munt/mijn_oog_8776/frames_preview/frame_#FRAME#.png'
          : '../assets/projects/munt/mijn_oog_8776/frames/frame_#FRAME#.png'
      },
      groundDrawing: {
        height: 2048,
        imgSrc: '../assets/projects/munt/ground2.jpg',
        width: 2048,
      },
      sky1024: {
        height: 1024,
        imgSrc: '../assets/projects/test/testimage3d.jpg',
        width: 1024,
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.width * (12 / 16),
      height3d: this.width3d * (12 / 16),
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene: this.scene,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    this.scene.add(this.dirLightTarget);
    this.directionalLight.target = this.dirLightTarget;

    const gltf = await this.load.gltf('../assets/projects/munt/munt.glb');
    const gltfCarMarisol = await this.load.gltf('../assets/projects/munt/car_marisol2.glb');

    // createSky(projectSettings, media);
    // await createPeople(projectSettings, media, this.fps);
    // await initTutorialBones(projectSettings, gltf);

    await initFigures(gltfCarMarisol);
    await initMunt(projectSettings, media);
    await initStaticObjects(projectSettings);
    this.leaderWithFollowers = new LeaderWithFollowers(projectSettings);
    const { crossersLeft, crossersRight } = createZebraCrossers(projectSettings);
    initCamera(
      projectSettings,
      this.pCamera,
      this.leaderWithFollowers.entityFollowers,
      this.leaderWithFollowers.entityFigureLeader,
      crossersLeft,
      crossersRight,
    );
    await initAnimatedHeads(projectSettings, media);
    initClocks(projectSettings);

    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    if (this.leaderWithFollowers) {
      this.leaderWithFollowers.update();
    }
    updateCamera(time);
    updateClocks(time);
    updateZebracrossers();
    super.updateAsync(time, delta);
  }

  captureImage() {
    this.captureCanvasContext.drawImage(
      this.renderer.domElement, 0, 0, this.width, this.viewportHeight, 0, 0, this.width, this.viewportHeight,
    );
    return this.captureCanvas.toDataURL();
  }
}
