import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import { EntityFigure } from './figures-follow-leader';
import createTween from '@app/tween';
import { Timeline } from '@app/timeline';
import { GROUND_Y } from './munt';

/**
 * 1. Roteren rond iemand's hoofd.
 * 2. Iemand volgen vanaf een bewakingscamera.
 * 3. Camera in iemands gezicht van dichtbij.
 */

type CameraScene = {
  [key: string]: any;
  amount: number;
  update: (
    time: number,
    camPosition: THREE.Vector3,
    camLookAt: THREE.Vector3,
  ) => void;
};

let entityFigures: EntityFigure[];
let camera: THREE.PerspectiveCamera;
let patternDuration: number;

let lookCloseAtEyeballHeadWeirdlyUnprecise: CameraScene;
let lookCloseAtMexicanHeadWeirdlyUnprecise: CameraScene;
let lookCloseAtTireClockHeadWeirdlyUnprecise: CameraScene;
let lookCloseAtTlaxcalaHeadWeirdlyUnprecise: CameraScene;
let rotateAroundEyesHead: CameraScene;
let rotateAroundKienholzClockHead: CameraScene;
let observationPoint1: CameraScene;
let observationPoint2: CameraScene;
let verticalSquareFromAbove: CameraScene;
let veryFarAwayRotating: CameraScene;
let lookAtClocks: CameraScene;

/**
 * Get an entity with a specific head.
 */
function getEntityByHead(entityFigures: EntityFigure[], headName: string) {
  const entityFigure = entityFigures.find((e) => e.figure.head.name === headName);
  return entityFigure ? entityFigure : entityFigures[0];
}

/**
 * Init
 */
export function initCamera(
  projectSettings: ProjectSettings,
  cam: THREE.PerspectiveCamera,
  entityFollowers: EntityFigure[],
  entityLeader: EntityFigure,
  entityCrossersLeft: EntityFigure[],
  entityCrossersRight: EntityFigure[],
) {
  const { patternDuration: p, stepDuration, timeline } = projectSettings;
  patternDuration = p;
  camera = cam;
  entityFigures = entityFollowers;

  lookCloseAtEyeballHeadWeirdlyUnprecise = {
    amount: 0,
    entityY: undefined,
    cameraY: undefined,
    dist: 1.5,
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity, figure } = getEntityByHead(entityFollowers, 'OogHoofd');
        if (!this.entityY) {
          this.entityY = figure.head.position.clone()
            .multiply(figure.group.scale)
            .add(figure.group.position)
            .add(new THREE.Vector3(0, 0.3, 0));
          this.cameraY = this.entityY.clone().add(new THREE.Vector3(0, -0.3, 0));
        }
        const radians = entity.rotation.y;
        const rotationX = Math.sin(radians);
        const rotationZ = Math.cos(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.dist, 0, rotationZ * this.dist);
        camPosition.add(entity.position.clone().add(this.entityY).multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().add(this.cameraY).multiplyScalar(this.amount));
      }
    },
  };

  lookCloseAtMexicanHeadWeirdlyUnprecise = {
    amount: 0,
    entityY: undefined,
    cameraY: undefined,
    dist: 2,
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity, figure } = getEntityByHead(entityFollowers, 'MexicanMask');
        if (!this.entityY) {
          this.entityY = figure.head.position.clone()
            .multiply(figure.group.scale)
            .add(figure.group.position)
            .add(new THREE.Vector3(0, 0.3, 0));
          this.cameraY = this.entityY.clone().add(new THREE.Vector3(0, 0, 0));
        }
        const radians = entity.rotation.y;
        const rotationX = Math.sin(radians);
        const rotationZ = Math.cos(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.dist, 0, rotationZ * this.dist);
        camPosition.add(entity.position.clone().add(this.entityY).multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().add(new THREE.Vector3(0, 0.5, 0)).multiplyScalar(this.amount));
      }
    },
  };

  lookCloseAtTireClockHeadWeirdlyUnprecise = {
    amount: 0,
    entityY: undefined,
    cameraY: undefined,
    dist: 1.5,
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity, figure } = getEntityByHead(entityFollowers, 'KellyTireClockHead');
        if (!this.entityY) {
          this.entityY = figure.head.position.clone()
            .multiply(figure.group.scale)
            .add(figure.group.position)
            .add(new THREE.Vector3(0, 0.3, 0));
          this.cameraY = this.entityY.clone().add(new THREE.Vector3(0, -0.3, 0));
        }
        const radians = entity.rotation.y;
        const rotationX = Math.sin(radians);
        const rotationZ = Math.cos(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.dist, 0, rotationZ * this.dist);
        camPosition.add(entity.position.clone().add(this.entityY).multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().add(this.cameraY).multiplyScalar(this.amount));
      }
    },
  };

  lookCloseAtTlaxcalaHeadWeirdlyUnprecise = {
    amount: 0,
    entityY: undefined,
    cameraY: undefined,
    dist: 1.5,
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity, figure } = getEntityByHead(entityFollowers, 'TlaxcalaHead');
        if (!this.entityY) {
          this.entityY = figure.head.position.clone()
            .multiply(figure.group.scale)
            .add(figure.group.position)
            .add(new THREE.Vector3(0, 0.3, 0));
          this.cameraY = this.entityY.clone().add(new THREE.Vector3(0, -0.3, 0));
        }
        const radians = entity.rotation.y;
        const rotationX = Math.sin(radians);
        const rotationZ = Math.cos(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.dist, 0, rotationZ * this.dist);
        camPosition.add(entity.position.clone().add(this.entityY).multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().add(this.cameraY).multiplyScalar(this.amount));
      }
    },
  };

  observationPoint1 = {
    amount: 0,
    observationPosition: new THREE.Vector3(-12, 8, 12),
    entityFigure: getEntityByHead(entityFollowers, 'OgenHoofd'),
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity } = this.entityFigure;
        camPosition.add(this.observationPosition.clone().multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().multiplyScalar(this.amount));
      }
    },
  };

  observationPoint2 = {
    amount: 0,
    observationPosition: new THREE.Vector3(12, 12, 12),
    entityFigure: getEntityByHead(entityFollowers, 'TlaxcalaHead'),
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity } = this.entityFigure;
        camPosition.add(this.observationPosition.clone().multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().multiplyScalar(this.amount));
      }
    },
  };

  rotateAroundEyesHead = {
    amount: 0,
    entityY: undefined,
    cameraY: undefined,
    distX: -1,
    distZ: 1,
    rotations: 12,
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity, figure } = getEntityByHead(entityFollowers, 'OgenHoofd');
        if (!this.entityY) {
          this.entityY = figure.head.position.clone()
            .multiply(figure.group.scale)
            .add(figure.group.position)
            .add(new THREE.Vector3(0, 0.3, 0));
          this.cameraY = this.entityY.clone().add(new THREE.Vector3(0, 0, 0));
        }
        const radians = ((time % patternDuration) / patternDuration) * Math.PI * 2 * this.rotations;
        const rotationX = Math.cos(radians);
        const rotationZ = Math.sin(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.distX, 0, rotationZ * this.distZ);
        camPosition.add(entity.position.clone().add(this.entityY).multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.clone().multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().add(this.cameraY).multiplyScalar(this.amount));
      }
    },
  };

  rotateAroundKienholzClockHead = {
    amount: 0,
    entityY: undefined,
    cameraY: undefined,
    distX: -2,
    distZ: 2,
    rotations: 24,
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const { entity, figure } = getEntityByHead(entityFollowers, 'KienholzKlokHoofd');
        if (!this.entityY) {
          this.entityY = figure.head.position.clone()
            .multiply(figure.group.scale)
            .add(figure.group.position)
            .add(new THREE.Vector3(0, 0.3, 0));
          this.cameraY = this.entityY.clone().add(new THREE.Vector3(0, 0, 0));
        }
        const radians = ((time % patternDuration) / patternDuration) * Math.PI * 2 * this.rotations;
        const rotationX = Math.cos(radians);
        const rotationZ = Math.sin(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.distX, 0, rotationZ * this.distZ);
        camPosition.add(entity.position.clone().add(this.entityY).multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.clone().multiplyScalar(this.amount));
        camLookAt.add(entity.position.clone().add(this.cameraY).multiplyScalar(this.amount));
      }
    },
  };

  verticalSquareFromAbove = {
    amount: 0,
    observationPosition: new THREE.Vector3(0, 35, 0.5),
    lookAt: new THREE.Vector3(0, 0, -0.5),
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        camPosition.add(this.observationPosition.clone().multiplyScalar(this.amount));
        camLookAt.add(this.lookAt.clone().multiplyScalar(this.amount));
      }
    },
  };

  veryFarAwayRotating = {
    amount: 0,
    dist: 23,
    observationPosition: new THREE.Vector3(0, 30, 0),
    lookAt: new THREE.Vector3(0, 0, 0),
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        const radians = ((time % patternDuration) / patternDuration) * Math.PI * -4;
        const rotationX = Math.cos(radians);
        const rotationZ = Math.sin(radians);
        const rotatedPosition = new THREE.Vector3(rotationX * this.dist, 0, rotationZ * this.dist);
        camPosition.add(this.observationPosition.clone().multiplyScalar(this.amount));
        camPosition.add(rotatedPosition.clone().multiplyScalar(this.amount));
        camLookAt.add(this.lookAt.clone().multiplyScalar(this.amount));
      }
    },
  };

  lookAtClocks = {
    amount: 0,
    observationPosition: new THREE.Vector3(15, GROUND_Y + 4, 15),
    lookAt: new THREE.Vector3(-15, 0, -15),
    // camPos: new THREE.Vector3(0, 0, 0),
    // lookAtPos: new THREE.Vector3(0, 0, 0),
    camPos: new THREE.Vector3(-13, 0, 0),
    lookAtPos: new THREE.Vector3(17, 0, 0),
    update: function (
      time: number,
      camPosition: THREE.Vector3,
      camLookAt: THREE.Vector3,
    ) {
      if (this.amount > 0) {
        // const pos1 = ((time * 2) % 60);
        // const pos2 = pos1 < 30 ? pos1 : 30 - (pos1 - 30);
        // this.camPos.x = -pos2;
        // this.lookAtPos.x = pos2;
        this.camPos.x -= 0.01;
        this.lookAtPos.x -= 0.01;
        camPosition.add(this.observationPosition.clone().add(this.camPos).multiplyScalar(this.amount));
        camLookAt.add(this.lookAt.clone().add(this.lookAtPos).multiplyScalar(this.amount));
      }
    },
  };

  // lookCloseAtTlaxcalaHeadWeirdlyUnprecise.amount = 1;

  // tweenCamScenes(timeline,
  //   stepDuration * 16 * 2,
  //   stepDuration * 16 * 2,
  //   lookCloseAtTlaxcalaHeadWeirdlyUnprecise,
  //   lookAtClocks,
  // );

  // tweenCamScenes(timeline,
  //   stepDuration * 16 * 10,
  //   stepDuration * 16 * 2,
  //   lookAtClocks,
  //   lookCloseAtEyeballHeadWeirdlyUnprecise,
  // );

  // return;

  lookCloseAtMexicanHeadWeirdlyUnprecise.amount = 1;

  tweenCamScenes(timeline,
    stepDuration * 16 * 6,
    stepDuration * 16 * 2,
    lookCloseAtMexicanHeadWeirdlyUnprecise,
    rotateAroundEyesHead,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 16,
    stepDuration * 16 * 2,
    rotateAroundEyesHead,
    observationPoint1,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 22,
    stepDuration * 16 * 2,
    observationPoint1,
    lookCloseAtTireClockHeadWeirdlyUnprecise,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 32,
    stepDuration * 16 * 2,
    lookCloseAtTireClockHeadWeirdlyUnprecise,
    rotateAroundKienholzClockHead,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 40,
    stepDuration * 16 * 2,
    rotateAroundKienholzClockHead,
    verticalSquareFromAbove,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 46,
    stepDuration * 16 * 2,
    verticalSquareFromAbove,
    lookCloseAtTlaxcalaHeadWeirdlyUnprecise,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 56,
    stepDuration * 16 * 2,
    lookCloseAtTlaxcalaHeadWeirdlyUnprecise,
    lookAtClocks,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 62,
    stepDuration * 16 * 2,
    lookAtClocks,
    lookCloseAtEyeballHeadWeirdlyUnprecise,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 72,
    stepDuration * 16 * 2,
    lookCloseAtEyeballHeadWeirdlyUnprecise,
    veryFarAwayRotating,
  );

  tweenCamScenes(timeline,
    stepDuration * 16 * 78,
    stepDuration * 16 * 2,
    veryFarAwayRotating,
    lookCloseAtMexicanHeadWeirdlyUnprecise,
  );
}

export function updateCamera(time: number) {
  if (!camera) {
    return;
  }

  const camPosition = new THREE.Vector3();
  const camLookAt = new THREE.Vector3();

  lookCloseAtEyeballHeadWeirdlyUnprecise.update(time, camPosition, camLookAt);
  lookCloseAtMexicanHeadWeirdlyUnprecise.update(time, camPosition, camLookAt);
  lookCloseAtTireClockHeadWeirdlyUnprecise.update(time, camPosition, camLookAt);
  lookCloseAtTlaxcalaHeadWeirdlyUnprecise.update(time, camPosition, camLookAt);
  observationPoint1.update(time, camPosition, camLookAt);
  observationPoint2.update(time, camPosition, camLookAt);
  rotateAroundEyesHead.update(time, camPosition, camLookAt);
  rotateAroundKienholzClockHead.update(time, camPosition, camLookAt);
  verticalSquareFromAbove.update(time, camPosition, camLookAt);
  veryFarAwayRotating.update(time, camPosition, camLookAt);
  lookAtClocks.update(time, camPosition, camLookAt);

  camera.lookAt(camLookAt);
  camera.position.copy(camPosition);
}

function tweenCamScenes(
  timeline: Timeline,
  start: number,
  duration: number,
  sceneA: CameraScene,
  sceneB: CameraScene,
) {
  timeline.add(createTween({
    delay: start,
    ease: 'sineInOut',
    duration: duration,
    onStart: () => {},
    onUpdate: (progress: number) => {
      sceneA.amount = 1 - progress;
      sceneB.amount = progress;
    },
    onComplete: () => {
      sceneA.amount = 0;
      sceneB.amount = 1;
    },
  }));
}
