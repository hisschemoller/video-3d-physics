import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import Figure, { FigureOptions, PartData } from './figure';
import { GROUND_Y } from './munt';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';

const heads: PartData[] = [];

const bodies: PartData[] = [];

const arms: PartData[] = [];

const legs: PartData[] = [];

const specialtyParts: { [key: string]: PartData } = {};

const figures: Figure[] = [];

const bodyPartIndices = {
  head: 0,
  leg: 0,
  body: 0,
};

/**
 * On a 20 x 20 ground area the group is situated at a random position at the left or right of the
 * plane but not more than one unit from the edges.
 */
function createRandomGroupPosition() {
  const isLeftOrRight = 1 - (2 * Math.round(Math.random()));
  const groupPosition = new THREE.Vector3(
    ((Math.random() * 3) - 9) * isLeftOrRight,
    GROUND_Y,
    Math.random() * 18 - 9,
  );
  return groupPosition;
}

/**
 * Create a figure part from an GLTF file and a color.
 */
function createGltfPart(
  modelName: string,
  texture: THREE.Texture | THREE.Texture[],
  gltf: GLTF,
  translateZ: number = 0,
  index?: number,
) {
  const mesh = (gltf.scene.getObjectByName(modelName) as THREE.Mesh).clone();
  mesh.userData.originalIndex = index;
  if (Array.isArray(texture)) {
    mesh.children.forEach((child, index) => {
      const mesh = child as THREE.Mesh;
      mesh.material = new THREE.MeshPhongMaterial({ map: texture[index] });
      mesh.castShadow = true;
      mesh.receiveShadow = true;
    });
  } else {
    mesh.material = new THREE.MeshPhongMaterial({ map: texture  });
    mesh.receiveShadow = true;
    mesh.castShadow = true;
  }

  const size = new THREE.Vector3();
  mesh.geometry.boundingBox?.getSize(size);
  mesh.name = modelName;

  return { mesh, size, translateZ };
}

/**
 * 
 */
function createTexture(texturePath: string) {
  const texture = new THREE.TextureLoader().load(texturePath, () => {
    texture.flipY = false;
    texture.colorSpace = THREE.SRGBColorSpace;
    texture.needsUpdate = true;
  });
  return texture;
}

/**
 * Initial setup.
 */
export async function initFigures(
  gltfCarMarisol: GLTF,
) {
  // TEXTURES
  const textureDriver= createTexture('../assets/projects/munt/car-driver.jpg');
  const textureWindshield = createTexture('../assets/projects/munt/car-windshield.jpg');
  const textureFigures = createTexture('../assets/projects/munt/figures.jpg');
  const textureFigures2 = createTexture('../assets/projects/munt/figures2.jpg');
  const textureFigures3 = createTexture('../assets/projects/munt/figures3.jpg');
  const textureFigures4 = createTexture('../assets/projects/munt/figures4.jpg');
  const textureFigures5 = createTexture('../assets/projects/munt/figures5.jpg');
  const textureStadsklok = createTexture('../assets/projects/munt/stadsklok-amsterdam.jpg');

  heads.push(createGltfPart('Driver', textureDriver, gltfCarMarisol));
  heads.push(createGltfPart('Hatman', textureWindshield, gltfCarMarisol));
  heads.push(createGltfPart('Girl', textureWindshield, gltfCarMarisol));
  heads.push(createGltfPart('Passenger', textureWindshield, gltfCarMarisol));
  heads.push(createGltfPart('Marisol', textureDriver, gltfCarMarisol));
  heads.push(createGltfPart('MarisolSphereHead', textureFigures, gltfCarMarisol));
  heads.push(createGltfPart('MexicanMask', textureFigures2, gltfCarMarisol));
  heads.push(createGltfPart('OgenHoofd', textureFigures2, gltfCarMarisol));
  heads.push(createGltfPart('EgyptischHoofd', textureFigures2, gltfCarMarisol));
  heads.push(createGltfPart('MeinckeNagyHoofd', textureFigures5, gltfCarMarisol));
  heads.push(createGltfPart('KienholzKlokHoofd', textureFigures5, gltfCarMarisol));
  heads.push(createGltfPart('KellyTireClockHead', textureFigures5, gltfCarMarisol));
  heads.push(createGltfPart('TlaxcalaHead', textureFigures5, gltfCarMarisol));
  heads.push(createGltfPart('OogHoofd', textureFigures5, gltfCarMarisol));

  bodies.push(createGltfPart('BlueBlazerBody', textureFigures, gltfCarMarisol, 0.01, 0));
  bodies.push(createGltfPart('AdidasGreenTrackBody', textureFigures, gltfCarMarisol, 0, 1));
  bodies.push(createGltfPart('TScotchSodaBody', textureFigures2, gltfCarMarisol, 0.03, 2));
  bodies.push(createGltfPart('TshirtLevisBody', textureFigures2, gltfCarMarisol, 0, 3));
  bodies.push(createGltfPart('THPinkShirtBody', textureFigures3, gltfCarMarisol, 0, 4));
  bodies.push(createGltfPart('UrbanClassicBody', textureFigures3, gltfCarMarisol, 0.05, 5));
  bodies.push(createGltfPart('LastBody', textureFigures3, gltfCarMarisol, 0.08, 6));
  bodies.push(createGltfPart('THPinkShirtBody', textureFigures4, gltfCarMarisol, 0, 7));
  bodies.push(createGltfPart('UrbanClassicBody', textureFigures4, gltfCarMarisol, 0.05, 8));
  bodies.push(createGltfPart('LastBody', textureFigures4, gltfCarMarisol, 0.08, 9));

  arms.push(createGltfPart('BlueBlazerArmLeft', textureFigures, gltfCarMarisol, 0.51));
  arms.push(createGltfPart('AdidasGreenTrackArm', textureFigures, gltfCarMarisol, 0.52));
  arms.push(createGltfPart('TScotchSodaArm', textureFigures2, gltfCarMarisol, 0.56));
  arms.push(createGltfPart('TshirtLevisArm', textureFigures2, gltfCarMarisol, 0.66));
  arms.push(createGltfPart('THPinkShirtArm', textureFigures3, gltfCarMarisol, 0.57));
  arms.push(createGltfPart('UrbanClassicArm', textureFigures3, gltfCarMarisol, 0.85));
  arms.push(createGltfPart('LastArm', textureFigures3, gltfCarMarisol, 0.62));
  arms.push(createGltfPart('THPinkShirtArm', textureFigures4, gltfCarMarisol, 0.57));
  arms.push(createGltfPart('UrbanClassicArm', textureFigures4, gltfCarMarisol, 0.85));
  arms.push(createGltfPart('LastArm', textureFigures4, gltfCarMarisol, 0.62));

  legs.push(createGltfPart('AdidasLeftLeg', textureFigures, gltfCarMarisol, 0.1));
  legs.push(createGltfPart('DockerGreyLeftLeg', textureFigures, gltfCarMarisol, 0.1));
  legs.push(createGltfPart('ShortsChecked', textureFigures, gltfCarMarisol, 0));
  legs.push(createGltfPart('BermudaCA', textureFigures2, gltfCarMarisol, 0));
  legs.push(createGltfPart('THWhiteLeftLeg', textureFigures3, gltfCarMarisol, 0));
  legs.push(createGltfPart('UrbanClassicLeg', textureFigures3, gltfCarMarisol, 0));
  legs.push(createGltfPart('LastLeg', textureFigures3, gltfCarMarisol, 0));
  legs.push(createGltfPart('THWhiteLeftLeg', textureFigures4, gltfCarMarisol, 0));
  legs.push(createGltfPart('UrbanClassicLeg', textureFigures4, gltfCarMarisol, 0));
  legs.push(createGltfPart('LastLeg', textureFigures4, gltfCarMarisol, 0));

  specialtyParts.leaderArmWithFlag = createGltfPart('BlueBlazerArmRightLeader', textureFigures, gltfCarMarisol, 0.51);
  specialtyParts.kienholzClockBigHand = createGltfPart('KienholzBigHand', textureFigures5, gltfCarMarisol);
  specialtyParts.kienholzClockSmallHand = createGltfPart('KienholzSmallHand', textureFigures5, gltfCarMarisol);
  specialtyParts.kellyTireClockBigHand = createGltfPart('KellyTireBigHand', textureFigures5, gltfCarMarisol);
  specialtyParts.kellyTireClockSmallHand = createGltfPart('KellyTireSmallHand', textureFigures5, gltfCarMarisol);
  specialtyParts.oogHoofdHaar = createGltfPart('OogHoofdHaar', textureFigures5, gltfCarMarisol);
  specialtyParts.oogHoofdOogbol = createGltfPart('OogHoofdOogbol', textureFigures5, gltfCarMarisol);
  specialtyParts.oogHoofdOog = createGltfPart('OogHoofdOog', textureFigures5, gltfCarMarisol);
  specialtyParts.tlaxcalaHeadEyes = createGltfPart('TlaxcalaHeadEyes', textureFigures5, gltfCarMarisol);
  specialtyParts.stadsklok = createGltfPart('Stadsklok', textureStadsklok, gltfCarMarisol);
  specialtyParts.stadsklokClockBigHand = createGltfPart('StadsklokBigHand', textureStadsklok, gltfCarMarisol);
  specialtyParts.stadsklokClockSmallHand = createGltfPart('StadsklokSmallHand', textureStadsklok, gltfCarMarisol);
}

/**
 * 
 */
export function createFigures(
  projectSettings: ProjectSettings,
) {
  // create the figures
  for (let i = 0, n = 40; i < n; i++) {
    figures.push(createRandomFigure(projectSettings));
  }

  const group1Position = createRandomGroupPosition();
  const group2Position = createRandomGroupPosition();
  
  figures.forEach((figure, index) => {
    if (index < 4) {
      figure.setPositions(group1Position);
    } else if (index >= 4 && index < 7) {
      figure.setPositions(group2Position);
    } else {
      figure.setPositions();
    }
  });
}

/**
 * 
 */
export function createRandomFigure(projectSettings: ProjectSettings) {
  const legIndex = getSemiRandomBodyPartIndex(legs, 'leg');
  const options = {
    legIndex,
    bodyIndex: legIndex === 5 ? legIndex : getSemiRandomBodyPartIndex(bodies, 'body'),
    headIndex: getSemiRandomBodyPartIndex(heads, 'head'),
  }
  return Figure.create(projectSettings, heads, bodies, legs, arms, options);
}

export function createSpecificFigure(
  projectSettings: ProjectSettings,
  options: FigureOptions,
) {
  return Figure.create(projectSettings, heads, bodies, legs, arms, options);
}

/**
 * 
 */
export function getFigurePartIndexByName(partType: 'head' | 'leg' | 'body', partName: string) {
  switch (partType) {
    case 'body':
      console.log(bodies[bodies.findIndex((b) => b.mesh.name === partName)].mesh.name);
      return bodies.findIndex((b) => b.mesh.name === partName);
    case 'head':
      return heads.findIndex((b) => b.mesh.name === partName);
    case 'leg':
      return legs.findIndex((b) => b.mesh.name === partName);
  }
}

/**
 * 
 */
export function getSemiRandomBodyPartIndex(heads: PartData[], index: 'head' | 'leg' | 'body') {
  if (bodyPartIndices[index] === 0) {
    shuffle(heads);
  }
  bodyPartIndices[index] = (bodyPartIndices[index] + 1) % heads.length;
  return bodyPartIndices[index];
}

/**
 * One off body parts.
 */
export function getSpecialtyPart(partName: string) {
  return specialtyParts[partName];
}

/**
 * Randomize array order.
 * The de-facto unbiased shuffle algorithm is the Fisher–Yates (aka Knuth) Shuffle.
 * https://stackoverflow.com/a/2450976
 */
function shuffle(array: any[]) {
  let currentIndex = array.length;

  // while there remain elements to shuffle...
  while (currentIndex != 0) {

    // pick a remaining element...
    let randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // and swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex],
    ];
  }
}
