import { THREE } from 'enable3d';
import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import { GROUND_SIZE } from './munt';

/**
 * createTestWall background.
 */
export async function createMuntTorenWall(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const { patternDuration } =  projectSettings;
  svgScale = GROUND_SIZE / 1920;

  const actor = await createActor(projectSettings, media.munt_8004_toren, {
      svg: { scale: svgScale, url: '../assets/projects/munt/munt_8004_toren.svg' },
      imageRect: { w: 1920, h: 766 },
      depth: 0.2,
      color: 0x5688cb,
      material: 'basic',
    });
  actor.addTween({
    delay: 0.1,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 52,
    videoStart: 0.1,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.setStaticPosition(getMatrix4({ x: GROUND_SIZE / -2, y: 13.5, z: GROUND_SIZE / 2, ry: Math.PI / 2 }));
  actor.getMesh().castShadow = false;
  actor.getMesh().receiveShadow = false;

  return actor;
}

/**
 * Reguliersbreestraat background.
 */
export async function createReguliersWall(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const { patternDuration } =  projectSettings;
  svgScale = GROUND_SIZE / 1920;

  const actor = await createActor(projectSettings, media.munt_7909_reguliers, {
      svg: { scale: svgScale, url: '../assets/projects/munt/munt_7909_reguliers.svg' },
      imageRect: { w: 1920, h: 784 },
      depth: 0.2,
      color: 0x5688cb,
      material: 'basic',
    });
  actor.addTween({
    delay: 0.1,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 70,
    videoStart: 0.1,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.setStaticPosition(getMatrix4({ x: GROUND_SIZE / 2, y: 13.9, z: GROUND_SIZE / -2, ry: Math.PI / -2 }));
  actor.getMesh().castShadow = false;
  actor.getMesh().receiveShadow = false;

  return actor;
}

/**
 * Rokin background.
 */
export async function createRokinWall(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const { patternDuration } =  projectSettings;
  svgScale = GROUND_SIZE / 1920;

  const actor = await createActor(projectSettings, media.munt_7914_europe, {
    svg: { scale: svgScale, url: '../assets/projects/munt/munt_7914_europe.svg' },
    imageRect: { w: 1920, h: 766 },
    depth: 0.2,
    color: 0x5688cb,
    material: 'basic',
  });
  actor.addTween({
    delay: 0.1,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 45,
    videoStart: 0.1,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.setStaticPosition(getMatrix4({ x: GROUND_SIZE / -2, y: 13.5, z: GROUND_SIZE / -2 }));
  actor.getMesh().castShadow = false;
  actor.getMesh().receiveShadow = false;

  return actor;
}

/**
 * Vijzelstraat background.
 */
export async function createVijzelstraatWall(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const { patternDuration } =  projectSettings;
  svgScale = GROUND_SIZE / 1920;

  const actor = await createActor(projectSettings, media.munt_8739_vijzelstraat, {
    svg: { scale: svgScale, url: '../assets/projects/munt/munt_8739_vijzelstraat.svg' },
    imageRect: { w: 1920, h: 766 },
    depth: 0.2,
    color: 0x5688cb,
    material: 'basic',
  });
  actor.addTween({
    delay: 0.1,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 45,
    videoStart: 0.1,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.setStaticPosition(getMatrix4({ x: GROUND_SIZE / 2, y: 13.5, z: GROUND_SIZE / 2, ry: Math.PI }));
  actor.getMesh().castShadow = false;
  actor.getMesh().receiveShadow = false;

  return actor;
}
