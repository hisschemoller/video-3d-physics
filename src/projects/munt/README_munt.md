# Munt

## Opslag van de gerenderde PNG sequence (december 2024)

Voor elk project bewaarde ik de gerenderde PNG's. Omdat de video's nu langer worden vind ik dat
teveel ruimte in beslag nemen. Met ingang van het Munt project - waar de PNG's zo'n 44GB zijn -
bewaar ik ze niet meer.

In plaats daarvan sla ik de originele gefilmde video's op bij het project. Daarmee kan ik een 
project weer tot leven brengen. Als de documentatie van het project tenminste precies genoeg is. 
Het kost veel meer tijd, maar in de praktijk komt het nauwelijks voor dat ik een project opnieuw 
moet renderen. De verticale video's voor de Demo festival inzending waren een test. Daar is het 
gelukt.


### Dit heb ik niet gebruikt

Een stukje video met lopend persoon uitknippen en dan losknippen uit de achtergrond. Dat vervolgens
repeteren als een doorlopende loop beweging.


### Three-steer

https://github.com/erosmarcon/three-steer

A basic steering behaviors library based on THREE.js. The term 'Steering Behaviors' refers to a set 
of common AI movement algorithms and was coined by Craig Reynolds in a paper published in 1999.

Herschreven door mij in Typescript, bestand `three-steer.ts`.

Tests geschreven in `scene-three-steer-test.ts`, te staren door het aan te zetten in 
`entry-client.ts`:

```typescript
import MainScene from '@app/mainscene';
import setup from '@app/app';
import scene from '@projects/munt/scene';
import sceneThreeSteerTest from '@projects/munt/scene-three-steer-test';

setup(sceneThreeSteerTest as unknown as MainScene);
```

## Mensen

### Man met hoed naar links

```bash
# Slice van 43 tot 45 seconden 
ffmpeg -ss 00:00:43.0 -i "Video eigen opnames/Munt 2024-07-22 7908.mov" -c copy -t 00:00:02.0 man-hoed-a.mov
# extract frame 1 as an image
ffmpeg -i "man-hoed-a.mov" -vf "select=eq(n\,1)" -vframes 1 "man-hoed-a.jpg"
# crop to only man
ffmpeg -i "man-hoed-a.mov" -filter:v "crop=1026:819:0:1080" "man-hoed-b.mov"
# green screen met https://app.runwayml.com/
# chromakey greenscreen naar transparente PNG's
ffmpeg -i "man-hoed-c.mp4" -vf "chromakey=0x00ff00:0.28:0.05" '/Volumes/Samsung_X5/munt_man_hoed_greenscreen/frames/frame_%05d.png'
# scale PNG's naar 25%, 1026 * 0.25 = 266 bij 819 * 0.25 = 205 preview size
ffmpeg -start_number 1 -i '/Volumes/Samsung_X5/munt_man_hoed_greenscreen/frames/frame_%05d.png' -vf scale=266:205 '/Volumes/Samsung_X5/munt_man_hoed_greenscreen/frames_preview/frame_%05d.png'
```

### Twee vrouwen naar rechts

```bash
# Slice van 52 tot 55 seconden 
ffmpeg -ss 00:00:52.0 -i "Video eigen opnames/Munt 2024-07-22 7908.mov" -c copy -t 00:00:03.0 vrouwenrechts-a.mov
# extract frame 1 as an image
ffmpeg -i "vrouwenrechts-a.mov" -vf "select=eq(n\,1)" -vframes 1 "vrouwenrechts-a.jpg"
# crop
ffmpeg -i "vrouwenrechts-a.mov" -filter:v "crop=1431:1080:2410:1080" "vrouwenrechts-b.mov"
# green screen met https://app.runwayml.com/
# chromakey greenscreen naar transparente PNG's
ffmpeg -i "vrouwenrechts-c.mp4" -vf "chromakey=0x00ff00:0.28:0.05" '/Volumes/Samsung_X5/munt_vrouwenrechts_greenscreen/frames/frame_%05d.png'
# scale PNG's naar 25%, 1280 * 0.25 = 320 bij 968 * 0.25 = 242 preview size
ffmpeg -start_number 1 -i '/Volumes/Samsung_X5/munt_vrouwenrechts_greenscreen/frames/frame_%05d.png' -vf scale=320:242 '/Volumes/Samsung_X5/munt_vrouwenrechts_greenscreen/frames_preview/frame_%05d.png'
```

### Man met krukken

```bash
# Slice van 51 tot 54 seconden 
ffmpeg -ss 00:00:51.0 -i "Video eigen opnames/Munt 2024-07-22 7908.mov" -c copy -t 00:00:03.0 mankrukken-a.mov
# extract frame 1 as an image
ffmpeg -i "mankrukken-a.mov" -vf "select=eq(n\,1)" -vframes 1 "mankrukken-a.jpg"
# crop
ffmpeg -i "mankrukken-a.mov" -filter:v "crop=794:654:704:1050" "mankrukken-b.mov"
# green screen met https://app.runwayml.com/
# chromakey greenscreen naar transparente PNG's
ffmpeg -i "mankrukken-c.mp4" -vf "chromakey=0x00ff00:0.28:0.05" '/Volumes/Samsung_X5/munt_mankrukken_greenscreen/frames/frame_%05d.png'
# scale PNG's naar 25%, 794 * 0.25 = 199 bij 654 * 0.25 = 164 preview size
ffmpeg -start_number 1 -i '/Volumes/Samsung_X5/munt_mankrukken_greenscreen/frames/frame_%05d.png' -vf scale=199:164 '/Volumes/Samsung_X5/munt_mankrukken_greenscreen/frames_preview/frame_%05d.png'
```

### Mannen witte shirts

```bash
# Slice van 18 tot 20 seconden 
ffmpeg -ss 00:00:18.0 -i "Video eigen opnames/Munt 2024-07-22 7909.mov" -c copy -t 00:00:02.0 mannenwitteshirts-a.mov
# extract frame 1 as an image
ffmpeg -i "mannenwitteshirts-a.mov" -vf "select=eq(n\,1)" -vframes 1 "mannenwitteshirts-a.jpg"
# crop w:h:x:y
ffmpeg -i "mannenwitteshirts-a.mov" -filter:v "crop=1545:976:0:1000" "mannenwitteshirts-b.mov"
# green screen met https://app.runwayml.com/
# chromakey greenscreen naar transparente PNG's
ffmpeg -i "mannenwitteshirts-c.mp4" -vf "chromakey=0x00ff00:0.28:0.05" '/Volumes/Samsung_X5/munt_mannenwitteshirts_greenscreen/frames/frame_%05d.png'
# scale PNG's naar 25%, 1280 * 0.25 = 320 bij 808 * 0.25 = 202 preview size
ffmpeg -start_number 1 -i '/Volumes/Samsung_X5/munt_mannenwitteshirts_greenscreen/frames/frame_%05d.png' -vf scale=320:202 '/Volumes/Samsung_X5/munt_mannenwitteshirts_greenscreen/frames_preview/frame_%05d.png'
```

### Dikke man

```bash
# Slice van 18 tot 20 seconden 
ffmpeg -ss 00:00:50.0 -i "Video eigen opnames/Munt 2024-07-22 7909.mov" -c copy -t 00:00:02.0 dikkeman-a.mov
# extract frame 1 as an image
ffmpeg -i "dikkeman-a.mov" -vf "select=eq(n\,1)" -vframes 1 "dikkeman-a.jpg"
# crop w:h:x:y
ffmpeg -i "dikkeman-a.mov" -filter:v "crop=682:841:0:1080" "dikkeman-b.mov"
# green screen met https://app.runwayml.com/
# chromakey greenscreen naar transparente PNG's
ffmpeg -i "dikkeman-c.mp4" -vf "chromakey=0x00ff00:0.28:0.05" '/Volumes/Samsung_X5/munt_dikkeman_greenscreen/frames/frame_%05d.png'
# scale PNG's naar 25%, 682 * 0.25 = 171 bij 840 * 0.25 = 210 preview size
ffmpeg -start_number 1 -i '/Volumes/Samsung_X5/munt_dikkeman_greenscreen/frames/frame_%05d.png' -vf scale=171:210 '/Volumes/Samsung_X5/munt_dikkeman_greenscreen/frames_preview/frame_%05d.png'
```

### Paar zonnebril pet

```bash
# Slice van 60 tot 62 seconden 
ffmpeg -ss 00:01:00.0 -i "Video eigen opnames/Munt 2024-07-22 7909.mov" -c copy -t 00:00:02.0 zonnebrilpet-a.mov
# extract frame 1 as an image
ffmpeg -i "zonnebrilpet-a.mov" -vf "select=eq(n\,1)" -vframes 1 "zonnebrilpet-a.jpg"
# crop w:h:x:y
ffmpeg -i "zonnebrilpet-a.mov" -filter:v "crop=1558:1016:0:1022" "zonnebrilpet-b.mov"
# green screen met https://app.runwayml.com/
# chromakey greenscreen naar transparente PNG's
ffmpeg -i "zonnebrilpet-c.mp4" -vf "chromakey=0x00ff00:0.28:0.05" '/Volumes/Samsung_X5/munt_zonnebrilpet_greenscreen/frames/frame_%05d.png'
# scale PNG's naar 25%, 1280 * 0.25 = 171 bij 836 * 0.25 = 209 preview size
ffmpeg -start_number 1 -i '/Volumes/Samsung_X5/munt_zonnebrilpet_greenscreen/frames/frame_%05d.png' -vf scale=320:209 '/Volumes/Samsung_X5/munt_zonnebrilpet_greenscreen/frames_preview/frame_%05d.png'
```

## Omgeving

### Vijzelstraat en Reguliersbreestraat links

Munt 2024-07-22 7909.mov\
3840 x 2160 px\
1:11

```bash
# video van 3840 x 2160 naar 1920 x 1080
ffmpeg -i 'Munt 2024-07-22 7909.mov' -vf scale=1920:1080 munt_7909.mov
# video naar png sequence
ffmpeg -i munt_7909.mov '/Volumes/Samsung_X5/munt_7909/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i 'munt_7909.mov' -vf scale=480:270 munt_7909_preview.mov
# convert preview to png sequence
ffmpeg -i munt_7909_preview.mov '/Volumes/Samsung_X5/munt_7909/frames_preview/frame_%05d.png'
```

### Diamond Factory en Reguliersbreestraat rechts

Munt 2024-07-22 7911.mov\
3840 x 2160 px\
0:33

```bash
# video van 3840 x 2160 naar 1920 x 1080
ffmpeg -i 'Munt 2024-07-22 7911.mov' -vf scale=1920:1080 munt_7911.mov
# video naar png sequence
ffmpeg -i munt_7911.mov '/Volumes/Samsung_X5/munt_7911/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i 'munt_7911.mov' -vf scale=480:270 munt_7911_preview.mov
# convert preview to png sequence
ffmpeg -i munt_7911_preview.mov '/Volumes/Samsung_X5/munt_7911/frames_preview/frame_%05d.png'
```

### l'Europe en Amstel

Munt 2024-07-22 7914.mov\
3840 x 2160 px\
0:46

```bash
# video van 3840 x 2160 naar 1920 x 1080
ffmpeg -i 'Munt 2024-07-22 7914.mov' -vf scale=1920:1080 munt_7914.mov
# video naar png sequence
ffmpeg -i munt_7914.mov '/Volumes/Samsung_X5/munt_7914/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i 'munt_7914.mov' -vf scale=480:270 munt_7914_preview.mov
# convert preview to png sequence
ffmpeg -i munt_7914_preview.mov '/Volumes/Samsung_X5/munt_7914/frames_preview/frame_%05d.png'
```

### Munt toren en Singel

Munt 2024-08-12 8004.mov\
1920 × 1080 px\
0:53

```bash
# video naar png sequence
ffmpeg -i 'Munt 2024-08-12 8004.mov' '/Volumes/Samsung_X5/munt_8004/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i 'Munt 2024-08-12 8004.mov' -vf scale=480:270 munt_8004_preview.mov
# convert preview to png sequence
ffmpeg -i munt_8004_preview.mov '/Volumes/Samsung_X5/munt_8004/frames_preview/frame_%05d.png'
```

### Vijzelstraat

Munt 2024-11-03 8739.mov\
1920 × 1080 px\
1:36

```bash
# video naar png sequence
ffmpeg -i 'Munt 2024-11-03 8739.mov' '/Volumes/Samsung_X5/munt_8739/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i 'Munt 2024-11-03 8739.mov' -vf scale=480:270 munt_8739_preview.mov
# convert preview to png sequence
ffmpeg -i munt_8739_preview.mov '/Volumes/Samsung_X5/munt_8739/frames_preview/frame_%05d.png'
```



## Blender & Three.js bones

* Blender Bones For Beginners - EASY
  * https://www.youtube.com/watch?v=gdOaUv0_TC8
* Blenders Armature For Beginners_The Basics To Get Started
  * https://www.youtube.com/watch?v=jbFjTFFwPsM


```bash
ffmpeg -i "Munt 2024-07-22 7911.mov" -vf "select=eq(n\,300)" -vframes 1 "munt-7911-frame-300.jpg"
ffmpeg -i "Munt 2024-08-12 8004.mov" -vf "select=eq(n\,300)" -vframes 1 "munt-8004-frame-300.jpg"
ffmpeg -i "Munt 2024-07-22 7914.mov" -vf "select=eq(n\,300)" -vframes 1 "munt-7914-frame-300.jpg"
```


## Blender face and head

* Make Low Poly Faces in Blender!
  * https://www.youtube.com/watch?v=1_BuspNnj_k
* The Ultimate Low Poly Hair Guide in Blender!
  * https://www.youtube.com/watch?v=xbz1ZUC3ywc


## Muziek

Etienne de Cercy - Prix Choc, Three Day Weekend
Contours - Ballofon Sketches

Nee, toch meer de Franse disco house. Het zonnige zorgeloze vrijetijdskleding de stad in gaan met
mooi weer in je t-shirtje. Hou met een beeetje latin percussie erbij.

Digitakt Minimal album nr. 5. Is nog niet bij een video gebruikt.

Aanpassingen

* Alle tien delen een eigen geluidskarakter.
* Paal 2 moet iets anders worden.

* Galm bij hoog in de lucht zweven.
* Paal 1 overgang.


## Video oog

mijn_oog_8776.mov van 0:55 tot 1:25

```bash
# extract frame 55 * 30 = 1650 as an image
ffmpeg -i "mijn_oog_8776.mov" -vf "select=eq(n\,1650)" -vframes 1 "mijn_oog_8776-frame-1650.jpg"
# tijd van 0:55 tot 1:25 
ffmpeg -ss 00:00:55.0 -i mijn_oog_8776.mov -c copy -t 00:00:30.0 mijn_oog_8776_a.mov
# crop w h x y
ffmpeg -i mijn_oog_8776_a.mov -filter:v "crop=352:352:700:307" mijn_oog_8776_b.mov
# video naar png sequence
ffmpeg -i mijn_oog_8776_b.mov 'mijn_oog_8776/frames/frame_%05d.png'
# scale to 25%, 254 * 0.25 = 88 (x 88)
ffmpeg -i 'mijn_oog_8776_b.mov' -vf scale=88:88 mijn_oog_8776_preview.mov
# preview naar png sequence
ffmpeg -i mijn_oog_8776_preview.mov 'mijn_oog_8776/frames_preview/frame_%05d.png'
```


## Render

```bash
# png to mp4 (vanaf index 1 met 30 FPS)
ffmpeg -framerate 30 -start_number 1 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p munt-render5-video-x1.mp4
```

* 108 BPM,
* 1 beat duurt 60 / 108 = 0.5555555555555556 sec.
* Animatie duurt 80 maten.
* Animatie duurt 80 * 4 = 320 beats.
* Animatie duurt 320 * 0.5555555555555556 = 177.77777777777777 sec.
* Animatie duurt 177.77777777777777 * 30 = 5333.333333333333 frames
* Twee keer de hele video is 10667 frames.
* De video duurt 10667 / 2 = 5333.5 frames.
* De eerste 3 frames zijn onbruikbaar.
* Frame 4 is het eerste frame.
* Frame 4 + 5333 = 5337 is het laatste frame.

### Twee video's uit één render

De video is in dubbele lengte plus 1 seconde gerendered, dus kunnen er twee enkele uit gehaald 
worden, vanaf frame 4 met een lengte van 5333 frames en vervolgens hetzelfde vanaf frame 5337.

```bash
# png to mp4 (vanaf index 4 met 30 FPS)
ffmpeg -framerate 30 -start_number 4 -i rendered/frame_%05d.png  -frames:v 5333 -f mp4 -vcodec libx264 -pix_fmt yuv420p munt-render5a-from-00004-frames-5333-video-x1.mp4
# png to mp4 (vanaf index 5337 met 30 FPS)
ffmpeg -framerate 30 -start_number 5337 -i rendered/frame_%05d.png  -frames:v 5333 -f mp4 -vcodec libx264 -pix_fmt yuv420p munt-render5b-from-05337-frames-5333-video-x1.mp4
```

### Twee video's aan elkaar

Daarna twee achter elkaar zetten, bijvoorbeeld de 1e van render 1 en de 2e van render 2.

```bash
# maak een tekstbestand met de lijst bestanden zoals hier:
cat munt_list_1.txt
file 'munt-render1a-from-00004-frames-5333-video-x1.mp4'
file 'munt-render2b-from-05337-frames-5333-video-x1.mp4'
# voeg samen tot 1 lange video
ffmpeg -f concat -safe 0 -i munt_list_1.txt -c copy munt-1a-en-2b-samen.mp4
# de twee voor de video's aangepaste audio bestanden samenvoegen
ffmpeg -i wouter_hisschemoller_-_munt_1a_-_2024-audio.wav -i wouter_hisschemoller_-_munt_2b_-_2024-audio.wav -filter_complex "[0:a][1:a]concat=n=2:v=0:a=1" wouter_hisschemoller_-_munt_1a_2b_-_2024-audio.wav
# video munt-1a-en-2b-samen.mp4 hernoemd naar wouter_hisschemoller_-_munt_1a_2b_-_2024-video.mp4
# video en audio samenvoegen
ffmpeg -i wouter_hisschemoller_-_munt_1a_2b_-_2024-video.mp4 -i wouter_hisschemoller_-_munt_1a_2b_-_2024-audio.wav -vcodec copy wouter_hisschemoller_-_munt_1a_2b_-_2024.mp4

ffmpeg -i munt-render1a-from-00004-frames-5333-video-x1.mp4 -i wouter_hisschemoller_-_munt_1a_-_2024-audio.wav -vcodec copy wouter_hisschemoller_-_munt_1a_-_2024.mp4
```

De inhoud van het tekstbestand is dus gewoon alleen dit:

```
file 'munt-render1a-from-00004-frames-5333-video-x1.mp4'
file 'munt-render2b-from-05337-frames-5333-video-x1.mp4'
```


### Render met de overgang tijdens het bovenaanzicht 

De overgang tussen twee versies van de video zou ook op het punt kunnen dat het stilstaande 
bovenaanzicht begint te bewegen. Dan zie je alleen plotseling de mensen veranderen.

* Dat punt is bij frame 03067.
* Het laatste frame is dan 03067 + 5333 = 8400.

```bash
# png to mp4 (vanaf index 3067 met 30 FPS)
ffmpeg -framerate 30 -start_number 3067 -i rendered/frame_%05d.png  -frames:v 5333 -f mp4 -vcodec libx264 -pix_fmt yuv420p munt-render5-from-03067-frames-5333-video-x1.mp4
```

Om bijvoorbeeld video 1 en 2 te combineren moeten er 3 achter elkaar gezet worden: 1-2-1. Daarna
moet zoveel van het begin en het eind afgehaald worden dat de eerste video weer bij zijn
oorspronkelijke eerste frame begint.

* Video's beginnen nu bij frame 3067 van de 5333.
* Er moeten dus 5333 - 3067 = 2266 frames van het begin afgehaald worden.
* En er moeten 3067 frames van het eind afgehaald worden.
* Vanaf het begin 2266 / 30 = 75.53333333333333 seconden.
* Met een lengte van (5333 * 2) / 30 = 355.53333333333336 seconden.

```bash
# maak een tekstbestand met de lijst bestanden zoals hier:
cat munt_list.txt
file 'munt-render1-from-03067-frames-5333-video-x1.mp4'
file 'munt-render2-from-03067-frames-5333-video-x1.mp4'
file 'munt-render1-from-03067-frames-5333-video-x1.mp4'
# voeg samen tot 1 lange video
ffmpeg -f concat -safe 0 -i munt_list.txt -c copy output_temp.mp4
# verwijder begin en eind
ffmpeg  -ss 00:01:15.533 -t 00:05:55.533 -i output_temp.mp4 -c:v copy munt-1-en-2-samen-video.mp4
```

## Muziek bij de video

* De video duurt 5333 frames.
* De video duurt 5333 / 30 = 177.76666666666668 seconden.

```bash
# repeat 2 times, audio
ffmpeg -stream_loop 1 -i '2024-12-07 Munt 009.wav' -c copy munt-audio-x2.wav
# video en audio samenvoegen
ffmpeg -i munt-1-en-2-samen-video.mp4 -i munt-audio-x2.wav -vcodec copy munt-1-en-2-samen.mp4
# video en audio samenvoegen
ffmpeg -i munt-1a-en-2b-samen-video.mp4 -i munt-audio-x2.wav -vcodec copy munt-1a-en-2b-samen.mp4
```
