import { THREE } from 'enable3d';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { createGround } from './drawings';
import { createMuntTorenWall, createReguliersWall, createRokinWall, createVijzelstraatWall } from './video-walls';

export const GROUND_Y = -2;
export const GROUND_SIZE = 34;
export const GROUND_FIGURES_SIZE = 30;

// Plane boundaries (do not cross)
export const BOUNDARIES = new THREE.Box3(
  new THREE.Vector3(-GROUND_FIGURES_SIZE / 2, 0, -GROUND_FIGURES_SIZE / 2),
  new THREE.Vector3(GROUND_FIGURES_SIZE / 2, 0, GROUND_FIGURES_SIZE / 2),
);

/**
 * Setup
 */
export default async function initMunt(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
) {
  const {
    scene, width, width3d,
  } = projectSettings;
  const svgScale = width3d / width;

  const group = new THREE.Group();
  group.position.set(0, GROUND_Y, 0);
  group.add(new THREE.AxesHelper());
  scene.add(group);

  const floor = await createGround(projectSettings, media);
  group.add(floor.getMesh());

  const muntWall = await createMuntTorenWall(projectSettings, media, svgScale);
  group.add(muntWall.getMesh());

  const rokinWall = await createRokinWall(projectSettings, media, svgScale);
  group.add(rokinWall.getMesh());

  const reguliersWall = await createReguliersWall(projectSettings, media, svgScale);
  group.add(reguliersWall.getMesh());

  const vijzelstraatWall = await createVijzelstraatWall(projectSettings, media, svgScale);
  group.add(vijzelstraatWall.getMesh());
}

export function createSky(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
) {
  const { scene } = projectSettings;

  const sky = new THREE.Mesh(
    new THREE.SphereGeometry(100, 25, 25, 0, Math.PI * 2, 0, Math.PI / 2),
    new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load((media.sky1024 as ImageData).imgSrc),
    }),
  );
  sky.material.side = THREE.BackSide;
  scene.add(sky);
}
