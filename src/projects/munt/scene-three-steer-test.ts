/* eslint-disable object-curly-newline */
import { THREE } from 'enable3d';
import MainScene from '@app/mainscene';
import { Entity, SteeringEntity } from './three-steer';
import { GROUND_FIGURES_SIZE } from './munt';
import createTimeline, { Timeline } from '@app/timeline';
import createTween from '@app/tween';

const BPM = 117;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 24;
const BEATS_PER_MEASURE = 4;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;

const ENTITY_SPREAD_OFFSET = GROUND_FIGURES_SIZE / 4;
const LEADER_IDLE_SECONDS = 8;
const LEADER_IDLE_TICKS = LEADER_IDLE_SECONDS * 60;

const NUM_FOLLOWERS = 10;
const NUM_CROSSERS = 30;
const ENTITY_RADIUS = 0.5; // radius only used in avoid() to determine mostThreatening
const AVOID_DISTANCE = 1; // only used in avoid() to determine entities ahead
const TOO_CLOSE_DISTANCE = 0.8; // only used in flock() to flle too close entities
const MAX_SPEED = 0.12; // maximum velocity of an entity
const MAX_FORCE = 0.01; // how strong an entity is pulled to the leader
const ARRIVAL_TRESHOLD = 2; // distance from a target considered to be close enough, so arrived
const SEPERATION_RADIUS = 1.7; // within this radius entities are too close
const MAX_SEPERATION = 1; // the force to flee too close entities
const LEADER_DISTANCE = 4; // distance to maintain from the leader?
const LEADER_IN_SIGHT_RADIUS = 20;

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  ball: THREE.Mesh | undefined;

  entity: SteeringEntity | undefined;

  entityLeader: SteeringEntity | undefined;

  entityFollowers: SteeringEntity[] = [];

  allEntities: SteeringEntity[] = [];

  boundaries: THREE.Box3 | undefined;

  path: THREE.Vector3[] = [];

  pathWayPointIndex: number = 1;

  leaderIdleCount: number = LEADER_IDLE_TICKS;

  zebraCrossers: ZebraCrosser[] = [];

  constructor() {
    super();

    this.width = 1920;
    this.canvasHeight = 1440;
    this.viewportHeight = 1440;
    this.width3d = 16;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 30;
    this.clearColor = 0x001133;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });
  }

  /**
   * Set the html viewport and canvas height. For adjusted horizon height.
   */
  adjustHorizon() {
    const canvasContainer = document.getElementById('canvas-container');
    const canvas = this.renderer.domElement;

    if (canvasContainer && canvas) {
      canvasContainer.style.height =  `${(this.viewportHeight / this.width) * 100}vw`;
      canvas.setAttribute('style', `${(this.canvasHeight / this.viewportHeight) * 100}% !important`);
    }
  }

  async create() {
    await super.create();

    this.renderer.setSize(this.width, this.canvasHeight);
  
    this.pCamera.fov = 45;
    this.pCamera.aspect = this.width / this.viewportHeight;
    this.pCamera.near = 1;
    this.pCamera.far = 500;
    this.pCamera.position.set(0, 30, 30);
    this.pCamera.lookAt(this.cameraTarget);
    this.pCamera.updateProjectionMatrix();

    // Floor
    const floorGeometry = new THREE.PlaneGeometry(GROUND_FIGURES_SIZE, GROUND_FIGURES_SIZE, 32);
    const floorMaterial = new THREE.MeshBasicMaterial({color: 0x666666, transparent: true, opacity: 0.8});
    const floor = new THREE.Mesh(floorGeometry, floorMaterial);
    floor.rotation.x = -Math.PI * .5;
    this.scene.add(floor);

    // Plane boundaries (do not cross)
    this.boundaries = new THREE.Box3(
      new THREE.Vector3(-GROUND_FIGURES_SIZE / 2, 0, -GROUND_FIGURES_SIZE / 2),
      new THREE.Vector3(GROUND_FIGURES_SIZE / 2, 0, GROUND_FIGURES_SIZE / 2),
    );

    // this.createArriveExample();
    this.createFollowLeaderExample();
    this.createZebracrossingExample();

    let isTweenRunning: boolean = false;
    this.timeline.add(createTween({
      delay: 0,
      duration: PATTERN_DURATION / 2,
      onStart: () => {
        if (!isTweenRunning) {
          isTweenRunning = true;
          console.log('onStart');
          this.zebraCrossers.forEach((zc) => zc.setTargetPosition(true));
        }
      },
      onUpdate: () => {},
      onComplete: () => {
        if (isTweenRunning) {
          isTweenRunning = false;
          console.log('onComplete');
          this.zebraCrossers.forEach((zc) => zc.setTargetPosition(false));
        }
      },
    }));
  
    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);

    // this.updateArriveExample();
    this.updateFollowLeaderExample();
    this.updateZebracrossingExample();
    
    super.updateAsync(time, delta);
  }

  createArriveExample() {
    // Ball
    const ballGeometry = new THREE.SphereGeometry(50, 32, 32);
    const ballMaterial = new THREE.MeshBasicMaterial({color: 0xBCFF00});
    this.ball = new THREE.Mesh(ballGeometry, ballMaterial);
    this.ball.position.set(Math.random() * (5000 - (-5000)) + (-5000) ,50,Math.random() * (5000 - (-5000)) + (-5000));
    this.scene.add(this.ball);

    // Entity Mesh
    const geometry = new THREE.BoxGeometry(100, 200, 50);
    const material = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, wireframe: true });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.setY(100);

    // Entity
    this.entity = new SteeringEntity(mesh);
    this.entity.position.set(Math.random() * (5000 - (-5000)) + (-5000) ,0,Math.random() * (5000 - (-5000)) + (-5000));
    this.entity.lookAtDirection = true;
    this.scene.add(this.entity);
  }

  updateArriveExample() {
    if (this.entity && this.ball && this.boundaries) {
      this.entity.arrive(this.ball.position);
      if (this.entity.lookAtDirection) {
        this.entity.lookWhereGoing(true);
      } else {
        this.entity.rotation.set(0, 0, 0)
      }
      this.entity.bounce(this.boundaries)
      this.entity.update();
    }
  }

  createFollowLeaderExample() {
    const geometry = new THREE.BoxGeometry(1, 2, 0.5);
  
    // Entity Mesh
    const materialLeader = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, wireframe: true });
    const meshLeader = new THREE.Mesh(geometry, materialLeader);
    meshLeader.position.setY(1);

    // LEADER PATH
    [[0, 0], [10, 10], [-14, 7], [-5, -11], [10, -6], [-8, 14], [-11, -5]].forEach(([x, z]) => {
      const wayPoint = new THREE.Vector3(x, 0, z);
      this.addFlag(wayPoint);
      this.path.push(wayPoint);
    });

    // LEADER
    this.entityLeader = new SteeringEntity(meshLeader);
    this.entityLeader.maxSpeed = MAX_SPEED;
    this.entityLeader.lookAtDirection = true;
    this.entityLeader.position.copy(this.path[0]);
    this.entityLeader.pathIndex = this.pathWayPointIndex;
    this.scene.add(this.entityLeader);
    this.allEntities.push(this.entityLeader);

    this.entityLeader.maxForce = MAX_FORCE;
    this.entityLeader.arrivalThreshold = ARRIVAL_TRESHOLD;
    this.entityLeader.inSightDistance = 1;
    this.entityLeader.radius = ENTITY_RADIUS;
    this.entityLeader.avoidDistance = AVOID_DISTANCE;
    this.entityLeader.tooCloseDistance = TOO_CLOSE_DISTANCE;

    // FOLLOWERS
    const materialFollower = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );

    for (let i = 0; i < NUM_FOLLOWERS; i++) {
      const mesh = new THREE.Mesh(geometry, materialFollower);
      mesh.position.setY(1);
      const entity = new SteeringEntity(mesh);
      entity.position.set(
        Math.random() * (ENTITY_SPREAD_OFFSET - -ENTITY_SPREAD_OFFSET) + -ENTITY_SPREAD_OFFSET,
        0,
        Math.random() * (ENTITY_SPREAD_OFFSET - -ENTITY_SPREAD_OFFSET) + -ENTITY_SPREAD_OFFSET,
      );
      entity.maxSpeed = MAX_SPEED;
      entity.maxForce = MAX_FORCE;
      entity.arrivalThreshold = ARRIVAL_TRESHOLD;
      entity.inSightDistance = 1;
      entity.radius = ENTITY_RADIUS;
      entity.avoidDistance = AVOID_DISTANCE;
      entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
      this.entityFollowers.push(entity);
      this.allEntities.push(entity);
      this.scene.add(entity);
    }
  }

  updateFollowLeaderExample() {
    if (this.entityLeader && this.boundaries) {

      // this.entityLeader.wander();

      if(this.path.length > 0) {
        if (this.leaderIdleCount < LEADER_IDLE_TICKS) {
          this.leaderIdleCount++;
          this.entityLeader.idle();
        } else {
          this.entityLeader.followPath(this.path, true, 1);
          this.entityLeader.lookWhereGoing(true);
        }

        // check for arrival at path waypoints
        if (this.entityLeader.pathIndex !== this.pathWayPointIndex) {
          this.pathWayPointIndex = this.entityLeader.pathIndex;
          this.leaderIdleCount = 0;
        }
      } else {
        this.entityLeader.idle();
        this.entityLeader.separation(
          this.allEntities,
          SEPERATION_RADIUS,
          MAX_SEPERATION,
        );
      }

      // FOLLOWERS
      this.entityFollowers.forEach((follower) => {
        follower.maxSpeed = MAX_SPEED;
        follower.maxForce = MAX_FORCE;
        follower.followLeader(
          this.entityLeader as Entity,
          this.allEntities,
          LEADER_DISTANCE,
          SEPERATION_RADIUS,
          MAX_SEPERATION,
          LEADER_IN_SIGHT_RADIUS,
          ARRIVAL_TRESHOLD,
        );
        follower.lookWhereGoing(true);
        follower.update();
        follower.bounce(this.boundaries as THREE.Box3);
      });

      this.entityLeader.update();
      this.entityLeader.bounce(this.boundaries)
    }
  }

  createZebracrossingExample() {
    // BALL
    const ballGeometry = new THREE.SphereGeometry(0.15, 8, 8);
    const ballMaterial = new THREE.MeshBasicMaterial({ color: 0xff6600 });
    const ballMesh = new THREE.Mesh(ballGeometry, ballMaterial);

    // ENTITY MESH
    const entityGeometry = new THREE.BoxGeometry(1, 2, 0.5);
    const entityMaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00, wireframe: true });
    const entityMesh = new THREE.Mesh(entityGeometry, entityMaterial);

    for (let i = 0, n = NUM_CROSSERS; i < n; i++) {
      const zebraCrosser = new ZebraCrosser(
        this.scene,
        entityMesh,
        ballMesh,
        !!Math.round(Math.random()),
      );
      this.allEntities.push(zebraCrosser.entity);
      this.zebraCrossers.push(zebraCrosser);
    }
    console.log(this.zebraCrossers[0].entity);
  }

  updateZebracrossingExample() {
    this.zebraCrossers.forEach((zebraCrosser) => {
      if (this.boundaries) {
        zebraCrosser.update(
          this.boundaries,
          this.allEntities,
        );
      }
    });
  }

  addBall() {
    const ballGeometry = new THREE.SphereGeometry(50, 32, 32);
    const ballMaterial = new THREE.MeshBasicMaterial({ color: 0xBCFF00 });
    const ball = new THREE.Mesh(ballGeometry, ballMaterial);
    ball.position.set(Math.random() * (5000 - (-5000)) + (-5000) ,50,Math.random() * (5000 - (-5000)) + (-5000));
    this.scene.add(ball);
  }

  addFlag(position: THREE.Vector3) {
    this.addArrow(position);
    const flagGeometry = new THREE.CylinderGeometry(0.1, 0.1, 2.2, 8);
    const flagMaterial = new THREE.MeshBasicMaterial({ color: 0xBCFF00 });
    const flag = new THREE.Mesh(flagGeometry, flagMaterial);
    flag.position.set(position.x, 1.5, position.z);
    this.scene.add(flag);
    // flags.push(flag)
  }

  addArrow(position: THREE.Vector3) {
    if (this.path.length > 0) {
      const lastPoint = this.path[this.path.length - 1];
      const start = lastPoint.clone();
      const distance = lastPoint.distanceTo(position);
      const direction = position.clone().sub(lastPoint).normalize();
      const arrow = new THREE.ArrowHelper(direction, start, distance, 0x999999, distance - 1, 1);
      this.scene.add(arrow);
      // arrows.push(arrow)
    }
  }
}

/**
 * De ZebraCrosser moeten bij create niet-overlappend neergezet worden?
 * Is dat wel zo, of neemt separation() dat voor zijn rekening?
 */
class ZebraCrosser {
  entity: SteeringEntity;

  ball: THREE.Mesh;

  positionLeft: THREE.Vector3;

  positionRight: THREE.Vector3;

  maxSpeed: number = MAX_SPEED - 0.05;

  isLeft: boolean;

  constructor(
    scene: THREE.Scene,
    entityMesh: THREE.Mesh,
    ballMesh: THREE.Mesh,
    isLeft: boolean,
  ) {
    this.isLeft = isLeft;

    entityMesh.position.setY(1);

    this.positionRight = new THREE.Vector3(
      (GROUND_FIGURES_SIZE / 2) - (Math.random() * 3),
      0,
      (GROUND_FIGURES_SIZE / 2) - (Math.random() * GROUND_FIGURES_SIZE),
    );

    this.positionLeft = this.positionRight.clone();
    this.positionLeft.x = ((GROUND_FIGURES_SIZE / 2) - (Math.random() * 3)) * -1;

    this.entity = new SteeringEntity(entityMesh.clone());
    this.entity.maxSpeed = this.maxSpeed;
    this.entity.maxForce = MAX_FORCE;
    this.entity.lookAtDirection = true;
    this.entity.inSightDistance = 2;
    this.entity.radius = ENTITY_RADIUS;
    this.entity.avoidDistance = AVOID_DISTANCE;
    this.entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
    this.entity.position.copy(isLeft ? this.positionLeft : this.positionRight);
    scene.add(this.entity);

    this.ball = ballMesh.clone();
    this.ball.position.copy(isLeft ? this.positionRight : this.positionLeft);
    scene.add(this.ball);
  }

  update(boundaries: THREE.Box3, allEntities: SteeringEntity[]) {
    this.entity.numSamplesForSmoothing = 50;
    this.entity.maxForce = MAX_FORCE;
  
    this.entity.maxSpeed = this.maxSpeed;
    this.entity.arrivalThreshold = ARRIVAL_TRESHOLD;
    this.entity.arrive(this.ball.position);

    this.entity.radius = ENTITY_RADIUS;
    this.entity.avoidDistance = AVOID_DISTANCE;
    this.entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
    this.entity.avoid(allEntities);
    this.entity.separation(allEntities, SEPERATION_RADIUS, MAX_SEPERATION);

    this.entity.lookWhereGoing(true);
    this.entity.update();
    this.entity.bounce(boundaries);
  }

  setTargetPosition(isAtStartPosition: boolean) {
    setTimeout(() => {
      if (isAtStartPosition) {
        this.ball.position.copy(this.isLeft ? this.positionRight : this.positionLeft);
      } else {
        this.ball.position.copy(this.isLeft ? this.positionLeft : this.positionRight);
      }
  
      const zDeviation = 3 - (Math.random() * 6);
      const halfSize = GROUND_FIGURES_SIZE / 2;
  
      this.ball.position.z = Math.max(-halfSize, Math.min(this.ball.position.z + zDeviation, halfSize));
    }, Math.random() * 4000);
  }
}
