import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import { GROUND_SIZE } from './munt';

/**
 * Weesperplein ground.
 */
export async function createGround(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
) {
  // svg of 1024px should be 30 3D-units.
  const svgScale = GROUND_SIZE / 2048;
  const actor = await createActor(projectSettings, media.groundDrawing, {
    svg: { scale: svgScale, url: '../assets/projects/munt/ground.svg' },
    imageRect: { w: 2048, h: 2048 },
    depth: 0.02,
    color: 0x666666,
  });
  actor.setStaticImage(0, 0);
  actor.setStaticPosition(getMatrix4({ x: GROUND_SIZE / -2, y: 0, z: GROUND_SIZE / -2 }));
  actor.setColor('#666666');
  actor.getMesh().rotation.x = Math.PI / -2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}