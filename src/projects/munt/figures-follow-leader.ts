import { THREE } from 'enable3d';
import { Entity, SteeringEntity } from './three-steer';
import { BOUNDARIES, GROUND_FIGURES_SIZE, GROUND_Y } from './munt';
import { ProjectSettings } from '@app/interfaces';
import { createRandomFigure, createSpecificFigure, getFigurePartIndexByName } from './figures';
import Figure from './figure';
import MainScene from '@app/mainscene';

const ENTITY_SPREAD_OFFSET = GROUND_FIGURES_SIZE / 4;
const LEADER_IDLE_SECONDS = 2;
const LEADER_IDLE_TICKS = LEADER_IDLE_SECONDS * 60;

const NUM_FOLLOWERS = 14;
export const ENTITY_RADIUS = 0.5; // radius only used in avoid() to determine mostThreatening
export const AVOID_DISTANCE = 1; // only used in avoid() to determine entities ahead
export const TOO_CLOSE_DISTANCE = 0.8; // only used in flock() to flle too close entities
export const MAX_SPEED = 0.09; // maximum velocity of an entity
export const MAX_FORCE = 0.01; // how strong an entity is pulled to the leader
export const ARRIVAL_TRESHOLD = 2; // distance from a target considered to be close enough, so arrived
export const SEPERATION_RADIUS = 1.7; // within this radius entities are too close
export const MAX_SEPERATION = 1; // the force to flee too close entities
const LEADER_DISTANCE = 4; // distance to maintain from the leader?
const LEADER_IN_SIGHT_RADIUS = 20;

export type EntityFigure = {
  entity: SteeringEntity;
  figure: Figure;
};

export const ALL_ENTITIES: SteeringEntity[] = [];

export class LeaderWithFollowers {

  scene3d: MainScene;

  entityFigureLeader: EntityFigure;

  figureLeader: Figure;

  entityLeader: SteeringEntity;

  entityFollowers: EntityFigure[] = [];

  path: THREE.Vector3[] = [];

  pathWayPointIndex: number = 1;

  leaderIdleCount: number = LEADER_IDLE_TICKS;

  constructor(
    projectSettings: ProjectSettings,
  ) {
    const { scene3d } = projectSettings;
    this.scene3d = scene3d;

    // LEADER PATH
    [[0, 0], [10, 10], [-14, 7], [-5, -11], [10, -6], [-8, 14], [-11, -5]].forEach(([x, z]) => {
      const wayPoint = new THREE.Vector3(x, 0, z);
      // this.addFlag(wayPoint);
      this.path.push(wayPoint);
    });

    // const geometry = new THREE.BoxGeometry(1, 2, 0.5);
  
    // Entity Mesh
    // const materialLeader = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, wireframe: true });
    // const meshLeader = new THREE.Mesh(geometry, materialLeader);
    // meshLeader.position.setY(GROUND_Y + 1);

    this.figureLeader = createSpecificFigure(projectSettings, {
      legIndex: getFigurePartIndexByName('head', 'EgyptischHoofd') || 8,
      bodyIndex: getFigurePartIndexByName('body', 'BlueBlazerBody') || 0,
      headIndex: 8,
      isLeader: true,
    });
    this.figureLeader.group.position.setY(GROUND_Y);
    this.figureLeader.group.rotateY(Math.PI / 2);

    // Entities
    this.entityLeader = new SteeringEntity(this.figureLeader.group);
    this.entityLeader.maxSpeed = MAX_SPEED;
    this.entityLeader.lookAtDirection = true;
    this.entityLeader.position.copy(this.path[0]);
    this.entityLeader.pathIndex = this.pathWayPointIndex;
    this.entityLeader.maxForce = MAX_FORCE;
    this.entityLeader.arrivalThreshold = ARRIVAL_TRESHOLD;
    this.entityLeader.inSightDistance = 1;
    this.entityLeader.radius = ENTITY_RADIUS;
    this.entityLeader.avoidDistance = AVOID_DISTANCE;
    this.entityLeader.tooCloseDistance = TOO_CLOSE_DISTANCE;
    scene3d.scene.add(this.entityLeader);

    ALL_ENTITIES.push(this.entityLeader);

    this.entityFigureLeader = { entity: this.entityLeader, figure: this.figureLeader };

    for (let i = 0; i < NUM_FOLLOWERS; i++) {
      const figureFollower = createRandomFigure(projectSettings);
      figureFollower.group.position.setY(GROUND_Y);
      figureFollower.group.rotateY(Math.PI / 2);
      const entity = new SteeringEntity(figureFollower.group);
      entity.position.set(
        Math.random() * (ENTITY_SPREAD_OFFSET - -ENTITY_SPREAD_OFFSET) + -ENTITY_SPREAD_OFFSET,
        GROUND_Y,
        Math.random() * (ENTITY_SPREAD_OFFSET - -ENTITY_SPREAD_OFFSET) + -ENTITY_SPREAD_OFFSET,
      );
      entity.maxSpeed = MAX_SPEED;
      entity.maxForce = MAX_FORCE;
      entity.arrivalThreshold = ARRIVAL_TRESHOLD;
      entity.inSightDistance = 1;
      entity.radius = ENTITY_RADIUS;
      entity.avoidDistance = AVOID_DISTANCE;
      entity.tooCloseDistance = TOO_CLOSE_DISTANCE;
      this.entityFollowers.push({ entity, figure: figureFollower });
      ALL_ENTITIES.push(entity);
      scene3d.scene.add(entity);
    }
  }

  update() {
    if (this.entityLeader) {
      
      // this.entityLeader.wander();

      if (this.path.length > 0) {
        if (this.leaderIdleCount < LEADER_IDLE_TICKS) {
          this.leaderIdleCount++;
          this.entityLeader.idle();
        } else {
          this.entityLeader.followPath(this.path, true, 1);
          this.entityLeader.lookWhereGoing(true);
        }

        // check for arrival at path waypoints
        if (this.entityLeader.pathIndex !== this.pathWayPointIndex) {
          this.pathWayPointIndex = this.entityLeader.pathIndex;
          this.leaderIdleCount = 0;
        }
      } else {
        this.entityLeader.idle();
        this.entityLeader.separation(
          ALL_ENTITIES,
          SEPERATION_RADIUS,
          MAX_SEPERATION,
        );
      }
  
      this.figureLeader.updateLimbs(this.entityLeader.velocity.length());

      this.entityFollowers.forEach((follower) => {
        follower.entity.numSamplesForSmoothing = 50;
        follower.entity.maxSpeed = MAX_SPEED;
        follower.entity.maxForce = MAX_FORCE;
        follower.entity.followLeader(
          this.entityLeader as Entity,
          ALL_ENTITIES,
          LEADER_DISTANCE,
          SEPERATION_RADIUS,
          MAX_SEPERATION,
          LEADER_IN_SIGHT_RADIUS,
          ARRIVAL_TRESHOLD,
        );
        follower.entity.lookWhereGoing(true);
        follower.entity.update();
        follower.entity.bounce(BOUNDARIES);
  
        follower.figure.updateLimbs(follower.entity.velocity.length());
      });

      this.entityLeader.update();
      this.entityLeader.bounce(BOUNDARIES);
    }
  }

  addFlag(position: THREE.Vector3) {
    // this.addArrow(position);
    const flagGeometry = new THREE.CylinderGeometry(0.05, 0.05, 3, 8);
    const flagMaterial = new THREE.MeshBasicMaterial({ color: 0xBCFF00, transparent: true, opacity: 0.7 });
    const flag = new THREE.Mesh(flagGeometry, flagMaterial);
    flag.position.set(position.x, GROUND_Y + 1.5, position.z);
    this.scene3d.scene.add(flag);
  }

  addArrow(position: THREE.Vector3) {
    if (this.path.length > 0) {
      const lastPoint = this.path[this.path.length - 1];
      const start = lastPoint.clone();
      const distance = lastPoint.distanceTo(position);
      const direction = position.clone().sub(lastPoint).normalize();
      const arrow = new THREE.ArrowHelper(direction, start, distance, 0x999999, distance - 1, 1);
      arrow.position.y = GROUND_Y;
      this.scene3d.scene.add(arrow);
    }
  }

  get leader() {
    return this.entityLeader;
  }
}
