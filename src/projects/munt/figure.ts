import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import { Mesh } from 'three';
import createTween from '@app/tween';
import { GROUND_Y } from './munt';
import { getSpecialtyPart } from './figures';

export type PartData = {
  mesh: THREE.Mesh;
  size: THREE.Vector3;
  translateZ: number;
};

export type FigureOptions = {
  legIndex: number;
  bodyIndex: number;
  headIndex: number;
  isLeader?: boolean;
}

export default class Figure {
  projectSettings: ProjectSettings;

  group: THREE.Group;

  leftLeg: THREE.Mesh;

  rightLeg: THREE.Mesh;

  leftArm: THREE.Mesh | undefined;

  rightArm: THREE.Mesh | undefined;

  body: Mesh;

  head: Mesh;

  options: FigureOptions;

  velocityTotal: number = 0;

  /**
   * Leg is positioned at leg height - leg translateZ.
   * Body is positioned at leg height + body translateZ.
   * Arm is positioned at leg height + arm translateZ.
   * Head is positioned at leg height + body height - body translateZ.
   */
  static create(
    projectSettings: ProjectSettings,
    heads: PartData[],
    bodies: PartData[],
    legs: PartData[],
    arms: PartData[],
    options: FigureOptions,
  ) {
    const { scene3d } = projectSettings;
    const {
      legIndex,
      bodyIndex,
      headIndex,
      isLeader,
    } = options;
    const scale = 1.35;

    const group = new THREE.Group();
    group.scale.set(scale, scale, scale);
    scene3d.scene.add(group);
    
    const leftLegData = legs[legIndex];
    const leftLeg = leftLegData.mesh.clone();
    leftLeg.position.set(0, leftLegData.size.y - leftLegData.translateZ, 0);
    group.add(leftLeg);

    const rightLeg = leftLegData.mesh.clone();
    rightLeg.position.copy(leftLeg.position);
    rightLeg.scale.z =-1;
    group.add(rightLeg);

    const bodyData = bodies[bodyIndex];
    const body = bodyData.mesh.clone();
    body.position.set(0, leftLegData.size.y + bodyData.translateZ, 0);
    group.add(body);

    const leftArmData = arms[body.userData.originalIndex];
    const leftArm = leftArmData.mesh.clone();
    leftArm.position.set(0, leftLegData.size.y + leftArmData.translateZ, 0);
    group.add(leftArm);

    const rightArm = isLeader ? getSpecialtyPart('leaderArmWithFlag').mesh.clone() : leftArmData.mesh.clone();
    rightArm.scale.z = isLeader ? 1 : -1;
    rightArm.position.copy(leftArm.position);
    group.add(rightArm);
  
    const headData = heads[headIndex];
    const head = headData.mesh.clone();
    head.position.set(0, leftLegData.size.y + bodyData.size.y - bodyData.translateZ, 0);
    group.add(head);

    return new Figure(projectSettings, group, leftLeg, rightLeg, leftArm, rightArm, body, head, options);
  }

  private constructor(
    projectSettings: ProjectSettings,
    group: THREE.Group,
    leftLeg: THREE.Mesh,
    rightLeg: THREE.Mesh,
    leftArm: THREE.Mesh | undefined,
    rightArm: THREE.Mesh | undefined,
    body: Mesh,
    head: Mesh,
    options: FigureOptions,
  ) {
    this.projectSettings = projectSettings;
    this.group = group;
    this.leftLeg = leftLeg;
    this.rightLeg = rightLeg;
    this.leftArm = leftArm;
    this.rightArm = rightArm;
    this.body = body;
    this.head = head;
    this.options = options;
  }

  setPositions(groupPosition: THREE.Vector3 | undefined = undefined) {
    const { patternDuration, timeline } = this.projectSettings;
    const direction = 1 - (2 * Math.round(Math.random()));

    let startPosition: THREE.Vector3;
    let endPosition: THREE.Vector3;
  
    if (groupPosition) {
      // Members of a group move in and out of the center of the group.
      const xStartOffset = 0.5 + Math.random() * -1;
      const zStartOffset = 2 + Math.random() * -4;
      startPosition = groupPosition.clone();
      startPosition.x += xStartOffset;
      startPosition.z += zStartOffset;
      endPosition = groupPosition.clone();
      endPosition.x += (xStartOffset * 2);
      endPosition.z += (zStartOffset * 2);
    } else {
      // Individual figures just go randomly from left or right of the other way around.
      const z = Math.random() * 20 - 10;
      startPosition = new THREE.Vector3(
        ((Math.random() * 3) - 10) * direction,
        GROUND_Y,
        z,
      );
      endPosition = new THREE.Vector3(
        (10 - (Math.random() * 3)) * direction,
        GROUND_Y,
        z,
      );
    }

    const delay = 0.5 + Math.random() * 2;

    this.group.position.copy(startPosition);
    this.group.rotation.y = direction === 1 ? Math.PI : 0;

    timeline.add(createTween({
      delay,
      duration: patternDuration * 0.4,
      ease: 'sineInOut',
      onStart: () => {
        this.group.rotation.y = direction === 1 ? Math.PI : 0;
      },
      onUpdate: (progress) => {
        this.group.position.lerpVectors(startPosition, endPosition, progress);
        if (!groupPosition) {
          const zeroToFour = (progress * 30) % 4;

          const zeroToTwo = (progress * 30) % 2;
          const minusOneToOne = 1 - zeroToTwo;
          const oneToZeroToOne = Math.abs(minusOneToOne);
          const zeroToMinusOneToZero = 1 - oneToZeroToOne;
          const zeroToMinusOneToZeroToOneToZero = zeroToFour >= 2 ? zeroToMinusOneToZero : zeroToMinusOneToZero * -1;

          this.leftLeg.rotation.z = zeroToMinusOneToZeroToOneToZero * 0.7;
          this.rightLeg.rotation.z = this.leftLeg.rotation.z * -1;

          if (this.leftArm) {
            this.leftArm.rotation.z = this.rightLeg.rotation.z;
          }
          if (this.rightArm) {
            this.rightArm.rotation.z = this.options.isLeader ? this.leftLeg.rotation.z / 10 : this.leftLeg.rotation.z;
          }
        }
      },
      onComplete: () => {
        this.group.rotation.y = direction === 1 ? 0 : Math.PI;
      },
    }));

    timeline.add(createTween({
      delay: delay + (patternDuration * 0.5),
      duration: patternDuration * 0.4,
      ease: 'sineInOut',
      onStart: () => {
        this.group.rotation.y = direction === 1 ? 0 : Math.PI;
      },
      onUpdate: (progress) => {
        this.group.position.lerpVectors(endPosition, startPosition, progress);
        if (!groupPosition) {
          const zeroToFour = (progress * 30) %  4;

          const zeroToTwo = (progress * 30) %  2;
          const minusOneToOne = 1 - zeroToTwo;
          const oneToZeroToOne = Math.abs(minusOneToOne);
          const zeroToMinusOneToZero = 1 - oneToZeroToOne;
          const zeroToMinusOneToZeroToOneToZero = zeroToFour >= 2 ? zeroToMinusOneToZero : zeroToMinusOneToZero * -1;

          this.leftLeg.rotation.z = zeroToMinusOneToZeroToOneToZero * 0.7;
          this.rightLeg.rotation.z = this.leftLeg.rotation.z * -1;

          if (this.leftArm) {
            this.leftArm.rotation.z = this.rightLeg.rotation.z;
          }
          if (this.rightArm) {
            this.rightArm.rotation.z = this.leftLeg.rotation.z;
          }
        }
      },
      onComplete: () => {
        this.group.rotation.y = direction === 1 ? Math.PI : 0;
      },
    }));
  }

  updateLimbs(velocity: number) {
    const progress = (this.velocityTotal % 15) / 15;
    this.velocityTotal += velocity;

    const zeroToFour = (progress * 30) %  4;

    const zeroToTwo = (progress * 30) %  2;
    const minusOneToOne = 1 - zeroToTwo;
    const oneToZeroToOne = Math.abs(minusOneToOne);
    const zeroToMinusOneToZero = 1 - oneToZeroToOne;
    const zeroToMinusOneToZeroToOneToZero = zeroToFour >= 2 ? zeroToMinusOneToZero : zeroToMinusOneToZero * -1;

    this.leftLeg.rotation.z = zeroToMinusOneToZeroToOneToZero * 0.5;
    this.rightLeg.rotation.z = this.leftLeg.rotation.z * -1;

    if (this.leftArm) {
      this.leftArm.rotation.z = this.rightLeg.rotation.z;
    }
    if (this.rightArm) {
      this.rightArm.rotation.z = this.leftLeg.rotation.z;
    }
  }
}
