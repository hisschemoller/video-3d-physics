/* eslint-disable no-param-reassign */
/* eslint-disable object-curly-newline */
import { ExtendedMesh, ExtendedObject3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { ProjectSettings } from '@app/interfaces';
// import { createActor } from '@app/actor';
// import { getMatrix4 } from '@app/utils';

// async function createFlatGround(
//   projectSettings: ProjectSettings,
// ) {
//   const { scene3d } = projectSettings;
//   const actor = await createActor(projectSettings, undefined, {
//     box: { w: 240, h: 70, d: 0.1 },
//     imageRect: { w: 260, h: 309 },
//     depth: 0.005,
//     color: 0x565754,
//   });
//   actor.setStaticPosition(getMatrix4({ x: -120, y: 35 }));
//   actor.getMesh().castShadow = true;
//   actor.getMesh().receiveShadow = true;
//   const groundExObj = new ExtendedObject3D();
//   groundExObj.position.set(0, -2, -30);
//   groundExObj.rotation.set(Math.PI * 0.5, 0, 0);
//   groundExObj.add(actor.getMesh());
//   scene3d.scene.add(groundExObj);
//   scene3d.physics.add.existing(groundExObj, { mass: 0 });
// }

async function createFrisoKramerPaaltoparmatuur(
  projectSettings: ProjectSettings,
  gltfStraatlamp: GLTF,
  texture: THREE.Texture,
  glowTexture: THREE.Texture,
  x: number,
) {
  const scale = 1.7;
  const { scene3d } = projectSettings;
  const model = (gltfStraatlamp.scene.getObjectByName('Paaltoparmatuur') as ExtendedObject3D).clone();

  model.children.forEach((child, index) => {
    child.material = new THREE.MeshPhongMaterial({
      map: texture,
      transparent: index === 1,
      opacity: index === 1 ? 0.8 : 1,
      emissive: 0xffcc4b,
      emissiveIntensity: index === 1 ? 0.6 : 0,
    });
    child.castShadow = true;
    child.receiveShadow = true;
  });

  model.position.set(x, -1.8, -65);
  model.scale.set(scale, scale, scale);
  scene3d.scene.add(model);

  const radius = 1;
  const spriteMaterial = new THREE.SpriteMaterial({
    map: glowTexture,
    opacity: 0.2,
    color: 0xffcc4b,
    transparent: true,
    blending: THREE.AdditiveBlending,
  });
  const sprite = new THREE.Sprite(spriteMaterial);
  sprite.position.set(0, 5.85, 0);
  sprite.scale.set(radius * 8, radius * 8, 1.0);
  model.add(sprite);
}

async function createGround(
  projectSettings: ProjectSettings,
  gltfGround: GLTF,
) {
  const { scene3d } = projectSettings;

  const texture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/ground.jpg');
  texture.flipY = false;

  const model = (gltfGround.scene.getObjectByName('Ground') as ExtendedObject3D).clone();
  model.receiveShadow = true;
  model.position.set(0, -2, -30);
  model.material = new THREE.MeshPhongMaterial({ map: texture });
  scene3d.scene.add(model);
  scene3d.physics.add.existing(model, { mass: 0, shape: 'concaveMesh' });
}

// async function createSchutting(
//   projectSettings: ProjectSettings,
//   x: number,
// ) {
//   const { scene3d, width, width3d } = projectSettings;
//   const svgScale = width3d / width;
//   const scale = 4;

//   const actor = await createActor(projectSettings, {
//     height: 653,
//     imgSrc: '../assets/projects/leidseplein2/schutting.jpg',
//     width: 1024,
//   }, {
//     svg: {
//       url: '../assets/projects/leidseplein2/schutting1.svg',
//       scale: svgScale,
//     },
//     imageRect: { w: 1024, h: 293 },
//     depth: 0.005,
//     color: 0xb5b6b1,
//   });
//   actor.setStaticPosition(getMatrix4({
//     x, y: 7.5, z: -68, sx: scale, sy: scale, sz: scale,
//   }));
//   actor.setStaticImage(0, 22);
//   actor.getMesh().receiveShadow = true;
//   scene3d.scene.add(actor.getMesh());
// }

function createSky(
  projectSettings: ProjectSettings,
) {
  const { scene } = projectSettings;

  scene.background = new THREE.TextureLoader().load(
    '../assets/projects/leidseplein2/sky3.jpg',
  );
}

async function createWallBack(
  projectSettings: ProjectSettings,
) {
  const { scene3d } = projectSettings;

  const wall = new ExtendedMesh(
    new THREE.BoxBufferGeometry(240, 2, 0.2),
    new THREE.MeshPhongMaterial({ opacity: 0, transparent: true }),
  );
  wall.position.set(0, -1, -65);
  wall.receiveShadow = true;
  scene3d.scene.add(wall);
  scene3d.physics.add.existing(wall as unknown as ExtendedObject3D, { mass: 0 });
}

async function createWallFront(
  projectSettings: ProjectSettings,
) {
  const { scene3d } = projectSettings;

  const wall = new ExtendedMesh(
    new THREE.BoxBufferGeometry(240, 2, 0.2),
    new THREE.MeshPhongMaterial({ opacity: 0, transparent: true }),
  );
  wall.position.set(0, -1, 5);
  wall.receiveShadow = true;
  scene3d.scene.add(wall);
  scene3d.physics.add.existing(wall as unknown as ExtendedObject3D, { mass: 0 });
}

export default async function createBackground(
  projectSettings: ProjectSettings,
  gltfGround: GLTF,
  gltfStraatlamp: GLTF,
) {
  // createFlatGround(projectSettings);
  await createGround(projectSettings, gltfGround);
  // await createSchutting(projectSettings, 0);
  // await createSchutting(projectSettings, -30);
  createSky(projectSettings);
  createWallBack(projectSettings);
  createWallFront(projectSettings);

  const glowTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/friso-kramer-glow.jpg');
  const texture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/friso-kramer-paaltoparmatuur.jpg');
  texture.flipY = false;
  [-18, 0, 18].forEach((x) => (
    createFrisoKramerPaaltoparmatuur(projectSettings, gltfStraatlamp, texture, glowTexture, x)));
}
