/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
import {
  ExtendedMesh, ExtendedObject3D, Scene3D, THREE,
} from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { ProjectSettings } from '@app/interfaces';
import { createActor } from '@app/actor';
import { getMatrix4 } from '@app/utils';
import CarSimple from './car-simple';
import {
  getBoundingBoxSize, getFloor, getWheel, getWheelCylinder,
} from './utils';
import Vehicle from './vehicle';
import { createGlowLight, createStringOfGlowLights } from './lights';

export async function createTree(
  projectSettings: ProjectSettings,
  gltfTrees: GLTF,
  velocity: number,
  modelName: string,
  treeScale: number,
  treeRotation: number,
  wheelRadius: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const model = (gltfTrees.scene.getObjectByName(modelName) as ExtendedObject3D).clone();
  model.position.set(0, 0, 0);
  model.rotation.y = treeRotation;
  model.scale.set(treeScale, treeScale, treeScale);

  const texture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/bomen-textuur.jpg');
  texture.flipY = false;
  model.children.forEach((child) => {
    child.material = new THREE.MeshPhongMaterial({ map: texture });
    child.castShadow = true;
    child.receiveShadow = true;
  });

  // CAR
  const body = new THREE.Group();

  const floor = getFloor(model, 0x3d3927);
  body.add(floor);

  const { x: WIDTH, z: DEPTH } = getBoundingBoxSize(body);
  const HEIGHT = 0.2;

  const lightY = 3 + (Math.random() * 2);
  createStringOfGlowLights(
    projectSettings, body,
    8,
    new THREE.Vector3(0, lightY, 1),
    new THREE.Vector3(Math.random() * 3, lightY + Math.random() * 3, 11),
    2,
  );

  body.add(model);

  const wheel = getWheel(gltfTrees, 'WheelTree', wheelRadius, 'bomen-textuur');

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: modelName,
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.2, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: wheelRadius * 2,
    wheelHalfTrack: WIDTH * 0.37,
    wheelScale: 0.2,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createTree2(
  projectSettings: ProjectSettings,
  gltfTrees: GLTF,
  velocity: number,
  modelName: string,
  treeRotation: number,
  wheelRadius: number,
  z: number,
) {
  const { scene3d, width, width3d } = projectSettings;
  const svgScale = width3d / width;
  const scale = 1;
  const image = {
    height: 910,
    imgSrc: '../assets/projects/leidseplein2/boom2.jpg',
    width: 1024,
  };

  const treeGroup = new THREE.Group();
  treeGroup.rotation.y = treeRotation;

  {
    const actor = await createActor(projectSettings, image, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/boom2a.svg' },
      imageRect: { w: 449, h: 841 },
      depth: 0.05,
      color: 0x9d845c,
    });
    actor.setStaticPosition(getMatrix4({
      x: -1.7, y: 7, z: 0, sx: scale, sy: scale,
    }));
    actor.setStaticImage(21, 24);
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    treeGroup.add(actor.getMesh());
  }

  {
    const actor = await createActor(projectSettings, image, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/boom2b.svg' },
      imageRect: { w: 477, h: 819 },
      depth: 0.05,
      color: 0x9d845c,
    });
    actor.setStaticPosition(getMatrix4({
      x: 0, y: 6.9, z: 1.5, sx: scale, sy: scale, ry: Math.PI * 0.5,
    }));
    actor.setStaticImage(534, 42);
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    treeGroup.add(actor.getMesh());
  }

  // CAR
  const body = new THREE.Group();

  const floor = getFloor(treeGroup, 0x3d3927);
  body.add(floor);

  const { x: WIDTH, z: DEPTH } = getBoundingBoxSize(body);
  const HEIGHT = 0.2;

  const lightY = 3 + (Math.random() * 2);
  createStringOfGlowLights(
    projectSettings, body,
    8,
    new THREE.Vector3(0, lightY, 1),
    new THREE.Vector3(Math.random() * 3, lightY + Math.random() * 3, 11),
    2,
  );

  body.add(treeGroup);

  const wheel = getWheel(gltfTrees, 'WheelTree', wheelRadius, 'bomen-textuur');

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: modelName,
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.2, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: wheelRadius * 2,
    wheelHalfTrack: WIDTH * 0.37,
    wheelScale: 0.2,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createPaaltje(
  projectSettings: ProjectSettings,
  gltfPaaltje: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const wheelRadius = 0.055;

  const model = (gltfPaaltje.scene.getObjectByName('Verkeerspaaltje') as ExtendedObject3D).clone();

  const paalTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/verkeerspaaltje.jpg');
  paalTexture.flipY = false;
  model.material = new THREE.MeshPhongMaterial({ map: paalTexture });
  model.receiveShadow = true;
  model.castShadow = true;
  model.position.set(0, 0, 0);
  model.scale.set(0.5, 0.5, 0.5);

  // CAR
  const body = new THREE.Group();

  const floor = getFloor(model, 0x444444);
  body.add(floor);

  const { x: WIDTH, z: DEPTH } = getBoundingBoxSize(body);
  const HEIGHT = 0.2;

  body.add(createGlowLight(projectSettings, new THREE.Vector3(1, 7, 0), 0.1, 0xff884b));
  body.add(createGlowLight(projectSettings, new THREE.Vector3(-2, 5, -2), 0.05, 0xff6600));

  body.add(model);

  const wheel = getWheel(gltfPaaltje, 'WheelPaaltje', wheelRadius, 'verkeerspaaltje');

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Verkeerspaaltje',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.2, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: wheelRadius * 2,
    wheelHalfTrack: WIDTH * 0.2,
    wheelScale: 1.2,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createTaxi(
  projectSettings: ProjectSettings,
  gltfTaxi: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;

  const vehicle = new Vehicle(scene3d as unknown as Scene3D);
  const wheel = (gltfTaxi.scene.getObjectByName('Wheel') as ExtendedObject3D).clone();
  vehicle.createVehicle({
    axisPositionBack: -1.17,
    axisPositionFront: 1.55,
    body: (gltfTaxi.scene.getObjectByName('Body') as ExtendedObject3D).clone(),
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/leidseplein2/car-black-body-texture.jpg',
    maxEngineForce: velocity,
    name: 'Taxi',
    physicsBox: {
      width: 2, height: 1, depth: 4.6, y: 0.6, z: 0.15,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI / 2,
    scale: 2,
    wheelBack: wheel,
    wheelFront: wheel,
    wheelAxisHeightBack: 0.05,
    wheelAxisHeightFront: 0.05,
    wheelDiameterBack: 0.65,
    wheelDiameterFront: 0.65,
    wheelHalfTrack: 0.88,
    wheelScale: 0.9,
    wheelTexturePath: '../assets/projects/leidseplein2/car-black-wheel-texture.jpg',
  });

  vehicle.chassis.add(createGlowLight(projectSettings, new THREE.Vector3(1, 7, 0), 0.1, 0xff884b));
  vehicle.chassis.add(createGlowLight(projectSettings, new THREE.Vector3(-2, 5, -2), 0.05, 0xff6600));

  return vehicle;
}

export async function createTaxipaal(
  projectSettings: ProjectSettings,
  gltfTaxipaal: GLTF,
  gltfBuildings: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const wheelRadius = 0.055;

  const model = (gltfTaxipaal.scene.getObjectByName('Taxipaal') as ExtendedObject3D).clone();
  const paalTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/taxipaal.jpg');
  paalTexture.flipY = false;

  model.children.forEach((child) => {
    child.material = new THREE.MeshPhongMaterial({
      map: paalTexture,
    });
    child.castShadow = true;
    child.receiveShadow = true;
  });

  model.position.set(0, 0, 0);
  // model.scale.set(0.8, 0.8, 0.8);

  // CAR
  const body = new THREE.Group();

  const floor = getFloor(model, 0x444444);
  body.add(floor);

  const { x: WIDTH, z: DEPTH } = getBoundingBoxSize(body);
  const HEIGHT = 0.2;

  body.add(createGlowLight(projectSettings, new THREE.Vector3(1, 7, 0), 0.1, 0xff884b));
  body.add(createGlowLight(projectSettings, new THREE.Vector3(-2, 5, -2), 0.05, 0xff6600));

  body.add(model);

  // const wheel = getWheel(gltfBuildings, 'WheelPaaltje', wheelRadius, 'verkeerspaaltje');
  const wheel = getWheel(gltfBuildings, 'Wheel4', 0.2);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 10,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Taxipaal',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.2, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: wheelRadius * 2,
    wheelHalfTrack: WIDTH * 0.2,
    wheelScale: 1.2,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createTram(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  wheelRadius: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const model = (gltf.scene.getObjectByName('TramKop') as ExtendedObject3D).clone();

  const paalTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/tram-kop-texture.jpg');
  paalTexture.flipY = false;
  model.children[0].material = new THREE.MeshPhongMaterial({ map: paalTexture });
  model.children[1].material = new THREE.MeshPhongMaterial({ color: 0x555555 });
  model.position.set(0, -0.15, 0);
  model.scale.set(0.8, 0.8, 0.8);
  model.rotation.y = Math.PI * -0.5;

  model.children.forEach((child) => {
    // child.material = new THREE.MeshPhongMaterial({ map: texture });
    child.castShadow = true;
    child.receiveShadow = true;
  });

  // CAR
  const body = new THREE.Group();
  body.add(model);

  const { x: WIDTH, z: DEPTH } = getBoundingBoxSize(body);
  const HEIGHT = 0.2;

  const lightY = 5 + (Math.random() * 2);
  createStringOfGlowLights(
    projectSettings, body,
    8,
    new THREE.Vector3(0, lightY, 1),
    new THREE.Vector3(Math.random() * 3, lightY + Math.random() * 3, 15),
    4,
  );

  // const wheel = getWheel(gltfBuildings, wheelName, wheelRadius);
  // (wheel.material as MeshPhongMaterial).map = paalTexture;
  const wheel = getWheelCylinder(wheelRadius, 0x111111);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.3,
    axisPositionFront: DEPTH * 0.3,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Tram Kop',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.2, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: wheelRadius * 2,
    wheelHalfTrack: WIDTH * 0.2,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createVeegwagen(
  projectSettings: ProjectSettings,
  gltfVeegwagen: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;

  const body = (gltfVeegwagen.scene.getObjectByName('Body') as ExtendedObject3D).clone();

  ['veegwagen-body-texture.jpg', 'veegwagen-body-texture2.jpg'].forEach((texturefile, index) => {
    const texture = new THREE.TextureLoader().load(`../assets/projects/leidseplein2/${texturefile}`);
    texture.flipY = false;

    const child = body.children[index];
    child.material = new THREE.MeshPhongMaterial({
      map: texture,
    });
    child.position.set(0, 0, 0.4);
    child.rotateY(Math.PI * -0.5);
    child.castShadow = true;
    child.receiveShadow = true;
  });

  const brushScale = 1.1;
  const brushTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/veegwagen-body-texture2.jpg');
  brushTexture.flipY = false;

  const brushRight = (gltfVeegwagen.scene.getObjectByName('Brush') as ExtendedObject3D).clone();
  brushRight.material = new THREE.MeshPhongMaterial({
    map: brushTexture,
  });
  brushRight.position.set(-0.65, 0.3, 2.05);
  brushRight.scale.set(brushScale, brushScale, brushScale);
  body.add(brushRight);

  const brushLeft = (gltfVeegwagen.scene.getObjectByName('Brush') as ExtendedObject3D).clone();
  brushLeft.material = new THREE.MeshPhongMaterial({
    map: brushTexture,
  });
  brushLeft.position.set(0.65, 0.3, 2.05);
  brushLeft.scale.set(brushScale, brushScale, brushScale);
  body.add(brushLeft);

  const wheelBackGeometry = (gltfVeegwagen.scene.getObjectByName('WheelBack') as ExtendedObject3D).geometry.clone();
  wheelBackGeometry.rotateY(Math.PI * 0.5);
  const wheelBack = new ExtendedMesh(wheelBackGeometry, undefined) as unknown as ExtendedObject3D;

  const wheelFrontGeometry = (gltfVeegwagen.scene.getObjectByName('WheelFront') as ExtendedObject3D).geometry.clone();
  wheelFrontGeometry.rotateY(Math.PI * 0.5);
  const wheelFront = new ExtendedMesh(wheelFrontGeometry, undefined) as unknown as ExtendedObject3D;

  const lightY = 2 + (Math.random() * 1);
  createStringOfGlowLights(
    projectSettings, body,
    8,
    new THREE.Vector3(0, lightY, 1),
    new THREE.Vector3(Math.random() * 3, lightY + Math.random() * 3, 13),
    3,
  );

  const vehicle = new Vehicle(scene3d as unknown as Scene3D);
  vehicle.createVehicle({
    axisPositionBack: -0.68,
    axisPositionFront: 1.12,
    body,
    bodyMass: 1200,
    bodyTexturePath: '../assets/projects/leidseplein2/veegwagen-body-texture.jpg',
    maxEngineForce: velocity,
    name: 'Veegwagen',
    physicsBox: {
      width: 2, height: 1, depth: 4.4, x: 0, y: 0.6, z: 0.4,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI / 2,
    scale: 2,
    wheelBack,
    wheelFront,
    wheelAxisHeightBack: 0.09,
    wheelAxisHeightFront: 0.13,
    wheelDiameterBack: 0.684,
    wheelDiameterFront: 0.764,
    wheelHalfTrack: 0.88,
    wheelScale: 0.94,
    wheelTexturePath: '../assets/projects/leidseplein2/veegwagen-body-texture2.jpg',
  });

  return vehicle;
}

export async function createWc(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  wheelRadius: number,
  z: number,
  flipY: boolean,
) {
  const { scene3d } = projectSettings;
  const model = (gltf.scene.getObjectByName('WC') as ExtendedObject3D).clone();

  const geometry = model.geometry.clone();

  const wcTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/wc-texture.jpg');
  wcTexture.flipY = false;
  const material = new THREE.MeshPhongMaterial({ map: wcTexture });

  const mesh = new THREE.Mesh(geometry, material);
  mesh.receiveShadow = true;
  mesh.castShadow = true;
  mesh.position.set(0, 0, 0);
  mesh.scale.set(1, 1, 1);
  mesh.rotateY(flipY ? Math.PI : 0);

  // CAR
  const body = new THREE.Group();
  body.add(mesh);

  const { x: WIDTH, y: HEIGHT, z: DEPTH } = getBoundingBoxSize(body);

  const lightY = 4 + (Math.random() * 1);
  createStringOfGlowLights(
    projectSettings, body,
    8,
    new THREE.Vector3(0, lightY, 4),
    new THREE.Vector3(Math.random() * 3, lightY + Math.random() * 3, 15),
    4,
  );

  mesh.geometry.translate(0, HEIGHT * -0.5, 0);

  // const wheel = getWheel(gltfBuildings, wheelName, wheelRadius);
  // (wheel.material as MeshPhongMaterial).map = wcTexture;
  const wheel = getWheelCylinder(wheelRadius, 0x2c2f2f);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.3,
    axisPositionFront: DEPTH * 0.3,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'WC',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: 0, z: 0,
    },
    position: new THREE.Vector3(0, HEIGHT * 0.5 + 1, z),
    rotationY: Math.PI * 0.5,
    scale: 1,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: (HEIGHT * -0.5) + 0.05,
    wheelDiameter: wheelRadius * 2,
    wheelHalfTrack: WIDTH * 0.2,
    wheelScale: 2,
    wheelTexturePath: undefined,
  });

  return car;
}
