/* eslint-disable no-param-reassign */
import { ExtendedObject3D, Scene3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { ProjectSettings } from '@app/interfaces';
import createTween from '@app/tween';
import { createActor } from '@app/actor';
import { getMatrix4 } from '@app/utils';
import CarSimple from './car-simple';
import { getFloor, getWheel } from './utils';
import { createStringOfGlowLights } from './lights';

export async function createAmericain(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d, width, width3d } = projectSettings;
  const svgScale = width3d / width;
  const scaleFront = 2.9;
  const frontImage = {
    height: 1024,
    imgSrc: '../assets/projects/leidseplein2/americain-voor.jpg',
    width: 1024,
  };

  const group = new THREE.Group();

  {
    const actor = await createActor(projectSettings, frontImage, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/americain-voor-1.svg' },
      imageRect: { w: 735, h: 990 },
      depth: 0.05,
      color: 0x959691,
    });
    actor.setStaticPosition(getMatrix4({
      x: 0, y: 24, sx: scaleFront, sy: scaleFront,
    }));
    actor.setStaticImage(9, 13);
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    group.add(actor.getMesh());
  }

  {
    const actor = await createActor(projectSettings, frontImage, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/americain-voor-2.svg' },
      imageRect: { w: 529, h: 498 },
      depth: 0.05,
      color: 0x959691,
    });
    actor.setStaticPosition(getMatrix4({
      x: 4, y: 20, z: -2, sx: scaleFront, sy: scaleFront,
    }));
    actor.setStaticImage(180, 195);
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    group.add(actor.getMesh());
  }

  { // DAK
    const actor = await createActor(projectSettings, frontImage, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/americain-voor-3.svg' },
      imageRect: { w: 514, h: 267 },
      depth: 0.05,
      color: 0x959691,
    });
    actor.setStaticPosition(getMatrix4({
      x: 4.5, y: 18, z: -5.75, sx: scaleFront, sy: scaleFront, rx: -0.35,
    }));
    actor.setStaticImage(197, 216);
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    group.add(actor.getMesh());
  }

  { // ZIJMUUR
    const actor = await createActor(projectSettings, {
      height: 1024,
      imgSrc: '../assets/projects/leidseplein2/americain-zij.jpg',
      width: 1024,
    }, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/americain-zij-1.svg' },
      imageRect: { w: 974, h: 987 },
      depth: 0.05,
      color: 0x959691,
    });
    actor.setStaticPosition(getMatrix4({
      x: 16.0, y: 18.0, z: 0, sx: scaleFront * 0.75, sy: scaleFront * 0.75, ry: 1.2,
    }));
    actor.setStaticImage(7, 18);
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    group.add(actor.getMesh());
  }

  // CAR
  const color = 0x959691;
  const RADIUS = 0.8;

  const body = new THREE.Group();
  group.position.set(-7.5, 0, -8.8);
  group.rotation.y = Math.PI * -0.5;

  const floor = getFloor(group, color);
  body.add(floor);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.5;

  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-11, 7, 0), new THREE.Vector3(-11, 10, 25), 4,
  );

  body.add(group);

  const wheel = getWheel(gltf, 'Wheel1', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Americain',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 1.1,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createCity(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const model = (gltf.scene.getObjectByName('City') as ExtendedObject3D).clone();

  ['city-texture.jpg', 'city-2-texture.jpg'].forEach((texturefile, index) => {
    const texture = new THREE.TextureLoader().load(`../assets/projects/leidseplein2/${texturefile}`);
    texture.flipY = false;
    model.children[index].material = new THREE.MeshPhongMaterial({
      map: texture,
    });
  });
  model.children.forEach((child) => {
    child.castShadow = true;
    child.receiveShadow = true;
  });

  // CAR
  const color = 0x959691;
  const RADIUS = 0.3;

  const body = new THREE.Group();

  const floor = getFloor(model, color);
  body.add(floor);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.5;

  createStringOfGlowLights(
    projectSettings, body, 10, new THREE.Vector3(-4, 5, 0), new THREE.Vector3(-5, 7, 20), 4,
  );

  model.rotation.y = Math.PI;
  body.add(model);

  const wheel = getWheel(gltf, 'Wheel2', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'City Theater',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 3,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createHirsch(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const model = (gltf.scene.getObjectByName('Hirsch') as ExtendedObject3D).clone();

  const texture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/hirsch-texture.jpg');
  texture.flipY = false;
  model.children[0].material = new THREE.MeshPhongMaterial({
    map: texture,
  });
  model.children.forEach((child) => {
    child.castShadow = true;
    child.receiveShadow = true;
  });

  // const scale = 3.0;
  // model.position.set(-2, -2, z);
  // model.scale.set(scale, scale, scale);
  // model.rotateY(Math.PI / -1.5);
  // model.castShadow = true;
  // model.receiveShadow = true;
  // scene.add(model);

  // CAR
  const color = 0x959691;
  const RADIUS = 0.5;

  const body = new THREE.Group();

  model.rotation.y = Math.PI;

  const floor = getFloor(model, color);
  body.add(floor);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.5;

  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-11, 7, 0), new THREE.Vector3(-11, 10, 11), 2,
  );
  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-3, 5, 7), new THREE.Vector3(-3, 4, 16), 2,
  );

  body.add(model);

  const wheel = getWheel(gltf, 'Wheel3', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Hirsch Gebouw',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 1.5,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}

async function createParadisoClock(projectSettings: ProjectSettings, paradiso: ExtendedObject3D) {
  const { patternDuration, timeline } = projectSettings;

  const scale = 0.3;
  const group = new THREE.Group();
  group.position.set(0.92, 3.35, -0.01);
  group.rotation.y = Math.PI * 0.5;
  group.scale.set(scale, scale, scale);
  paradiso.add(group);

  // const faceRadius = 0.6;
  // const face = new THREE.Mesh(
  //   new THREE.CylinderBufferGeometry(faceRadius, faceRadius, 0.01, 16),
  //   new THREE.MeshPhongMaterial({ color: 0xeeeeee }),
  // );
  // face.rotateX(Math.PI * 0.5);
  // group.add(face);

  const handLargeWidth = 0.06;
  const handLargeHeight = 0.55;
  const handLargeGeometry = new THREE.BoxBufferGeometry(handLargeWidth, handLargeHeight, 0.01);
  handLargeGeometry.translate(0, handLargeHeight * 0.45, 0);
  const handLarge = new THREE.Mesh(
    handLargeGeometry,
    new THREE.MeshPhongMaterial({ color: 0x5d4f41 }),
  );
  handLarge.position.setZ(0.1);
  group.add(handLarge);

  timeline.add(createTween({
    delay: 0,
    duration: patternDuration * 0.99,
    ease: 'linear',
    onComplete: () => {},
    onStart: () => {},
    onUpdate: (progress: number) => {
      handLarge.rotation.z = progress * Math.PI * -8;
    },
  }));

  const handSmallWidth = 0.1;
  const handSmallHeight = 0.4;
  const handSmallGeometry = new THREE.BoxBufferGeometry(handSmallWidth, handSmallHeight, 0.01);
  handSmallGeometry.translate(0, handSmallHeight * 0.45, 0);
  const handSmall = new THREE.Mesh(
    handSmallGeometry,
    new THREE.MeshPhongMaterial({ color: 0x5d4f41 }),
  );
  handSmall.position.setZ(0.1);
  group.add(handSmall);

  timeline.add(createTween({
    delay: 0,
    duration: patternDuration * 0.99,
    ease: 'linear',
    onComplete: () => {},
    onStart: () => {},
    onUpdate: (progress: number) => {
      handSmall.rotation.z = progress * Math.PI * -2;
    },
  }));
}

export async function createParadiso(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const model = (gltf.scene.getObjectByName('Paradiso') as ExtendedObject3D).clone();

  ['paradiso-texture.jpg', 'paradiso-texture2.jpg'].forEach((texturefile, index) => {
    const texture = new THREE.TextureLoader().load(`../assets/projects/leidseplein2/${texturefile}`);
    texture.flipY = false;
    model.children[index].material = new THREE.MeshPhongMaterial({
      map: texture,
    });
  });

  model.children.forEach((child) => {
    child.castShadow = true;
    child.receiveShadow = true;
  });

  createParadisoClock(projectSettings, model);

  // CAR
  const RADIUS = 0.15;

  const body = new THREE.Group();
  model.position.set(0, 0, 0);
  model.rotation.y = Math.PI;
  body.add(model);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.2;

  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-2, 3, 0), new THREE.Vector3(-2, 4, 11), 2,
  );

  const wheel = getWheel(gltf, 'Wheel4', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Paradiso',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 5,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createSchouwburg(
  projectSettings: ProjectSettings,
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { scene3d } = projectSettings;
  const model = (gltf.scene.getObjectByName('Schouwburg') as ExtendedObject3D).clone();

  ['schouwburg-muur-texture.jpg', 'schouwburg-wit-texture.jpg'].forEach((texturefile, index) => {
    const texture = new THREE.TextureLoader().load(`../assets/projects/leidseplein2/${texturefile}`);
    texture.flipY = false;
    model.children[index].material = new THREE.MeshPhongMaterial({ map: texture });
  });

  model.children.forEach((child) => {
    child.castShadow = true;
    child.receiveShadow = true;
  });

  // CAR
  const color = 0x959691;
  const RADIUS = 0.3;

  const body = new THREE.Group();
  model.rotation.y = Math.PI;

  const floor = getFloor(model, color);
  body.add(floor);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.5;

  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-4, 6, 0), new THREE.Vector3(-5, 8, 11), 2,
  );

  body.add(model);

  const wheel = getWheel(gltf, 'Wheel5', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Stadsschouwburg',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2.5,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}
