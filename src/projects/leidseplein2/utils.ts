import { ExtendedMesh, ExtendedObject3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';

export const START_X = -30;

export function getBoundingBoxSize(body: THREE.Object3D) {
  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  return size;
}

export function getFloor(body: THREE.Object3D<THREE.Event>, color: number) {
  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: W, z: D } = size;

  const box = new THREE.Mesh(
    new THREE.BoxBufferGeometry(W + 0.1, 0.1, D + 0.1),
    new THREE.MeshPhongMaterial({ color }),
  );
  box.receiveShadow = true;
  box.castShadow = true;
  return box;
}

export function getTree(
  gltf: GLTF,
  name: string,
) {
  const geometry = (gltf.scene.getObjectByName(name) as THREE.Mesh).geometry.clone();
  geometry.rotateZ(Math.PI / 2);
  // geometry.scale(scale, scale, scale);
  const texture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/wheels.jpg');
  texture.flipY = false;
  const tree = new ExtendedMesh(
    geometry,
    new THREE.MeshPhongMaterial({ map: texture }),
  );
  return tree as unknown as ExtendedObject3D;
}

export function getWheel(
  gltf: GLTF,
  name: string,
  radius: number,
  textureName = 'wheels',
) {
  const wheelModelRadius = 0.15;
  const scale = radius / wheelModelRadius;
  const geometry = (gltf.scene.getObjectByName(name) as THREE.Mesh).geometry.clone();
  geometry.rotateZ(Math.PI / 2);
  geometry.scale(scale, scale, scale);
  const texture = new THREE.TextureLoader().load(`../assets/projects/leidseplein2/${textureName}.jpg`);
  texture.flipY = false;
  const wheel = new ExtendedMesh(
    geometry,
    new THREE.MeshPhongMaterial({ map: texture }),
  );
  return wheel as unknown as ExtendedObject3D;
}

export function getWheelCylinder(
  radius: number,
  color: number,
) {
  const wheel = new THREE.Mesh(
    new THREE.CylinderBufferGeometry(radius, radius, 0.1, 16),
    new THREE.MeshPhongMaterial({ color }),
  );
  // wheel.geometry.rotateZ(Math.PI * 0.5);
  return wheel;
}
