/* eslint-disable no-param-reassign */
import { ExtendedObject3D, Scene3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import VehicleBase from './vehicle-base';

function prepareWheel(
  wheel: ExtendedObject3D,
  wheelScale: number,
  wheelDiameter: number,
  wheelTexturePath?: string,
) {
  if (wheelTexturePath) {
    const wheelTexture = new THREE.TextureLoader().load(wheelTexturePath);
    wheelTexture.flipY = false;
    wheel.material = new THREE.MeshPhongMaterial({
      map: wheelTexture,
      shininess: 0,
    });
  }
  wheel.receiveShadow = true;
  wheel.castShadow = true;
  wheel.geometry.center();
  wheel.scale.set(wheelScale, wheelScale, wheelScale);

  const wheelRadius = (wheelDiameter / 2) * wheelScale;
  return wheelRadius;
}

export type VehicleProps = {
  axisPositionBack: number;
  axisPositionFront: number;
  body: ExtendedObject3D,
  bodyMass: number;
  bodyTexturePath: string | undefined;
  maxEngineForce: number;
  name: string;
  physicsBox: { [property: string]: any };
  position: THREE.Vector3;
  rotationY: number;
  scale?: number;
  wheelBack: ExtendedObject3D;
  wheelFront: ExtendedObject3D;
  wheelAxisHeightBack: number;
  wheelAxisHeightFront: number;
  wheelDiameterBack: number;
  wheelDiameterFront: number;
  wheelHalfTrack: number;
  wheelScale?: number;
  wheelTexturePath: string | undefined;
};

export default class Vehicle extends VehicleBase {
  name: string | undefined;

  rotationY: number;

  async createVehicle({
    axisPositionBack,
    axisPositionFront,
    body,
    bodyMass,
    bodyTexturePath,
    maxEngineForce,
    name,
    physicsBox,
    position,
    rotationY,
    scale = 1,
    wheelBack,
    wheelFront,
    wheelAxisHeightBack,
    wheelAxisHeightFront,
    wheelDiameterBack,
    wheelDiameterFront,
    wheelHalfTrack,
    wheelScale = 1,
    wheelTexturePath,
  }: VehicleProps) {
    this.name = name;
    this.rotationY = rotationY;

    // BODY
    if (bodyTexturePath) {
      const bodyTexture = new THREE.TextureLoader().load(bodyTexturePath);
      bodyTexture.flipY = false;
      body.material = new THREE.MeshPhongMaterial({
        map: bodyTexture,
        shininess: 1,
      });
    }
    body.receiveShadow = true;
    body.castShadow = true;
    body.position.copy(position);
    body.rotation.y = this.rotationY;
    body.scale.set(scale, scale, scale);
    this.scene3d.add.existing(body);
    this.physics.add.existing(body, {
      compound: [{
        mass: bodyMass, shape: 'box', ...physicsBox,
      }],
    });

    // WHEEL
    const wScale = wheelScale * scale;

    const wheelRadiusBack = prepareWheel(
      wheelBack,
      wScale,
      wheelDiameterBack,
      wheelTexturePath,
    );

    const wheelRadiusFront = prepareWheel(
      wheelFront,
      wScale,
      wheelDiameterFront,
      wheelTexturePath,
    );

    super.create(
      body,
      wheelBack,
      wheelFront,
      wheelRadiusBack,
      wheelRadiusFront,
      axisPositionBack * scale,
      axisPositionFront * scale,
      wheelHalfTrack * scale,
      wheelHalfTrack * scale,
      wheelAxisHeightBack * scale,
      wheelAxisHeightFront * scale,
    );

    const FRONT_LEFT = 0;
    const FRONT_RIGHT = 1;
    const BACK_LEFT = 2;
    const BACK_RIGHT = 3;
    const breakingForce = 0;
    const vehicleSteering = 0;
    // const engineForce = 0.01;
    // const maxEngineForce = 1;
    const engineForce = maxEngineForce / 8;
    // this.vehicle.applyEngineForce(engineForce, BACK_LEFT);
    // this.vehicle.applyEngineForce(engineForce, BACK_RIGHT);

    this.vehicle.applyEngineForce(engineForce, BACK_LEFT);
    this.vehicle.applyEngineForce(engineForce, BACK_RIGHT);

    this.vehicle.setBrake(breakingForce / 2, FRONT_LEFT);
    this.vehicle.setBrake(breakingForce / 2, FRONT_RIGHT);
    this.vehicle.setBrake(breakingForce, BACK_LEFT);
    this.vehicle.setBrake(breakingForce, BACK_RIGHT);

    this.vehicle.setSteeringValue(vehicleSteering, FRONT_LEFT);
    this.vehicle.setSteeringValue(vehicleSteering, FRONT_RIGHT);
  }

  activate() {
    this.active = true;
    this.chassis.body.setVelocity(0, 0, 0);
    this.chassis.body.setAngularVelocity(0, 0, 0);
    this.chassis.body.setCollisionFlags(0);
    this.chassis.visible = true;
    this.wheelMeshes.forEach((wheel) => {
      // wheel.body.setVelocity(0, 0, 0);
      // wheel.body.setAngularVelocity(0, 0, 0);
      // wheel.body.setCollisionFlags(0);
      wheel.visible = true;
    });
  }

  deactivate() {
    this.active = false;
    this.chassis.body.setVelocity(0, 0, 0);
    this.chassis.body.setAngularVelocity(0, 0, 0);
    this.chassis.body.setCollisionFlags(2);
    this.chassis.visible = false;
    this.wheelMeshes.forEach((wheel) => {
      // wheel.body.setVelocity(0, 0, 0);
      // wheel.body.setAngularVelocity(0, 0, 0);
      // wheel.body.setCollisionFlags(2);
      wheel.visible = false;
    });
  }

  reposition(x: number, z: number) {
    this.deactivate();

    const distance = x - this.chassis.position.x;
    this.chassis.position.y = 1;
    this.chassis.position.z = z;
    this.chassis.translateZ(distance);
    this.chassis.rotation.set(0, this.rotationY, 0);
    this.chassis.body.needUpdate = true;
  }

  getPosition() {
    return this.chassis.position;
  }
}

export function createTestVehicle(projectSettings: ProjectSettings) {
  const { scene3d } = projectSettings;
  const WIDTH = 2;
  const HEIGHT = 0.2;
  const DEPTH = 4;
  const RADIUS = 0.5;

  const body = new THREE.Mesh(
    new THREE.BoxBufferGeometry(WIDTH, HEIGHT, DEPTH),
    new THREE.MeshPhongMaterial({ color: 0xaa6600 }),
  );

  const wheel = new THREE.Mesh(
    new THREE.CylinderBufferGeometry(RADIUS, RADIUS, 0.1),
    new THREE.MeshPhongMaterial({ color: 0xff0000 }),
  );
  wheel.geometry.rotateZ(Math.PI * 0.5);

  const testVehicle = new Vehicle(scene3d as unknown as Scene3D);
  testVehicle.createVehicle({
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1,
    bodyTexturePath: undefined,
    maxEngineForce: 1,
    name: 'testVehicle',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: 0, z: 0,
    },
    position: new THREE.Vector3(0, 0, -10),
    rotationY: Math.PI * 0.5,
    scale: 1,
    wheelBack: wheel as unknown as ExtendedObject3D,
    wheelFront: wheel as unknown as ExtendedObject3D,
    wheelAxisHeightBack: 0,
    wheelAxisHeightFront: 0,
    wheelDiameterBack: RADIUS * 2,
    wheelDiameterFront: RADIUS * 2,
    wheelHalfTrack: WIDTH * 0.5,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return testVehicle;
}
