/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-param-reassign */
import { THREE } from 'enable3d';
import createTween from '@app/tween';
import { ProjectSettings } from '@app/interfaces';

function moveCamera(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;
  const CAM_X = cameraGroup.position.x;
  const CAM_Y = cameraGroup.position.y;
  const CAM_Z = cameraGroup.position.z;

  const tween = createTween({
    delay: 2,
    duration: patternDuration,
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.position.x = CAM_X - Math.sin(progress * Math.PI * 6) * 0.2;
      cameraGroup.position.y = CAM_Y - Math.sin(progress * Math.PI * 4) * 0.2;
      cameraGroup.position.z = CAM_Z + Math.sin(progress * Math.PI * 2) * 2;
    },
    onComplete: () => {},
  });
  timeline.add(tween);
}

function rotateCamera(
  projectSettings: ProjectSettings,
  cameraGroup: THREE.Group,
) {
  const { patternDuration, timeline } = projectSettings;

  const DELAY = 2;
  const DURATION = patternDuration / 4;
  const ROTATION = Math.PI * -0.1;

  timeline.add(createTween({
    delay: DELAY + (DURATION * 0),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = ROTATION * progress;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 1),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = ROTATION * (1 - progress);
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 2),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = -ROTATION * progress;
    },
    onComplete: () => {},
  }));

  timeline.add(createTween({
    delay: DELAY + (DURATION * 3),
    duration: DURATION,
    ease: 'sineInOut',
    onStart: () => {},
    onUpdate: (progress) => {
      cameraGroup.rotation.y = -ROTATION * (1 - progress);
    },
    onComplete: () => {},
  }));
}

export default function animateCamera(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
) {
  const { scene } = projectSettings;

  const cameraGroup = new THREE.Group();
  cameraGroup.add(camera);
  cameraGroup.position.set(0, 0, 0);
  scene.add(cameraGroup);

  moveCamera(projectSettings, cameraGroup);
  // rotateCamera(projectSettings, cameraGroup);
}
