/* eslint-disable object-curly-newline */
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import { createTweenGroup } from '@app/actor';
import { getMatrix4 } from '@app/utils';
import { updateTimeout } from '@app/timeout-world';
import createBackground from './background';
import { createBuildings, updateBuildings } from './buildings-manager';
import { createTraffic, updateTraffic } from './traffic-manager';
import { createTrafficFront, updateTrafficFront } from './traffic-manager-front';
import createLights from './lights';
import animateCamera from './camera';

const PROJECT_PREVIEW_SCALE = 0.5;
const BPM = 113.6;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 16;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  constructor() {
    super();

    this.width = 1920;
    this.height = 1920 + 480; // = 2400; 2400 / 1440 = 1.66, dus 166%
    this.width3d = 16;
    this.height3d = (this.height / this.width) * this.width3d;
    this.fps = 15;
    this.captureFps = 30;
    this.captureThrottle = 10;
    this.captureDuration = PATTERN_DURATION * 12;
    this.clearColor = 0x5178a7;
    this.shadowSize = 40;
  }

  async create() {
    await super.create();

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = 1920;
    this.captureCanvas.height = 1440;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();

    // this.renderer.autoClear = false;
    this.renderer.setSize(this.width, this.height);

    // DIRECTIONAL LIGHT
    this.directionalLight.intensity = 0.6; // 1; // 1.15;
    this.directionalLight.position.set(25, 50, -30);
    this.directionalLight.target.position.set(0, 0, -50);
    this.scene.add(this.directionalLight.target);

    // // AMBIENT LIGHT
    this.ambientLight.intensity = 0.2; // 0.4; // 0.7;

    // CAMERA
    this.pCamera.position.z -= 5;
    this.pCamera.lookAt(this.cameraTarget);

    // TWEENS
    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    // MEDIA
    const mediaVideo = {
      fps: 30,
      height: 480,
      scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
      width: 640,
    };
    const media = {
      video2: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/leidseplein2/leidseplein2-2_frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/leidseplein2-2/frames/&img=frame_#FRAME#.png',
      },
      video3: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/leidseplein2/leidseplein2-3_frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/leidseplein2-3/frames/&img=frame_#FRAME#.png',
      },
      video4: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/leidseplein2/leidseplein2-4_frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/leidseplein2-4/frames/&img=frame_#FRAME#.png',
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.height,
      height3d: this.height3d,
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene: this.scene,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    // BLENDER MODELS
    const gltfBuildings = await this.load.gltf('../assets/projects/leidseplein2/leidseplein2.glb');
    const gltfTrees = await this.load.gltf('../assets/projects/leidseplein2/leidseplein2-trees.glb');
    const gltfPaaltje = await this.load.gltf('../assets/projects/leidseplein2/verkeerspaaltje.glb');
    const gltfTram = await this.load.gltf('../assets/projects/leidseplein2/tram.glb');
    const gltfWc = await this.load.gltf('../assets/projects/leidseplein2/wc.glb');
    const gltfTaxi = await this.load.gltf('../assets/projects/leidseplein2/car-black.glb');
    const gltfVeegwagen = await this.load.gltf('../assets/projects/leidseplein2/veegwagen.glb');
    const gltfGround = await this.load.gltf('../assets/projects/leidseplein2/ground.glb');
    const gltfStraatlamp = await this.load.gltf('../assets/projects/leidseplein2/friso-kramer-paaltoparmatuur.glb');
    const gltfTaxipaal = await this.load.gltf('../assets/projects/leidseplein2/taxipaal.glb');

    const group = createTweenGroup(projectSettings);
    group.setStaticPosition(getMatrix4({ x: 0 }));

    createLights(projectSettings);
    animateCamera(projectSettings, this.pCamera);
    await createBackground(projectSettings, gltfGround, gltfStraatlamp);
    await createBuildings(projectSettings, media, gltfBuildings);
    await createTraffic(
      projectSettings,
      gltfTrees,
      gltfBuildings,
      gltfPaaltje,
      gltfTram,
      gltfWc,
      gltfTaxi,
      gltfVeegwagen,
      gltfTaxipaal,
    );
    await createTrafficFront(
      projectSettings,
      gltfTrees,
      gltfBuildings,
      gltfPaaltje,
      gltfTram,
      gltfWc,
      gltfTaxi,
      gltfVeegwagen,
      gltfTaxipaal,
    );

    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    updateTimeout(delta);
    updateBuildings();
    updateTraffic();
    updateTrafficFront();
    super.updateAsync(time, delta);
  }

  captureImage() {
    this.captureCanvasContext.drawImage(
      this.renderer.domElement, 0, 0, 1920, 1440, 0, 0, 1920, 1440,
    );
    return this.captureCanvas.toDataURL();
  }
}
