# Leidseplein 2

Leidseplein 2019-09-21 2.avi\
640 x 480\
3:05

```bash
# extract frame 390 as an image
ffmpeg -i 'Leidseplein 2019-09-21 2.avi' -vf "select=eq(n\,389)" -vframes 1 'Leidseplein 2019-09-21 2 frame 390.jpg'
# convert to png sequence
ffmpeg -i 'Leidseplein 2019-09-21 2.avi' '/Volumes/Samsung_X5/leidseplein2-2/frames/frame_%05d.png'
# scale to 50%, (320 x  240)
ffmpeg -i 'Leidseplein 2019-09-21 2.avi' -vf scale=320:240 leidseplein2-2_preview.mov
# convert preview to png sequence
ffmpeg -i leidseplein2-2_preview.mov '/Volumes/Samsung_X5/leidseplein2-2/frames_preview/frame_%05d.png'
```

Leidseplein 2019-09-21 3.avi\
640 x 480\
3:20

```bash
# convert to png sequence
ffmpeg -i 'Leidseplein 2019-09-21 3.avi' '/Volumes/Samsung_X5/leidseplein2-3/frames/frame_%05d.png'
# scale to 50%, (320 x  240)
ffmpeg -i 'Leidseplein 2019-09-21 3.avi' -vf scale=320:240 leidseplein2-3_preview.mov
# convert preview to png sequence
ffmpeg -i leidseplein2-3_preview.mov '/Volumes/Samsung_X5/leidseplein2-3/frames_preview/frame_%05d.png'
```

Leidseplein 2019-09-21 4.avi<br>
640 x 480<br>
3:05

```bash
# extract frame 100 as an image
ffmpeg -i 'Leidseplein 2019-09-21 4.avi' -vf "select=eq(n\,99)" -vframes 1 'Leidseplein 2019-09-21 4 frame 100.jpg'
# only the first 29 seconds
ffmpeg -ss 00:00:00.0 -i 'Leidseplein 2019-09-21 4.avi' -c copy -t 00:00:29.0 -an leidseplein2-4.avi
# convert to png sequence
ffmpeg -i leidseplein2-4.avi '/Volumes/Samsung_X5/leidseplein2-4/frames/frame_%05d.png'
# scale to 50%, (320 x  240)
ffmpeg -i leidseplein2-4.avi -vf scale=320:240 leidseplein2-4_preview.avi
# convert preview to png sequence
ffmpeg -i leidseplein2-4_preview.avi '/Volumes/Samsung_X5/leidseplein2-4/frames_preview/frame_%05d.png'
```

#### Test render 1

```bash
# png to mp4 (from index 0 met 30 FPS
ffmpeg -framerate 30 -start_number 0 -i /Volumes/Samsung_X5/leidseplein2-rendered-1/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p leidseplein2-rendered-1.mp4
```

Ammo.js raycastvehicle demo\
https://github.com/kripken/ammo.js/blob/main/examples/webgl_demo_vehicle/index.html
https://kripken.github.io/ammo.js/examples/webgl_demo_vehicle/index.html

### Low poly trees

* How to create a Low Poly Tree in Blender in 3 Minutes!
  * https://www.youtube.com/watch?v=P3WhEfdqXZY

## Muziek

Willeke Alberti - Als op 't Leidseplein
https://www.youtube.com/watch?v=RaFZN21SbiQ
