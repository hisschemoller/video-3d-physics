/* eslint-disable no-param-reassign */
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { ProjectSettings } from '@app/interfaces';
import { setWorldTimeout } from '@app/timeout-world';
import Vehicle from './vehicle';
import CarSimple from './car-simple';
import {
  createPaaltje,
  createTaxi,
  createTaxipaal,
  createTram,
  createTree,
  createTree2,
  createVeegwagen,
  createWc,
} from './traffic';
import { START_X } from './utils';

const cars: (Vehicle | CarSimple)[] = [];

let activeCars: (Vehicle | CarSimple)[] = [];

let queuedCars: (Vehicle | CarSimple)[] = [];

let nextCarIndex = 0;

const START_Z = -30;

function getNextCar() {
  const nextCar = cars[nextCarIndex];
  nextCarIndex = (nextCarIndex + 1) % cars.length;
  return nextCar;
}

function removeFromArray<A>(arr: A[], item: A) {
  return arr.reduce((accumulator, i) => (
    (i === item) ? accumulator : [...accumulator, i]), [] as A[]);
}

export async function createTraffic(
  projectSettings: ProjectSettings,
  gltfTrees: GLTF,
  gltfBuildings: GLTF,
  gltfPaaltje: GLTF,
  gltfTram: GLTF,
  gltfWc: GLTF,
  gltfTaxi: GLTF,
  gltfVeegwagen: GLTF,
  gltfTaxipaal: GLTF,
) {
  const defaultTreeArgs: [ProjectSettings, GLTF] = [
    projectSettings,
    gltfTrees,
  ];

  cars.push(await createWc(
    projectSettings,
    gltfWc,
    -3 - Math.random() * 3,
    0.2,
    START_Z,
    false,
  ));
  cars.push(await createTaxipaal(
    projectSettings,
    gltfTaxipaal,
    gltfBuildings,
    -3 - Math.random() * 3,
    START_Z,
  ));
  cars.push(await createTree(
    ...defaultTreeArgs,
    -3 - Math.random() * 5,
    'Boom1',
    0.2,
    Math.PI,
    0.4,
    START_Z,
  ));
  cars.push(await createTaxi(
    projectSettings,
    gltfTaxi,
    2,
    START_Z,
  ));
  cars.push(await createTree(
    ...defaultTreeArgs,
    -3 - Math.random() * 3,
    'Boom1',
    0.2,
    Math.PI,
    0.4,
    START_Z,
  ));
  cars.push(await createVeegwagen(
    projectSettings,
    gltfVeegwagen,
    1,
    START_Z,
  ));
  cars.push(await createPaaltje(
    projectSettings,
    gltfPaaltje,
    -3 - Math.random() * 3,
    START_Z,
  ));
  cars.push(await createTree2(
    ...defaultTreeArgs,
    -3 - Math.random() * 5,
    'Boom Getekend',
    Math.PI * 0.0,
    0.4,
    START_Z,
  ));
  cars.push(await createTree(
    ...defaultTreeArgs,
    -3 - Math.random() * 3,
    'Boom2',
    0.3,
    Math.PI * 0.5,
    0.3,
    START_Z,
  ));
  cars.push(await createTram(
    projectSettings,
    gltfTram,
    -3 - Math.random() * 4,
    0.25,
    START_Z,
  ));
  cars.push(await createTree(
    ...defaultTreeArgs,
    -3 - Math.random() * 5,
    'Boom1',
    0.25,
    0,
    0.25,
    START_Z,
  ));

  cars.forEach((car) => car.reposition(150, START_Z));

  [5, -15, -30].forEach((x) => {
    const car = getNextCar();
    car.reposition(x, START_Z);

    setWorldTimeout(() => {
      car.activate();
      activeCars.push(car);
    }, projectSettings.stepDuration * 8);
  });
}

export function updateTraffic() {
  let carToRemove: CarSimple | Vehicle | undefined;
  let leftMostCarX = -10;

  activeCars.forEach((car) => {
    const isToBeRemoved = car.update();

    leftMostCarX = Math.min(leftMostCarX, car.getPosition().x);

    // get cars to remove
    if (isToBeRemoved || (car.getPosition().x > 60 && car.active)) {
      carToRemove = car;
      carToRemove.reposition(120, START_Z);
    }
  });

  if (carToRemove) {
    activeCars = removeFromArray(activeCars, carToRemove);
    console.log(`#1 remove ${carToRemove.name}, ${activeCars.length}`);
  }

  queuedCars.forEach((queuedCar) => {
    leftMostCarX = Math.min(leftMostCarX, queuedCar.getPosition().x);
  });

  if (leftMostCarX > -15 && activeCars.length && !queuedCars.length) {
    const nextCar = getNextCar();
    nextCar.reposition(START_X, START_Z);
    queuedCars.push(nextCar);
    console.log(`#1 add ${nextCar.name}, ${activeCars.length}`);

    setWorldTimeout(() => {
      queuedCars = removeFromArray(queuedCars, nextCar);
      activeCars.push(nextCar);
      nextCar.activate();
    }, 0.1);
  }
}
