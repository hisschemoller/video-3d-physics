/* eslint-disable object-curly-newline */
import { ExtendedGroup, ExtendedMesh, ExtendedObject3D, Scene3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import { createActor } from '@app/actor';
import { getFloor, getWheel } from './utils';
import CarSimple from './car-simple';
import { createGlowLight, createStringOfGlowLights } from './lights';

export async function createHeinekenhoek(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { patternDuration, stepDuration, width, width3d, scene3d } = projectSettings;
  const svgScale = width3d / width;
  const color = 0x959691;
  const heinekenGroup = new THREE.Group();

  {
    const actorScale = 2.9;
    const actor = await createActor(projectSettings, media.video2, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/heinekenhoek2.svg' },
      imageRect: { w: 232, h: 305 },
      depth: 0.1,
      color,
    });
    actor.setStaticPosition(getMatrix4({
      x: 232 * -svgScale * actorScale,
      y: (305 * svgScale * actorScale) + 0.2,
      sx: actorScale,
      sy: actorScale,
    }));
    actor.addTween({
      delay: stepDuration * 1,
      duration: patternDuration * 0.99,
      videoStart: 0,
      fromImagePosition: new THREE.Vector2(407, 0),
    });
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    heinekenGroup.add(actor.getMesh());
  }

  {
    const actorScale = 2;
    const actor = await createActor(projectSettings, media.video4, {
      svg: { scale: svgScale, url: '../assets/projects/leidseplein2/heinekenhoek4.svg' },
      imageRect: { w: 260, h: 309 },
      depth: 0.1,
      color,
    });
    actor.setStaticPosition(getMatrix4({
      x: 0,
      y: (309 * svgScale * actorScale) + 0.2,
      sx: actorScale,
      sy: actorScale,
    }));
    actor.addTween({
      delay: stepDuration * 1,
      duration: patternDuration * 0.499,
      videoStart: 0,
      fromImagePosition: new THREE.Vector2(44, 0),
    });
    actor.addTween({
      delay: patternDuration * 0.5,
      duration: patternDuration * 0.499,
      videoStart: 0,
      fromImagePosition: new THREE.Vector2(44, 0),
    });
    actor.getMesh().castShadow = true;
    actor.getMesh().receiveShadow = true;
    heinekenGroup.add(actor.getMesh());
  }

  // CAR
  const RADIUS = 0.3;

  const body = new ExtendedGroup();

  const box = new ExtendedMesh(
    new THREE.BoxBufferGeometry(3, 0.1, 9.5),
    new THREE.MeshPhongMaterial({ color }),
  );
  box.receiveShadow = true;
  box.castShadow = true;
  body.add(box);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.5;

  body.add(createGlowLight(projectSettings, new THREE.Vector3(0, 10, 0), 0.1, 0xffcc4b));
  body.add(createGlowLight(projectSettings, new THREE.Vector3(-6, 7, -2), 0.05, 0xffa600));
  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-1, 5, -5.5), new THREE.Vector3(-9, 5, -15), 3,
  );

  heinekenGroup.position.set(0, 0, 0.6);
  heinekenGroup.rotation.y = Math.PI * -0.5;
  body.add(heinekenGroup);

  const wheel = getWheel(gltf, 'Wheel3', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'Heinekenhoek',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 2.5,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}

export async function createKlmBuilding(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  gltf: GLTF,
  velocity: number,
  z: number,
) {
  const { patternDuration, scene, scene3d, stepDuration, width, width3d } = projectSettings;
  const svgScale = width3d / width;

  // GROUP
  const klmGroup = new THREE.Group();
  scene.add(klmGroup);

  // ACTOR
  const actorScale = 8.1;
  const actor = await createActor(projectSettings, media.video3, {
    svg: { scale: svgScale, url: '../assets/projects/leidseplein2/klmgebouw.svg' },
    imageRect: { w: 327, h: 310 },
    depth: 0.01,
    color: 0xEAD8D6,
  });
  actor.setStaticPosition(getMatrix4({ x: -11, y: 21, z: 2.6, sx: actorScale, sy: actorScale }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.999,
    videoStart: 75,
    fromImagePosition: new THREE.Vector2(196, 14),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  klmGroup.add(actor.getMesh());

  // MODEL
  const modelScale = 2.5;
  const model = (gltf.scene.getObjectByName('Klm') as ExtendedObject3D).clone();
  model.position.set(0, 0, 0);
  model.scale.set(modelScale, modelScale, modelScale);
  model.rotateY(Math.PI * -0.5);
  model.castShadow = true;
  model.receiveShadow = true;
  klmGroup.add(model);

  // CAR
  const color = 0x959691;
  const RADIUS = 0.5;

  const body = new THREE.Group();

  const groupScale = 0.75;
  klmGroup.rotation.y = Math.PI * -0.5;
  klmGroup.scale.set(groupScale, groupScale, groupScale);

  const floor = getFloor(klmGroup, color);
  body.add(floor);

  const aabb = new THREE.Box3();
  aabb.setFromObject(body);
  const size = new THREE.Vector3();
  aabb.getSize(size);
  const { x: WIDTH, z: DEPTH } = size;
  const HEIGHT = 0.5;

  createStringOfGlowLights(
    projectSettings, body, 8, new THREE.Vector3(-5, 10, 0), new THREE.Vector3(-5, 14, 16), 4,
  );

  body.add(klmGroup);

  const wheel = getWheel(gltf, 'Wheel5', RADIUS);

  const car = new CarSimple(scene3d as unknown as Scene3D, {
    axisPositionBack: DEPTH * -0.5,
    axisPositionFront: DEPTH * 0.5,
    body: body as unknown as ExtendedObject3D,
    bodyMass: 1000,
    bodyTexturePath: undefined,
    maxEngineForce: velocity,
    name: 'KLM Gebouw',
    physicsBox: {
      width: WIDTH, height: HEIGHT, depth: DEPTH, x: 0, y: HEIGHT * 0.5, z: 0,
    },
    position: new THREE.Vector3(0, 0, z),
    rotationY: Math.PI * 0.5,
    scale: 1.3,
    wheel: wheel as unknown as ExtendedObject3D,
    wheelAxisHeight: 0,
    wheelDiameter: RADIUS * 2,
    wheelHalfTrack: (WIDTH * 0.5) - 0.25,
    wheelScale: 1,
    wheelTexturePath: undefined,
  });

  return car;
}
