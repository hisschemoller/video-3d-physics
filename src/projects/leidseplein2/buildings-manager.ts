/* eslint-disable no-param-reassign */
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { setWorldTimeout } from '@app/timeout-world';
import { createHeinekenhoek, createKlmBuilding } from './buildingvideos';
import {
  createAmericain, createCity, createHirsch, createParadiso, createSchouwburg,
} from './buildings';
import Vehicle from './vehicle';
import CarSimple from './car-simple';

const START_Z = -50;

const cars: (Vehicle | CarSimple)[] = [];

let activeCars: (Vehicle | CarSimple)[] = [];

let queuedCars: (Vehicle | CarSimple)[] = [];

let nextCarIndex = 0;

function getNextCar() {
  const nextCar = cars[nextCarIndex];
  nextCarIndex = (nextCarIndex + 1) % cars.length;
  return nextCar;
}

function removeFromArray<A>(arr: A[], item: A) {
  return arr.reduce((accumulator, i) => (
    (i === item) ? accumulator : [...accumulator, i]), [] as A[]);
}

export async function createBuildings(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  gltf: GLTF,
) {
  cars.push(await createHeinekenhoek(projectSettings, media, gltf, -3, START_Z));
  cars.push(await createHirsch(projectSettings, gltf, -3, START_Z));
  cars.push(await createSchouwburg(projectSettings, gltf, -3, START_Z));
  cars.push(await createKlmBuilding(projectSettings, media, gltf, -3, START_Z));
  cars.push(await createCity(projectSettings, gltf, -3, START_Z));
  cars.push(await createParadiso(projectSettings, gltf, -3, START_Z));
  cars.push(await createAmericain(projectSettings, gltf, -3, START_Z));
  // cars.push(createTestVehicle(projectSettings));

  cars.forEach((car) => car.reposition(150, START_Z));

  [0, -40, -80].forEach((x) => {
    const car = getNextCar();
    car.reposition(x, START_Z);

    setWorldTimeout(() => {
      car.activate();
      activeCars.push(car);
    }, projectSettings.stepDuration * 8);
  });
}

export function updateBuildings() {
  let carToRemove: CarSimple | Vehicle | undefined;
  let leftMostCarX = -10;

  activeCars.forEach((car) => {
    car.update();

    leftMostCarX = Math.min(leftMostCarX, car.getPosition().x);

    // get cars to remove
    if (car.getPosition().x > 60 && car.active) {
      carToRemove = car;
      carToRemove.reposition(120, START_Z);
    }
  });

  if (carToRemove) {
    activeCars = removeFromArray(activeCars, carToRemove);
    // console.log('remove car, activeCars.length', activeCars.length);
  }

  queuedCars.forEach((queuedCar) => {
    leftMostCarX = Math.min(leftMostCarX, queuedCar.getPosition().x);
  });

  if (leftMostCarX > -20 && activeCars.length && !queuedCars.length) {
    const nextCar = getNextCar();
    nextCar.reposition(-50, START_Z);
    queuedCars.push(nextCar);
    // console.log('add car, activeCars.length', activeCars.length, nextCar.name);

    setWorldTimeout(() => {
      queuedCars = removeFromArray(queuedCars, nextCar);
      activeCars.push(nextCar);
      nextCar.activate();
    }, 0.1);
  }
}
