/* eslint-disable max-len */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
import {
  ExtendedObject3D, Scene3D, THREE, Types,
} from 'enable3d';
import { START_X } from './utils';

export type CarSimpleProps = {
  axisPositionBack: number;
  axisPositionFront: number;
  body: ExtendedObject3D,
  bodyMass: number;
  bodyTexturePath: string | undefined;
  maxEngineForce: number,
  name?: string;
  physicsBox: { [property: string]: any },
  position: THREE.Vector3;
  rotationY: number;
  scale: number;
  wheel: ExtendedObject3D,
  wheelAxisHeight: number;
  wheelDiameter: number;
  wheelHalfTrack: number;
  wheelScale?: number;
  wheelTexturePath: string | undefined;
};

export default class CarSimple {
  scene3d: Scene3D;

  body: ExtendedObject3D;

  wheels: ExtendedObject3D[] = [];

  wheelsMotorized: ExtendedObject3D[] = [];

  active: boolean = true;

  angularVelocity: number;

  name: string | undefined;

  startY: number;

  previousX: number;

  previousSpeed: number;

  rotationY: number;

  constructor(scene3d: Scene3D, props: CarSimpleProps) {
    this.scene3d = scene3d;
    this.angularVelocity = props.maxEngineForce;
    this.name = props.name;
    this.create(props);
  }

  create(props: CarSimpleProps) {
    const {
      axisPositionBack,
      axisPositionFront,
      body,
      physicsBox,
      position,
      rotationY,
      scale,
      wheel,
      wheelAxisHeight = 0,
      wheelHalfTrack,
      wheelScale = 1,
    } = props;
    this.previousX = position.x;
    this.startY = position.y;
    this.rotationY = rotationY;

    this.body = body;
    this.body.position.copy(position);
    this.body.scale.set(scale, scale, scale);
    this.body.rotateY(rotationY);

    this.scene3d.scene.add(this.body);
    this.scene3d.physics.add.existing(this.body, { mass: 1, shape: 'box', ...physicsBox });

    // FRONT LEFT
    const wheelFL = this.createWheel(
      this.body, wheel, position, scale, rotationY, -wheelHalfTrack, wheelAxisHeight, axisPositionFront, wheelScale,
    );
    this.wheels.push(wheelFL);

    // FRONT RIGHT
    const wheelFR = this.createWheel(
      this.body, wheel, position, scale, rotationY, wheelHalfTrack, wheelAxisHeight, axisPositionFront, wheelScale,
    );
    this.wheels.push(wheelFR);

    // REAR LEFT
    const wheelRL = this.createWheel(
      this.body, wheel, position, scale, rotationY, -wheelHalfTrack, wheelAxisHeight, axisPositionBack, wheelScale,
    );
    this.wheels.push(wheelRL);
    this.wheelsMotorized.push(wheelRL);

    // REAR RIGHT
    const wheelRR = this.createWheel(
      this.body, wheel, position, scale, rotationY, wheelHalfTrack, wheelAxisHeight, axisPositionBack, wheelScale,
    );
    this.wheels.push(wheelRR);
    this.wheelsMotorized.push(wheelRR);
  }

  createWheel(
    body: ExtendedObject3D,
    wheel: ExtendedObject3D,
    carPosition: THREE.Vector3,
    scale: number,
    rotationY: number,
    x: number,
    y: number,
    z: number,
    wheelScale: number,
  ) {
    const wheelClone = wheel.clone();
    wheelClone.position.copy(carPosition);
    wheelClone.rotateY(rotationY);
    wheelClone.translateX(x * scale);
    wheelClone.translateZ(z * scale);
    wheelClone.rotateZ(Math.PI * 0.5);
    wheelClone.scale.set(scale * wheelScale, scale * wheelScale, scale * wheelScale);
    wheelClone.castShadow = true;
    wheelClone.receiveShadow = true;

    this.scene3d.scene.add(wheelClone);
    this.scene3d.physics.add.existing(wheelClone, { mass: 1, shape: 'mesh' });

    const flip = x / Math.abs(x);
    const pivotOnWheel: Types.XYZ = { x: 0, y: flip, z: 0 };
    const pivotOnBody: Types.XYZ = { x: x * scale + (flip * 0), y: y * scale, z: z * scale };
    const hingeAxisOnBody: Types.XYZ = { x: 1, y: 0, z: 0 };
    const hingeAxisOnWheel: Types.XYZ = { x: 0, y: -1, z: 0 };
    this.scene3d.physics.add.constraints.hinge(wheelClone.body, body.body, {
      pivotA: { ...pivotOnWheel },
      pivotB: { ...pivotOnBody },
      axisA: { ...hingeAxisOnWheel },
      axisB: { ...hingeAxisOnBody },
    });

    return wheelClone;
  }

  activate() {
    this.active = true;
    this.body.body.setVelocity(0, 0, 0);
    this.body.body.setAngularVelocity(0, 0, 0);
    this.body.body.setCollisionFlags(0);
    this.body.visible = true;
    this.wheels.forEach((wheel) => {
      wheel.body.setVelocity(0, 0, 0);
      wheel.body.setAngularVelocity(0, 0, 0);
      wheel.body.setCollisionFlags(0);
      wheel.visible = true;
    });
  }

  deactivate() {
    this.active = false;
    this.body.body.setVelocity(0, 0, 0);
    this.body.body.setAngularVelocity(0, 0, 0);
    this.body.body.setCollisionFlags(2);
    this.body.visible = false;
    this.wheels.forEach((wheel) => {
      wheel.body.setVelocity(0, 0, 0);
      wheel.body.setAngularVelocity(0, 0, 0);
      wheel.body.setCollisionFlags(2);
      wheel.visible = false;
    });
  }

  /**
   * General repositioning rule:
   * Reposition all the parts correctly relative to each other so the constraints will follow.
   *
   * First get the position of the wheels relative to the body.
   * Move the body.
   * Reposition the wheel relative to the body.
   * @param x Displacement in the x direction.
   */
  reposition(x: number, z: number) {
    this.deactivate();

    this.wheels.forEach((wheel) => {
      wheel.userData.relPos = new THREE.Vector3(0, 0, 0).subVectors(
        wheel.position, this.body.position,
      );
    });

    this.body.position.set(x, this.startY, z);
    this.body.rotation.set(0, this.rotationY, 0);
    this.body.body.needUpdate = true;
    this.previousX = this.body.position.x;

    this.wheels.forEach((wheel) => {
      wheel.position.addVectors(wheel.userData.relPos, this.body.position);
      wheel.body.needUpdate = true;
    });
  }

  /**
   * Move the car.
   */
  update() {
    let isToBeRemoved = false;
    if (this.active) {
      this.wheelsMotorized.forEach((wheel) => wheel.body.setAngularVelocityZ(this.angularVelocity));

      const speed = this.body.position.x - this.previousX;
      const acceleration = speed - this.previousSpeed;
      this.previousSpeed = speed;
      this.previousX = this.body.position.x;
      if (acceleration < -0.01 && speed <= 0 && this.body.position.x > START_X) {
        console.log('LEAVE', this.name, acceleration, speed, this.body.position.x);
        isToBeRemoved = true;
      }
    }

    return isToBeRemoved;
  }

  getPosition() {
    return this.body.position;
  }
}
