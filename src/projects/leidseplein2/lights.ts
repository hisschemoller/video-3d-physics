import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

const glowTexture = new THREE.TextureLoader().load('../assets/projects/leidseplein2/glow.png');

let reusableGlowLight: THREE.Mesh | undefined;

type SpotLightProps = {
  position: THREE.Vector3;
  targetPos: THREE.Vector3;
  color: number;
  angle: number;
};

export function createGlowLight(
  projectSettings: ProjectSettings,
  position: THREE.Vector3,
  radius: number,
  color: number,
) {
  const { scene } = projectSettings;
  const geometry = new THREE.SphereGeometry(radius, 8, 8);
  const material = new THREE.MeshLambertMaterial({
    color, opacity: 0.8, transparent: true, emissive: color, emissiveIntensity: 1,
  });
  const mesh = new THREE.Mesh(geometry, material);
  mesh.position.copy(position);
  scene.add(mesh);

  const spriteMaterial = new THREE.SpriteMaterial({
    map: glowTexture,
    opacity: 0.4,
    color,
    transparent: true,
    blending: THREE.AdditiveBlending,
  });
  const sprite = new THREE.Sprite(spriteMaterial);
  sprite.scale.set(radius * 8, radius * 8, 1.0);
  mesh.add(sprite);

  return mesh;
}

export function createStringOfGlowLights(
  projectSettings: ProjectSettings,
  parent: THREE.Object3D,
  amount: number,
  start: THREE.Vector3,
  end: THREE.Vector3,
  scale: number = 1,
) {
  if (!reusableGlowLight) {
    reusableGlowLight = createGlowLight(
      projectSettings,
      new THREE.Vector3(-100, 0, 0),
      0.02,
      0xffcc4b,
    );
  }

  for (let i = 0, n = amount; i < n; i += 1) {
    const s = scale + (Math.random() * 0.006 * scale);
    const glowLight = reusableGlowLight.clone();
    glowLight.scale.set(s, s, s);
    glowLight.position.lerpVectors(start, end, i / n);
    glowLight.position.x += Math.random() * 0.1;
    glowLight.position.y -= Math.sin((i / n) * Math.PI);
    glowLight.position.z += Math.random() * 0.1;
    parent.add(glowLight);
  }
}

function createSpotLight(projectSettings: ProjectSettings, {
  position,
  targetPos,
  color,
  angle,
}: SpotLightProps) {
  const { scene } = projectSettings;
  const sphere = new THREE.SphereGeometry(0.5, 16, 8);

  const target = new THREE.Object3D();
  target.position.copy(targetPos);
  scene.add(target);

  const spotLight = new THREE.SpotLight(color, 1);
  spotLight.add(new THREE.Mesh(sphere, new THREE.MeshBasicMaterial({ color: 0xfddb63 })));
  spotLight.position.copy(position);
  spotLight.target = target;
  spotLight.angle = angle;
  spotLight.penumbra = 1;
  spotLight.decay = 2;
  spotLight.distance = 0;
  spotLight.lookAt(target.position);

  spotLight.castShadow = true;
  spotLight.shadow.mapSize.width = 1024;
  spotLight.shadow.mapSize.height = 1024;
  spotLight.shadow.camera.near = 1;
  spotLight.shadow.camera.far = 10;
  spotLight.shadow.focus = 1;
  scene.add(spotLight);
}

export default function createLights(projectSettings: ProjectSettings) {
  createSpotLight(projectSettings, {
    position: new THREE.Vector3(10, 20, -20),
    targetPos: new THREE.Vector3(5, 0, -30),
    color: 0xfff1bd,
    angle: 0.5,
  });

  createSpotLight(projectSettings, {
    position: new THREE.Vector3(0, 20, 0),
    targetPos: new THREE.Vector3(0, 15, -50),
    color: 0x999999,
    angle: 0.5,
  });

  createSpotLight(projectSettings, {
    position: new THREE.Vector3(-15, 18, 0),
    targetPos: new THREE.Vector3(-12, 2, -20),
    color: 0xffcc4b,
    angle: 0.3,
  });

  createSpotLight(projectSettings, {
    position: new THREE.Vector3(12, 7, 20),
    targetPos: new THREE.Vector3(10, 4, -15),
    color: 0x662200,
    angle: 0.1,
  });

  // createGlowLight(projectSettings, new THREE.Vector3(0, 10, -20), 0.2, 0xffcc4b);
  // createGlowLight(projectSettings, new THREE.Vector3(-10, 15, -40), 0.1, 0xffa600);
  // createGlowLight(projectSettings, new THREE.Vector3(12, 20, -40), 0.15, 0xffb700);
  // createGlowLight(projectSettings, new THREE.Vector3(4, 4, -10), 0.1, 0xffcc4b);
}
