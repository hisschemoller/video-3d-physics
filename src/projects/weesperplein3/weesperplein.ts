import { ExtendedObject3D, Scene3D, THREE } from 'enable3d';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import createTween from '@app/tween';

/**
 * Bounding boxes as kinematic physics objects and long sticks connected with point to point
 * constraints.
 */

export interface DetectedObjectExt {
  bbox: [number, number, number, number];
  class: string;
  id: number;
  score: number;
}

export type BBox = {
  box: ExtendedObject3D;
  constraint: Ammo.btPoint2PointConstraint | undefined;
  cylinder: ExtendedObject3D;
  id: number;
  mesh: THREE.Mesh;
};

const COLORS = [
  '#00ff00',
  '#ff0000',
  '#0000ff',
  '#00ffff',
  '#ffff00',
  '#ff00ff',
  '#000000',
  '#ffffff',
  '#555555',
  '#aaaaaa',
  '#ff9900',
  '#9933ff',
];

let motiondata: DetectedObjectExt[][];
let motionDataStartIndex: number;
let svgScale: number;
let scene3: Scene3D;
let group: THREE.Group;
let dataIndex: number;
let dataStep: number;
let videoStartTime: number;
const bBoxes: BBox[] = [];

/**
 * Update.
 */
function updateBox(box: ExtendedObject3D, mesh: THREE.Mesh, bbox: [number, number, number, number]) {
  const [x, y, width, height] = bbox;
  mesh.scale.set(width * svgScale, height * svgScale, 1);
  // mesh.position.set((x + (width / 2)) * svgScale, (-y - (height / 2)) * svgScale, 0.1);
  box.position.set(x * svgScale, -y * svgScale, 0.1);
  box.body.needUpdate = true;
}

/**
 * Create.
 */
function createBox(id: number, bbox: [number, number, number, number]) {
  const [x, y, width, height] = bbox;
  const positions = [0, 0, 0, 1, 0, 0, 1, -1, 0, 0, -1, 0, 0, 0, 0];
  const geometry = new LineGeometry();
  geometry.setPositions(positions);

  const material = new LineMaterial({
    color: 0x0000ff,
    linewidth: 0.01,
    vertexColors: false,
    dashed: false,
    alphaToCoverage: true,
  });

  const mesh = new Line2(geometry, material);
  mesh.computeLineDistances();
  mesh.scale.set(width * svgScale, height * svgScale, 1);
  mesh.castShadow = true;
  mesh.receiveShadow = true;

  const box = new ExtendedObject3D();
  box.add(mesh);
  box.position.set(x * svgScale, -y * svgScale, 0.1);

  group.add(box);
  scene3.physics.add.existing(box, {
    mass: 1,
    shape: 'mesh',
  });
  box.body.setCollisionFlags(2); // make it kinematic

  // cylinder
  const cylinderLength = 6;
  const cylinder = scene3.add.cylinder({
    height: cylinderLength,
    radiusBottom: 0.04,
    radiusTop: 0.04,
  }, {});
  cylinder.position.set(
    group.position.x + (bbox[0] * svgScale),
    group.position.y + (bbox[1] * -svgScale),
    group.position.z + (cylinderLength / 2),
  ); 
  cylinder.rotation.x = Math.PI * 0.5;
  cylinder.castShadow = true;
  cylinder.receiveShadow = true;
  
  scene3.physics.add.existing(cylinder, {
    mass: 0.01,
  });
  cylinder.body.setCollisionFlags(0);

  // const constraint = undefined;
  const constraint = scene3.physics.add.constraints.pointToPoint(box.body, cylinder.body, {
    pivotA: { z: 0.1 },
    pivotB: { y: cylinderLength / -2 }
  });

  bBoxes.push({ id, box, constraint, cylinder, mesh });
}

/**
 * Delete box.
 */
function deleteBox(index: number) {
  const { box, constraint, cylinder, mesh } = bBoxes[index];
  box.remove(mesh);

  if (constraint) {
    scene3.physics.physicsWorld.removeConstraint(constraint);
  }
  
  group.remove(box);
  scene3.destroy(box);
  scene3.destroy(cylinder);
  bBoxes.splice(index, 1);
}

/**
 * Start tween.
 */
function onMotionTweenStart() {
  dataIndex = motionDataStartIndex;
  motiondata[0].forEach((data) => {
    const { bbox, id } = data;
    createBox(id, bbox);
  });
}

/**
 * Update tween.
 */
function onMotionTweenUpdate() {
  // the motion data frame to use in this update cycle
  dataIndex += dataStep;

  // update all motion objects in the scene
  motiondata[dataIndex].forEach((data) => {
    const { bbox, id } = data;

    // find if the id already exists in the bBoxes
    const bBox = bBoxes.find((bb) => bb.id === id);
    if (bBox) {
      // re-position the existing mesh
      updateBox(bBox.box, bBox.mesh, bbox);
    } else {
      // create a new bbox mesh
      createBox(id, bbox);
    }

    // find bboxes that don't exist anymore in the current frame
    for (let i = bBoxes.length - 1; i >= 0; i -= 1) {
      const b = motiondata[dataIndex].find((d) => d.id === bBoxes[i].id);
      if (!b) {
        deleteBox(i);
      }
    }
  });
}

/**
 * End tween.
 */
function onMotionTweenComplete() {
  for (let i = bBoxes.length - 1; i >= 0; i -= 1) {
    deleteBox(i);
  }
}

/**
 * Setup.
 */
export default async function createWeesperplein(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  fps: number,
  videoStart: number,
) {
  const {
    patternDuration, scene, scene3d, stepDuration, timeline, width, width3d,
  } = projectSettings;

  // const mesh = new THREE.Mesh(
  //   new THREE.BoxGeometry(1, 1, 1),
  //   new THREE.MeshStandardMaterial({ color: '#ff0000' }),
  // );
  // scene.add(mesh);

  const videoFps = (media.video as unknown as VideoData).fps;
  dataStep = videoFps / fps;
  svgScale = width3d / width;
  videoStartTime = videoStart;
  motionDataStartIndex = Math.round(videoStartTime * videoFps);
  scene3 = scene3d as unknown as Scene3D;

  group = new THREE.Group();
  group.position.set(-8, 4.5, -5);
  group.add(new THREE.AxesHelper());
  scene.add(group);

  const actor = await createActor(projectSettings, media.video, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein2/alles.svg' },
    imageRect: { w: 1920, h: 1080 },
    depth: 0.05,
    color: 0x000000,
  });
  actor.setStaticPosition(getMatrix4({ x: 0, y: 0, z: 0 }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: videoStartTime,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  const extendedObject3D = new ExtendedObject3D();
  extendedObject3D.add(actor.getMesh());
  group.add(extendedObject3D);
  scene3.physics.add.existing(extendedObject3D, {
    mass: 0,
    shape: 'mesh',
  });
  extendedObject3D.body.setCollisionFlags(1);

  const response = await fetch('../assets/projects/weesperplein3/weesperplein-motiondata-4.json');
  motiondata = await response.json() as DetectedObjectExt[][];

  const tween = createTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    onStart: onMotionTweenStart,
    onUpdate: onMotionTweenUpdate,
    onComplete: onMotionTweenComplete,
  });
  timeline.add(tween);

  const floor = await createActor(projectSettings, undefined, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/vloer.svg' },
    imageRect: { w: 1920, h: 1080 },
    depth: 0.1,
    color: 0xffdd99,
  });
  floor.setStaticPosition(getMatrix4({ x: 0, y: 0, z: 0 }));
  floor.setColor('#996633');
  floor.getMesh().castShadow = true;
  floor.getMesh().receiveShadow = true;

  const floorExtendedObject3D = new ExtendedObject3D();
  floorExtendedObject3D.add(floor.getMesh());
  floorExtendedObject3D.position.set(0, -7, 0);
  floorExtendedObject3D.rotation.x = Math.PI / -2;
  group.add(floorExtendedObject3D);
  scene3.physics.add.existing(floorExtendedObject3D, {
    mass: 0,
    shape: 'mesh',
  });
}
