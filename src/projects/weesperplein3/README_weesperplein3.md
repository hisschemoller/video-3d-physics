# Weesperplein 3

Weesperplein 2022-08-19.mov\
1920 x 1080\
3:31

Uses the same frames as Weesperplein 2, 43 seconds from 0:20 to 1:03.

Finished and branched.

## Render

```bash
# png to mp4 (from index 1286 met 30 FPS
ffmpeg -framerate 30 -start_number 1286 -i /Volumes/Samsung_X5/weesperplein3-rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperplein3-video-x1.mp4
# repeat 6 times, 1276 frames, video only
ffmpeg -i weesperplein3-video-x1.mp4 -filter_complex "loop=6:size=1276:start=0" weesperplein3-video-x6.mp4
# repeat 6 times, audio 
ffmpeg -stream_loop 2 -i weesperplein3-audio-x1.wav -c copy weesperplein3-audio-x6.wav
# video en audio samenvoegen
ffmpeg -i weesperplein3-video-x6.mp4 -i weesperplein3-audio-x6.wav -vcodec copy weesperplein3-x6.mp4
# scale to 50%, 960 * 720
ffmpeg -i weesperplein3-x6.mp4 -vf scale=960:720 weesperplein3-x6_halfsize.mp4
```

## Music

Video duurt 1276 frames.\
Video duurt 1276 / 30 FPS = 42.53333333333333 seconden.\
Video duurt 16 maten van 4 beats = 64 beats.\
Een beat duurt 42.53333333333333 seconden / 64 beats = 0.6645833333333333 seconden.\
Het tempo is 60 / 0.6645833333333333 = 90.28213166144201 BPM\
Dat klopt wel ongeveer met de ingestelde 90 BPM.

## About

Verkeer op het Weesperplein dat gedetecteerd is met AI object detectie software (Tensorflow en COCO-SSD). 