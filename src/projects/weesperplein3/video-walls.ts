import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import { VideoActor } from './weesperplein-a';

/**
 * Weesperplein background.
 */
export async function createBackground(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const actor = await createActor(projectSettings, media.video, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/background.svg' },
    imageRect: { w: 1920, h: 1080 },
    depth: 0.05,
    color: 0x000000,
  });
  actor.setStaticPosition(getMatrix4({ x: 0, y: 0, z: -8 }));
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  videoActors.push({ actor, x: 0, y: 0, width: 1920 * ps, height: 1080 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createTreesBack(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.1;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/bomen-achter.svg' },
    imageRect: { w: 541, h: 474 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(857, 197);
  actor.setStaticPosition(getMatrix4({ x: 3, y: -1.2, z: -5, sx: S, sy: S }));
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  // actor.getMesh().renderOrder = 3;

  videoActors.push({ actor, x: 857 * ps, y: 197 * ps, width: 541 * ps, height: 474 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createWallLeftFrontLeft(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.1;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-links-voor-links.svg' },
    imageRect: { w: 471, h: 756 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(0, 0);
  actor.setStaticPosition(getMatrix4({ x: -4, y: 0.3, z: 0.5, sx: S, sy: S }));
  actor.getMesh().rotateY(Math.PI * 0.2);
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  videoActors.push({ actor, x: 0 * ps, y: 0 * ps, width: 471 * ps, height: 756 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createWallLeftFront(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.1;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-links-voor.svg' },
    imageRect: { w: 403, h: 685 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(304, 80);
  actor.setStaticPosition(getMatrix4({ x: -1, y: -0.5, z: -1, sx: S, sy: S }));
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  videoActors.push({ actor, x: 304 * ps, y: 80 * ps, width: 403 * ps, height: 685 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createWallLeftSide(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.1;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-links-zij.svg' },
    imageRect: { w: 178, h: 745 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(667, 69);
  actor.setStaticPosition(getMatrix4({ x: 2.686, y: -0.4, z: -0.99, sx: S * 1.5, sy: S }));
  actor.getMesh().rotateY(Math.PI * 0.2);
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  videoActors.push({ actor, x: 667 * ps, y: 69 * ps, width: 178 * ps, height: 745 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createWallLeftSide2(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 0.9;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-links-zij-2.svg' },
    imageRect: { w: 270, h: 520 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(816, 313);
  actor.setStaticPosition(getMatrix4({ x: 4.5, y: -3, z: -2.7, sx: S * 1.2, sy: S }));
  actor.getMesh().rotateY(Math.PI * 0.3);
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  // actor.getMesh().renderOrder = 1;

  videoActors.push({ actor, x: 816 * ps, y: 313 * ps, width: 270 * ps, height: 520 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createWallRightFront(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.4;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-rechts-voor.svg' },
    imageRect: { w: 318, h: 891 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(1602, 162);
  actor.setStaticPosition(getMatrix4({ x: 10, y: 0, z: 0, sx: S, sy: S }));
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  videoActors.push({ actor, x: 1602 * ps, y: 162 * ps, width: 318 * ps, height: 891 * ps });

  return actor;
}

/**
 * Weesperplein background.
 */
export async function createWallRightSide1(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.4;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-rechts-zij-1.svg' },
    imageRect: { w: 169, h: 652 },
    depth: 0.02,  
    color: 0x6c6866,
  });
  actor.setStaticImage(1453, 160);
  actor.setStaticPosition(getMatrix4({ x: 9.7, y: 0, z: -2.26, sx: S, sy: S }));
  actor.getMesh().rotateY(Math.PI * -0.4);
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;

  videoActors.push({ actor, x: 1453 * ps, y: 160 * ps, width: 169 * ps, height: 652 * ps });

  return actor;
}

/**
 * Weesperplein background with metro.
 */
export async function createWallRightSide2(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
  videoActors: VideoActor[],
) {
  const { isPreview, previewScale } = projectSettings;
  const ps = isPreview ? previewScale : 1;
  const S = 1.3;
  const actor = await createActor(projectSettings, media.frame_00015_straight, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/gevel-rechts-zij-2.svg' },
    imageRect: { w: 287, h: 564 },
    depth: 0.02,
    color: 0x6c6866,
  });
  actor.setStaticImage(1206, 190);
  actor.setStaticPosition(getMatrix4({ x: 7.7, y: -0.6, z: -4.2, sx: S, sy: S }));
  actor.getMesh().rotateY(Math.PI * -0.22);
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  // actor.getMesh().renderOrder = 2;

  videoActors.push({ actor, x: 1206 * ps, y: 190 * ps, width: 287 * ps, height: 564 * ps });

  return actor;
}