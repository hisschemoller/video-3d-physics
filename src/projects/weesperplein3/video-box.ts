import { THREE } from 'enable3d';
import { Actor, createActor } from '@app/actor';
import { ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';

const PADDING = 20;

const INDIVIDUAL = [
  { id: 57, z: 5, speed: -0.01 },
];

export default class VideoBox {
  projectSettings: ProjectSettings;

  video: VideoData;

  parent: THREE.Object3D;

  actor: Actor | undefined;

  svgScale: number;

  yIncrease: number = 0;

  z: number = 1 + (Math.random() * 1);

  speed: number = 0.005 + (Math.random() * 0.005);

  paddingImage: number;
  
  constructor(
    projectSettings: ProjectSettings,
    svgScale: number,
    video: VideoData,
    parent: THREE.Object3D,
    id: number,
  ) {
    const { isPreview, previewScale } = projectSettings;
    this.projectSettings = projectSettings;
    this.svgScale = svgScale;
    this.video = video;
    this.parent = parent;
    this.paddingImage = isPreview ? PADDING * previewScale : PADDING;

    const individual = INDIVIDUAL.find((i) => i.id === id);
    if(individual) {
      this.z = individual.z;
      this.speed = individual.speed;
    }
  }

  async create(bbox: [number, number, number, number]) {
    const [x, y] = bbox;

    this.actor = await createActor(this.projectSettings, this.video, {
      box: { w: 1, h: 1, d: 0.05 },
      imageRect: { w: 256, h: 256 },
      depth: 0.05,
      color: 0x666666,
    });
    this.actor.setStaticPosition(getMatrix4({ x: x * this.svgScale, y: -y * this.svgScale, z: this.z }));
    this.actor.getMesh().castShadow = true;
    this.actor.getMesh().receiveShadow = true;
    this.parent.add(this.actor.getMesh());
  }

  update(img: HTMLImageElement, bbox: [number, number, number, number]) {
    let [x, y, width, height] = bbox;
    this.z += this.speed;
    this.yIncrease += 0.001;

    if (this.actor) {
      this.actor.getMesh().position.set(
        (x - PADDING) * this.svgScale,
        ((-y + PADDING) * this.svgScale) + this.yIncrease,
        this.z,
      );
      this.actor.getMesh().scale.set(
        (width + (PADDING * 2)) * this.svgScale,
        (height + (PADDING * 2)) * this.svgScale,
        1,
      );

      if (this.projectSettings.isPreview) {
        x *= this.projectSettings.previewScale;
        y *= this.projectSettings.previewScale;
        width *= this.projectSettings.previewScale;
        height *= this.projectSettings.previewScale;
      }
      this.actor.setImage(
        img,
        x - this.paddingImage,
        y - this.paddingImage,
        width + (this.paddingImage * 2),
        height + (this.paddingImage * 2),
      );
    }
  }

  delete() {
    if (this.actor) {
      this.parent.remove(this.actor.getMesh());
    }
  }
}
