import { ExtendedObject3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ProjectSettings } from '@app/interfaces';

function createPole(gltf: GLTF, x: number, z: number, ry: number) {
  const scale = 0.5;
  const model = (gltf.scene.getObjectByName('Lantarenpaal') as ExtendedObject3D).clone();
  model.material = new THREE.MeshLambertMaterial({ color: 0x61605c });
  model.position.set(x, -6, z);
  model.scale.set(scale, scale, scale);
  model.rotation.y = ry;
  model.castShadow = true;
  model.receiveShadow = true;

  return model;
}

export default async function createObjects(
  projectSettings: ProjectSettings,
  group: THREE.Group,
) {
  const { scene3d } =  projectSettings;

  // BLENDER MODEL
  const gltf = await scene3d.load.gltf('../assets/projects/weesperplein3/weesperplein3.glb');

  group.add(createPole(gltf, -6, 1.5, 0));
  group.add(createPole(gltf, -6, 7.7, Math.PI * 0.5));
  group.add(createPole(gltf, 18, 1.5, Math.PI * -0.5));
  group.add(createPole(gltf, 19, 7.7, Math.PI * 1));

  const grid = new THREE.GridHelper(80, 10, 0x000000, 0x000000);
  grid.position.set(0, 0, -30);
  grid.rotateX(Math.PI / 2);
  group.add(grid);

  const shadowCamHelper = new THREE.CameraHelper(scene3d.directionalLight.shadow.camera);
  scene3d.scene.add(shadowCamHelper);

  const directionalLightHelper = new THREE.DirectionalLightHelper(scene3d.directionalLight);
  scene3d.scene.add(directionalLightHelper);
}
