import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';

/**
 * Weesperplein wall far left.
 */
export async function createFarLeft(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/verte.svg' },
    imageRect: { w: 1024, h: 488 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(0, 1560);
  actor.setStaticPosition(getMatrix4({ x: -16, y: -6 + (488 * svgScale), z: 4.5 + ((1024 * svgScale) / 2) }));
  actor.getMesh().rotation.y = Math.PI / 2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein wall far right.
 */
export async function createFarRight(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/verte.svg' },
    imageRect: { w: 1024, h: 488 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(1024, 1560);
  actor.setStaticPosition(getMatrix4({ x: 32, y: -6 + (488 * svgScale), z: 4.5 - ((1024 * svgScale) / 2) }));
  actor.getMesh().rotation.y = Math.PI / -2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein ground.
 */
export async function createFloor(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.groundDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/vloer.svg' },
    imageRect: { w: 2048, h: 1152 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(0, 0);
  actor.setStaticPosition(getMatrix4({ x: 0, y: -6, z: 0 }));
  actor.setColor('#ffffff');
  actor.getMesh().rotation.x = Math.PI / -2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein ground.
 */
export async function createFloorLeft(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.groundDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/vloer.svg' },
    imageRect: { w: 1593, h: 896 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(0, 1152);
  actor.setStaticPosition(getMatrix4({ x: -16, y: -6, z: 0 }));
  actor.setColor('#ffffff');
  actor.getMesh().rotation.x = Math.PI / -2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein ground.
 */
export async function createFloorRight(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.groundDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/vloer.svg' },
    imageRect: { w: 1593, h: 896 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(0, 1152);
  actor.setStaticPosition(getMatrix4({ x: 32, y: -5.9, z: 0 }));
  actor.setColor('#ffffff');
  actor.getMesh().rotation.x = Math.PI / -2;
  actor.getMesh().rotation.y = Math.PI;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein wall far left.
 */
export async function createWallLeft(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/wand-langer.svg' },
    imageRect: { w: 640, h: 920 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(0, 0);
  actor.setStaticPosition(getMatrix4({ x: -8, y: -6 + (640 * svgScale), z: 4.5 - 2 }));
  actor.getMesh().rotation.y = Math.PI / 3;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein wall far left.
 */
export async function createWallLeftWest(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/wand.svg' },
    imageRect: { w: 640, h: 640 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(0, 920);
  actor.setStaticPosition(getMatrix4({ x: -8, y: -7 + (640 * svgScale), z: 4.5 + 6 }));
  actor.getMesh().rotation.y = Math.PI / 2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein wall far left.
 */
export async function createWallRight(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/wand-langer.svg' },
    imageRect: { w: 640, h: 920 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(640, 0);
  actor.setStaticPosition(getMatrix4({ x: 20, y: -6 + (640 * svgScale), z: 4.5 - 7 }));
  actor.getMesh().rotation.y = Math.PI / -2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein wall far left.
 */
export async function createWallRightBehind(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const scale = 1.06;
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/wand-langer-2.svg' },
    imageRect: { w: 770, h: 920 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(1280, 0);
  actor.setStaticPosition(getMatrix4({ x: 16, y: -6 + (640 * svgScale), z: -8 }));
  actor.getMesh().scale.set(scale, scale, 1);
  actor.getMesh().rotation.y = Math.PI / -3.34;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}

/**
 * Weesperplein wall far left.
 */
export async function createWallRightWest(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  svgScale: number,
) {
  const actor = await createActor(projectSettings, media.wallDrawings, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein3/wand.svg' },
    imageRect: { w: 640, h: 640 },
    depth: 0.1,
    color: 0xcccccc,
  });
  actor.setStaticImage(640, 920);
  actor.setStaticPosition(getMatrix4({ x: 20, y: -7 + (640 * svgScale), z: 4.5 + 0.5 }));
  actor.getMesh().rotation.y = Math.PI / -2;
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  return actor;
}