/* eslint-disable object-curly-newline */
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import createWeesperplein_a from './weesperplein-a';
import initCamera_1 from './camera';

const PROJECT_PREVIEW_SCALE = 0.25;
const BPM = 120;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 5;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  constructor() {
    super();

    this.width = 1080;
    this.canvasHeight = 1920 + 240; // = 2160; 2160 / 1440 = 1.5, dus 150%
    this.viewportHeight = 1920;
    this.width3d = 9;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 25;
    this.captureFps = 25;
    this.captureThrottle = 10;
    this.captureDuration = PATTERN_DURATION * 3;
    this.clearColor = 0x5178a7;
    this.shadowSize = 16;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = this.width;
    this.captureCanvas.height = this.viewportHeight;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;
  }

  /**
   * Set the html viewport and canvas height. For adjusted horizon height.
   */
  adjustHorizon() {
    const canvasContainer = document.getElementById('canvas-container');
    const canvas = this.renderer.domElement;

    if (canvasContainer && canvas) {
      canvasContainer.style.height =  `${(this.viewportHeight / this.width) * 100}vw`;
      canvas.setAttribute('style', `${(this.canvasHeight / this.viewportHeight) * 100}% !important`);
    }
  }

  async create() {
    await super.create();

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();

    this.renderer.setSize(this.width, this.canvasHeight);

    // DIRECTIONAL LIGHT
    this.directionalLight.intensity = 1.6 * Math.PI;
    this.directionalLight.position.set(10, 20, 10);

    // // // AMBIENT LIGHT
    this.ambientLight.intensity = 0.5 * Math.PI;

    // CAMERA
    this.pCamera.position.z -= 5;
    this.pCamera.lookAt(this.cameraTarget);
    this.pCamera.updateProjectionMatrix();
    this.orbitControls.target = this.cameraTarget;
    this.orbitControls.update();
    this.orbitControls.saveState();

    // MEDIA
    const mediaVideo = {
      fps: 25,
      height: 1080,
      scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
      width: 1920,
    };
    const media = {
      video: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/weesperplein3/frames_preview/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/weesperplein2/frames/&img=frame_#FRAME#.png',
      },
      frame_00015_straight: {
        ...mediaVideo,
        imgSrc: '../assets/projects/weesperplein3/weesperplein3-recht.png',
      },
      wallDrawings: {
        height: 2048,
        imgSrc: '../assets/projects/weesperplein3/weesperplein3-drawings.jpg',
        width: 2048,
      },
      groundDrawings: {
        height: 2048,
        imgSrc: '../assets/projects/weesperplein3/weesperplein3-ground.jpg',
        width: 2048,
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.width * 0.75,
      height3d: this.width3d * 0.75,
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene: this.scene,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    await createWeesperplein_a(projectSettings, media, this.fps, 30);
    await initCamera_1(projectSettings, this.pCamera);
  
    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    super.updateAsync(time, delta);
  }

  captureImage() {
    this.captureCanvasContext.drawImage(
      this.renderer.domElement, 0, 0, this.width, this.viewportHeight, 0, 0, this.width, this.viewportHeight,
    );
    return this.captureCanvas.toDataURL();
  }
}
