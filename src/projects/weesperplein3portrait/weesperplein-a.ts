import { THREE } from 'enable3d';
import { Font, FontLoader } from 'three/examples/jsm/loaders/FontLoader.js';
import { Actor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import createTween from '@app/tween';
import VideoBox from './video-box';
import { createBox, deleteBox, updateBox } from './bounding-box';
import {
  createBackground,
  createTreesBack,
  createWallLeftFront,
  createWallLeftFrontLeft,
  createWallLeftSide,
  createWallLeftSide2,
  createWallRightFront,
  createWallRightSide1,
  createWallRightSide2,
} from './video-walls';
import {
  createFarLeft,
  createFarRight,
  createFloor,
  createFloorLeft,
  createFloorRight,
  createWallLeft,
  createWallLeftWest,
  createWallRight,
  createWallRightBehind,
  createWallRightWest,
} from './drawings';
import createObjects from './objects';

export interface DetectedObjectExt {
  bbox: [number, number, number, number];
  class: string;
  id: number;
  score: number;
}

export type BBox = {
  box: THREE.Mesh;
  id: number;
  textMesh: THREE.Mesh;
  videoBox: VideoBox;
};

export type VideoActor = {
  actor: Actor;
  x: number,
  y: number,
  width: number,
  height: number,
};

const bBoxes: BBox[] = [];
const videoActors: VideoActor[] = [];
let projectSettings: ProjectSettings;
let videoData: VideoData;
let motiondata: DetectedObjectExt[][];
let group: THREE.Group;
let img: HTMLImageElement;
let motionDataStartIndex: number;
let svgScale: number;
let dataIndex: number;
let dataStep: number;
let videoStartTime: number;
let fps: number;
let font: Font;

/**
 * Load a JSON font file.
 */
function loadFont() {
  return new Promise<Font>(
    (resolve, reject) => {
      const fontLoader = new FontLoader();
      fontLoader.load(
        '../../../node_modules/three/examples/fonts/helvetiker_regular.typeface.json',
        (font) => resolve(font),
        undefined,
        (err) => reject(err),
      );
    });
}

/**
 * Start tween.
 */
function onMotionTweenStart() {
  dataIndex = motionDataStartIndex;
  motiondata[0].forEach((data) => {
    const { bbox, id } = data;
    createBox(id, bbox, svgScale, group, projectSettings, videoData, bBoxes, font);
  });
}

/**
 * Update tween.
 */
async function onMotionTweenUpdate() {
  let { isPreview, previewScale } = projectSettings;
  let { width, height } = videoData;
  if (isPreview) {
    width *= previewScale;
    height *= previewScale;
  }
  // the motion data frame to use in this update cycle
  dataIndex += dataStep;

  // load image
  await loadVideoFrame(dataIndex);

  // background image frames
  videoActors.forEach((v) => v.actor.setImage(img, v.x, v.y, v.width, v.height));

  // update all motion objects in the scene
  motiondata[dataIndex].forEach((data) => {
    const { bbox, id } = data;

    // find if the id already exists in the bBoxes
    const bBox = bBoxes.find((bb) => bb.id === id);
    if (bBox) {
      // re-position the existing mesh
      updateBox(bBox, bbox, svgScale, img);
    } else {
      // create a new bbox mesh
      createBox(id, bbox, svgScale, group, projectSettings, videoData, bBoxes, font);
    }

    // find bboxes that don't exist anymore in the current frame
    for (let i = bBoxes.length - 1; i >= 0; i -= 1) {
      const b = motiondata[dataIndex].find((d) => d.id === bBoxes[i].id);
      if (!b) {
        deleteBox(bBoxes, i, group);
      }
    }
  });
}

/**
 * End tween.
 */
function onMotionTweenComplete() {
  for (let i = bBoxes.length - 1; i >= 0; i -= 1) {
    deleteBox(bBoxes, i, group);  
  }
}

/**
 * Load the image for this frame.
 */
async function loadVideoFrame(dataIndex: number) {
  return new Promise<boolean>((resolve, reject) => {
    img.onload = () => resolve(true);
    img.onerror = reject;
    img.src = videoData.imgSrcPath
      .split('#FRAME#')
      .join((dataIndex <= 99999) ? (`0000${Math.round(dataIndex)}`).slice(-5) : '99999');
  })
}

/**
 * Setup.
 */
export default async function createWeesperplein_a(
  _projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  _fps: number,
  videoStart: number,
) {
  projectSettings = _projectSettings;
  const {
    patternDuration, scene, stepDuration, timeline, width, width3d,
  } = projectSettings;

  fps = _fps;
  videoData = media.video as unknown as VideoData;
  dataStep = videoData.fps / fps;
  svgScale = width3d / width;
  videoStartTime = videoStart;
  motionDataStartIndex = Math.round(videoStartTime * videoData.fps);
  img = new Image();

  const response = await fetch('../assets/projects/weesperplein3/weesperplein-motiondata-5.json');
  motiondata = await response.json() as DetectedObjectExt[][];

  font = await loadFont();

  group = new THREE.Group();
  group.position.set(-8, 5, 0);
  group.add(new THREE.AxesHelper());
  scene.add(group);

  const background = await createBackground(projectSettings, media, svgScale, videoActors);
  group.add(background.getMesh());

  const farLeft = await createFarLeft(projectSettings, media, svgScale);
  group.add(farLeft.getMesh());

  const wallLeft = await createWallLeft(projectSettings, media, svgScale);
  group.add(wallLeft.getMesh());

  const wallLeftWest = await createWallLeftWest(projectSettings, media, svgScale);
  group.add(wallLeftWest.getMesh());

  const wallRight = await createWallRight(projectSettings, media, svgScale);
  group.add(wallRight.getMesh());

  const wallRightBehind = await createWallRightBehind(projectSettings, media, svgScale);
  group.add(wallRightBehind.getMesh());

  const wallRightWest = await createWallRightWest(projectSettings, media, svgScale);
  group.add(wallRightWest.getMesh());

  const farRight = await createFarRight(projectSettings, media, svgScale);
  group.add(farRight.getMesh());

  const floor = await createFloor(projectSettings, media, svgScale);
  group.add(floor.getMesh());

  const floorLeft = await createFloorLeft(projectSettings, media, svgScale);
  group.add(floorLeft.getMesh());

  const floorRight = await createFloorRight(projectSettings, media, svgScale);
  group.add(floorRight.getMesh());

  const treesBack = await createTreesBack(projectSettings, media, svgScale, videoActors);
  group.add(treesBack.getMesh());

  const wallLeftFront = await createWallLeftFront(projectSettings, media, svgScale, videoActors);
  group.add(wallLeftFront.getMesh());

  const wallLeftFrontLeft = await createWallLeftFrontLeft(projectSettings, media, svgScale, videoActors);
  group.add(wallLeftFrontLeft.getMesh());

  const wallLeftSide = await createWallLeftSide(projectSettings, media, svgScale, videoActors);
  group.add(wallLeftSide.getMesh());

  const wallLeftSide2 = await createWallLeftSide2(projectSettings, media, svgScale, videoActors);
  group.add(wallLeftSide2.getMesh());

  const wallRightFront = await createWallRightFront(projectSettings, media, svgScale, videoActors);
  group.add(wallRightFront.getMesh());

  const wallRightSide1 = await createWallRightSide1(projectSettings, media, svgScale, videoActors);
  group.add(wallRightSide1.getMesh());

  const wallRightSide2 = await createWallRightSide2(projectSettings, media, svgScale, videoActors);
  group.add(wallRightSide2.getMesh());

  await createObjects(projectSettings, group);

  const tween = createTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    onStart: onMotionTweenStart,
    onUpdate: onMotionTweenUpdate,
    onComplete: onMotionTweenComplete,
  });
  timeline.add(tween);
}
