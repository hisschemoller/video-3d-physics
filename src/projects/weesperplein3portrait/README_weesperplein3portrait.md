# Weesperplein 3 Portrait

Portrait versie van Weesperstraat 3.

### Demo festival vereisten

* Size: 1080 x 1920px
* Orientation: Portrait
* Duration: Exactly 10sec
* Encoding: mp4, H.264
* Frames per second: 25fps
* Max file size: 25MB
* Do not add credits in your work: NO title, NO credit outro
* The credit will be communicated in a special DEMO caption on top of your work

## Weesperplein eigen opname video

Weesperplein 2022-08-19.mov\
1920 x 1080\
3:31

Weesperplein 3 gebruikt dezelfde frames als Weesperplein 2.\
De video moet opnieuw omgezet worden want de image sequence had ik niet bewaard.\
Zowel de normale grootte als de 25% geschaalde preview versie.

```bash
# from 0:20 to 1:03
ffmpeg -ss 00:00:20.0 -i 'Weesperplein 2022-08-19.mov' -c copy -t 00:00:43.0 'weesperplein.mov'
# convert to png sequence
ffmpeg -i 'weesperplein.mov' '/Volumes/Samsung_X5/weesperplein2/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i weesperplein.mov -vf scale=480:270 weesperplein_preview.mov
# convert preview to png sequence
ffmpeg -i weesperplein_preview.mov '/Volumes/Samsung_X5/weesperplein2/frames_preview/frame_%05d.png'
```

## Tijd

10 seconden is 5 maten bij 120 BPM.

## Render

```bash
# png to mp4 (from index 254 met 25 FPS
ffmpeg -framerate 25 -start_number 254 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperplein3portrait-video-x1.mp4
# repeat 6 times, 250 frames, video only
ffmpeg -i weesperplein3portrait-video-x1.mp4 -filter_complex "loop=loop=5:size=250:start=0" weesperplein3portrait-video-x6.mp4
```

