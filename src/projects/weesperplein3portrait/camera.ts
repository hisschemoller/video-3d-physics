/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-param-reassign */
import { ProjectSettings } from '@app/interfaces';
import { PerspectiveCamera } from 'three';
import { createActor } from '@app/actor';
import { getMatrix4 } from '@app/utils';

export default async function initCamera_1(
  projectSettings: ProjectSettings,
  pCamera: PerspectiveCamera,
) {
  // weesperplein 3 pattern duration is 42.53333333333333 seconds

  const matrices = [
    // getMatrix4({ x: -4, y: 0, z: -1, ry: Math.PI * -0.2 }), // heel dichtbij
    getMatrix4({ x: -2, y: 0, z: 7, ry: Math.PI * -0.4 }), // de straat vanaf enigszins links
    getMatrix4({ x: -9, y: 0, z: 10, ry: Math.PI * -0.35 }), // begin rode vrachtwagen
    getMatrix4({ x: 13, y: 0, z: 12, ry: Math.PI * 0.35 }), // volg rode vrachtwagen
    getMatrix4({ x: -2, y: 0, z: 3, ry: Math.PI * 0.46 }), // dichtbij rode vrachtwagen
    getMatrix4({ x: 0, y: 0, z: 10, ry: Math.PI * 0 }), // gebarende vrouw

    // getMatrix4({ x: 0, y: 0, z: 0, ry: 0 }), dichtbij beginnen
    // getMatrix4({ x: 3, y: 0, z: 10, ry: Math.PI * 0.2 }), // fietsers vanaf enigszins rechts
    // getMatrix4({ x: -6, y: 0, z: 12, ry: Math.PI * -0.2 }), // de straat vanaf enigszins links
    // getMatrix4({ x: 8, y: 0, z: 14, ry: Math.PI * 0.1 }), // begin tram
    // getMatrix4({ x: 11, y: 0, z: 8, ry: Math.PI * 0.4 }), // tram gevolgd
    // getMatrix4({ x: -4, y: 0, z: -1, ry: Math.PI * -0.2 }), // heel dichtbij
    // getMatrix4({ x: -5, y: 0, z: 2, ry: Math.PI * -0.45 }), // naar rechts kijkend
    // getMatrix4({ x: -9, y: 0, z: 10, ry: Math.PI * -0.35 }), // witte busje
    // getMatrix4({ x: 8, y: 0, z: 14, ry: Math.PI * 0.1 }),  // begin rode vrachtwagen
    // getMatrix4({ x: 9, y: 0, z: 12, ry: Math.PI * 0.4 }), // rode vrachtwagen gevolgd
    // getMatrix4({ x: 0, y: 0, z: 10, ry: Math.PI * 0 }), // vrouw met gebaar
    // getMatrix4({ x: 10, y: 0, z: 6, ry: Math.PI * 0.1 }), // recht begin auto's
    // getMatrix4({ x: -10, y: 0, z: 18, ry: Math.PI * -0.1 }), // auto's volgen
    // getMatrix4({ x: 32, y: 0, z: 48, ry: Math.PI * 0.17 }), // ver weg
  ];

  const tweenPoints = [
    {  // naar de rode vrachtwagen
      delay: 0.1,
      duration: 1.9,
      fromMatrix4: matrices[0],
      toMatrix4: matrices[1],
    },
    {  // volg rode vrachtwagen
      delay: 2,
      duration: 2.4,
      fromMatrix4: matrices[1],
      toMatrix4: matrices[2],
    },
    { // naar dichtbij rode vrachtwagen
      delay: 2 + 2.4,
      duration: 2.2,
      fromMatrix4: matrices[2],
      toMatrix4: matrices[3],
    },
    { // naar gebarende vrouw
      delay: 2 + 2.4 + 2.2, // = 7.6
      duration: 1.5, // 7.6 + 1.5 = 9.1
      fromMatrix4: matrices[3],
      toMatrix4: matrices[4],
    },
    { // terug naar de straat vanaf enigszins links
      delay: 0.5 + 2 + 2.4 + 2.2 + 1.5, // = 8.6
      duration: 1.5,
      fromMatrix4: matrices[4],
      toMatrix4: matrices[0],
    },


    // { // naar fietsers
    //   delay: pd * 0.02,
    //   duration: pd * 0.089,
    //   fromMatrix4: matrices[0],
    //   toMatrix4: matrices[1],
    // },
    // { // naar overzicht straat
    //   delay: pd * 0.11,
    //   duration: pd * 0.059,
    //   fromMatrix4: matrices[1],
    //   toMatrix4: matrices[2],
    // },
    // { // naar begin tram
    //   delay: pd * 0.17,
    //   duration: pd * 0.049,
    //   fromMatrix4: matrices[2],
    //   toMatrix4: matrices[3],
    // },
    // { // tram volgen
    //   delay: pd * 0.22,
    //   duration: pd * 0.109,
    //   fromMatrix4: matrices[3],
    //   toMatrix4: matrices[4],
    // },
    // { // naar heel dichtbij
    //   delay: pd * 0.33,
    //   duration: pd * 0.059,
    //   fromMatrix4: matrices[4],
    //   toMatrix4: matrices[5],
    // },
    // { // heel rechts kijkend
    //   delay: pd * 0.39,
    //   duration: pd * 0.099,
    //   fromMatrix4: matrices[5],
    //   toMatrix4: matrices[6],
    // },
    // { // naar witte busje
    //   delay: pd * 0.49,
    //   duration: pd * 0.059,
    //   fromMatrix4: matrices[6],
    //   toMatrix4: matrices[7],
    // },
    // { // naar begin rode vrachtwagen
    //   delay: pd * 0.55, // 42.53333333333333 * 
    //   duration: pd * 0.059,
    //   fromMatrix4: matrices[7],
    //   toMatrix4: matrices[8],
    // },
    // { // volg rode vrachtwagen
    //   delay: pd * 0.61, // 42.53333333333333 * 0.61 = 25.94533333333333
    //   duration: pd * 0.099, // 42.53333333333333 * 0.099 = 4.2108
    //   fromMatrix4: matrices[8],
    //   toMatrix4: matrices[9],
    // },
    // { // naar vrouw met gebaar
    //   delay: pd * 0.71, // 42.53333333333333 * 0.71 = 30.198666666666664
    //   duration: pd * 0.039, // 42.53333333333333 * 0.039 = 1.6587999999999998
    //   fromMatrix4: matrices[9],
    //   toMatrix4: matrices[10],
    // },
    // { // naar rechts begin auto's
    //   delay: pd * 0.75,
    //   duration: pd * 0.059,
    //   fromMatrix4: matrices[10],
    //   toMatrix4: matrices[11],
    // },
    // { // auto's volgen
    //   delay: pd * 0.81,
    //   duration: pd * 0.099,
    //   fromMatrix4: matrices[11],
    //   toMatrix4: matrices[12],
    // },
    // { // ver weg
    //   delay: pd * 0.91,
    //   duration: pd * 0.039,
    //   fromMatrix4: matrices[12],
    //   toMatrix4: matrices[13],
    // },
    // {
    //   delay: pd * 0.95,
    //   duration: pd * 0.049,
    //   fromMatrix4: matrices[13],
    //   toMatrix4: matrices[0],
    // },
  ];

  const cameraActor = await createActor(projectSettings, undefined, {
    box: { w: 0.01, h: 0.01, d: 0.01 },
    depth: 0.01,
    imageRect: { w: 1, h: 1, }
  });
  cameraActor.getMesh().add(pCamera);
  cameraActor.getMesh().position.copy(pCamera.position);
  cameraActor.getMesh().quaternion.copy(pCamera.quaternion);

  pCamera.position.set(0, 0, 0);

  tweenPoints.forEach((tweenPoint) => cameraActor.addTween({
    ...tweenPoint,
    ease: 'sineInOut',
  }));
}
