import { THREE } from 'enable3d';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { Font } from 'three/examples/jsm/loaders/FontLoader.js';
import { ProjectSettings, VideoData } from '@app/interfaces';
import VideoBox from './video-box';
import { BBox } from './weesperplein-a';

const COLOR = 0x0000ff;

/**
 * Create box.
 */
export async function createBox(
  id: number,
  bbox: [number, number, number, number],
  svgScale: number,
  parent: THREE.Object3D,
  projectSettings: ProjectSettings,
  videoData: VideoData,
  bBoxes: BBox[],
  font: Font,
) {
  const [x, y, width, height] = bbox;
  const positions = [0, 0, 0, 1, 0, 0, 1, -1, 0, 0, -1, 0, 0, 0, 0];
  const geometry = new LineGeometry();
  geometry.setPositions(positions);

  const material = new LineMaterial({
    color: COLOR,
    linewidth: 0.002,
    vertexColors: false,
    dashed: false,
    alphaToCoverage: true,
  });

  const box = new Line2(geometry, material);
  box.computeLineDistances();
  box.scale.set(width * svgScale, height * svgScale, 1);
  box.castShadow = true;
  box.receiveShadow = true;
  box.position.set(x * svgScale, -y * svgScale, 0.4);
  parent.add(box);

  const textMessage = id.toString();
  const textShapes = font.generateShapes(textMessage, 0.15);
  const textGeometry = new THREE.ShapeGeometry(textShapes);
  const textMaterial = new THREE.MeshBasicMaterial({ color: COLOR, side: THREE.DoubleSide });
  const textMesh = new THREE.Mesh(textGeometry, textMaterial);
  textMesh.position.copy(box.position).y += 0.05;
  parent.add(textMesh);

  const videoBox = new VideoBox(projectSettings, svgScale, videoData, parent, id);
  videoBox.create(bbox);

  bBoxes.push({ id, box, textMesh, videoBox });
}

/**
 * Update box.
 */
export function updateBox(
  bBox: BBox,
  bbox: [number, number, number, number],
  svgScale: number,
  img: HTMLImageElement,
) {
  const { box, textMesh, videoBox } = bBox;
  const [x, y, width, height] = bbox;
  box.scale.set(width * svgScale, height * svgScale, 1);
  box.position.set(x * svgScale, -y * svgScale, 0.1);

  textMesh.position.copy(box.position).y += 0.05;

  videoBox.update(img, bbox);
}

/**
 * Delete box.
 */
export function deleteBox(
  bBoxes: BBox[],
  index: number,
  parent: THREE.Object3D,
) {
  const { box, textMesh, videoBox } = bBoxes[index];
  
  videoBox.delete();
  parent.remove(box);
  parent.remove(textMesh);
  bBoxes.splice(index, 1);
}
