type Voice = {
  id: number;
  osc: OscillatorNode;
  pan: StereoPannerNode;
};

const voices: Voice[] = [];
let audioCtx: AudioContext;
let gain: GainNode;

/**
 * Initialise the audio context.
 */
export function initAudio() {
  audioCtx = new window.AudioContext();
  gain = audioCtx.createGain();
  gain.gain.value = 0.025;
  gain.connect(audioCtx.destination);
}

export function startAudioVoice(id: number, x: number) {
  const midiPitch = 60 + (id % 24);
  const freq = (2 ** ((midiPitch - 60) / 12)) * 440;
  const pan = audioCtx.createStereoPanner();
  pan.pan.setValueAtTime((x - 1000) / 1000, audioCtx.currentTime);
  pan.connect(gain);
  const osc = audioCtx.createOscillator();
  osc.type = 'sine';
  osc.frequency.setValueAtTime(freq, audioCtx.currentTime);
  osc.connect(pan);
  osc.start();
  voices.push({ id, osc, pan });
}

export function stopAudioVoice(id: number) {
  const index = voices.findIndex((v) => v.id === id);
  if (index !== -1) {
    voices[index].osc.stop();
    voices.splice(index, 1);
  }
}
