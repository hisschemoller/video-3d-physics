import { THREE } from 'enable3d';
import { createActor } from '@app/actor';
import { ImageData, ProjectSettings, VideoData } from '@app/interfaces';
import { getMatrix4 } from '@app/utils';
import createTween from '@app/tween';
import { startAudioVoice, stopAudioVoice } from './audio';

export interface DetectedObjectExt {
  bbox: [number, number, number, number];
  class: string;
  id: number;
  score: number;
}

type BBox = {
  id: number;
  mesh: THREE.Mesh;
};

const COLORS = [
  '#00ff00',
  '#ff0000',
  '#0000ff',
  '#00ffff',
  '#ffff00',
  '#ff00ff',
  '#000000',
  '#ffffff',
  '#555555',
  '#aaaaaa',
  '#ff9900',
  '#9933ff',
];

let motiondata: DetectedObjectExt[][];
let motionDataStartIndex: number;
let svgScale: number;
let group: THREE.Group;
let dataIndex: number;
let dataStep: number;
let videoStartTime: number;
const bBoxes: BBox[] = [];

function updateBox(mesh: THREE.Mesh, bbox: [number, number, number, number]) {
  const [x, y, width, height] = bbox;
  mesh.scale.set(width * svgScale, height * svgScale, 1);
  mesh.position.set((x + (width / 2)) * svgScale, (-y - (height / 2)) * svgScale, 0);
}

function createBox(id: number, bbox: [number, number, number, number]) {
  const mesh = new THREE.Mesh(
    new THREE.BoxBufferGeometry(1, 1, 0.5),
    new THREE.MeshStandardMaterial({ color: COLORS[id % COLORS.length] }),
  );
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  updateBox(mesh, bbox);
  group.add(mesh);
  bBoxes.push({ id, mesh });

  startAudioVoice(id, bbox[0]);
}

function deleteBox(index: number) {
  stopAudioVoice(bBoxes[index].id);
  group.remove(bBoxes[index].mesh);
  bBoxes.splice(index, 1);
}

function onMotionTweenStart() {
  dataIndex = motionDataStartIndex;
  motiondata[0].forEach((data) => {
    const { bbox, id } = data;
    createBox(id, bbox);
  });
}

/**
 *
 */
function onMotionTweenUpdate() {
  // the motion data frame to use in this update cycle
  dataIndex += dataStep;

  // update all motion objects in the scene
  motiondata[dataIndex].forEach((data) => {
    const { bbox, id } = data;

    // find if the id already exists in the bBoxes
    const bBox = bBoxes.find((bb) => bb.id === id);
    if (bBox) {
      // re-position the existing mesh
      updateBox(bBox.mesh, bbox);
    } else {
      // create a new bbox mesh
      createBox(id, bbox);
    }

    // find bboxes that don't exist anymore in the current frame
    for (let i = bBoxes.length - 1; i >= 0; i -= 1) {
      const b = motiondata[dataIndex].find((d) => d.id === bBoxes[i].id);
      if (!b) {
        deleteBox(i);
      }
    }
  });
}

function onMotionTweenComplete() {
  for (let i = bBoxes.length - 1; i >= 0; i -= 1) {
    deleteBox(i);
  }
}

export default async function createWeesperplein(
  projectSettings: ProjectSettings,
  media: { [key: string]: VideoData | ImageData | undefined },
  fps: number,
  videoStart: number,
) {
  const {
    patternDuration, scene, stepDuration, timeline, width, width3d,
  } = projectSettings;

  const videoFps = (media.video as unknown as VideoData).fps;
  dataStep = videoFps / fps;
  svgScale = width3d / width;
  videoStartTime = videoStart;
  motionDataStartIndex = Math.round(videoStartTime * videoFps);

  group = new THREE.Group();
  group.position.set(-8, 4.5, -5);
  group.add(new THREE.AxesHelper());
  scene.add(group);

  const actor = await createActor(projectSettings, media.video, {
    svg: { scale: svgScale, url: '../assets/projects/weesperplein2/alles.svg' },
    imageRect: { w: 1920, h: 1080 },
    depth: 0.05,
    color: 0x5B783D,
  });
  actor.setStaticPosition(getMatrix4({ x: 0, y: 0, z: 0 }));
  actor.addTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    videoStart: videoStartTime,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
  actor.getMesh().castShadow = true;
  actor.getMesh().receiveShadow = true;
  group.add(actor.getMesh());

  const response = await fetch('../assets/projects/weesperplein2/weesperplein-motiondata-2.json');
  motiondata = await response.json() as DetectedObjectExt[][];

  const tween = createTween({
    delay: stepDuration * 1,
    duration: patternDuration * 0.99,
    onStart: onMotionTweenStart,
    onUpdate: onMotionTweenUpdate,
    onComplete: onMotionTweenComplete,
  });
  timeline.add(tween);
}
