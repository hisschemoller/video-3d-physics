# Weesperplein 2

Weesperplein 2022-08-19.mov\
1920 x 1080\
3:31

```bash
# extract frame 600 as an image
ffmpeg -i 'Weesperplein 2022-08-19.mov' -vf "select=eq(n\,599)" -vframes 1 'Weesperplein 2022-08-19 frame 600.jpg'
# from 0:20 to 1:03
ffmpeg -ss 00:00:20.0 -i 'Weesperplein 2022-08-19.mov' -c copy -t 00:00:43.0 'weesperplein.mov'
# convert to png sequence
ffmpeg -i 'weesperplein.mov' '/Volumes/Samsung_X5/weesperplein2/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i weesperplein.mov -vf scale=480:270 weesperplein_preview.mov
# convert preview to png sequence
ffmpeg -i weesperplein_preview.mov '/Volumes/Samsung_X5/weesperplein2/frames_preview/frame_%05d.png'
```

## Render

```bash
# png to mp4 (from index 6 met 30 FPS
ffmpeg -framerate 30 -start_number 6 -i /Volumes/Samsung_X5/weesperplein2-rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p weesperplein2-video-x1.mp4
# repeat 4 times, 1280 frames, video only
ffmpeg -i weesperplein2-video-x1.mp4 -filter_complex "loop=4:size=1280:start=0" weesperplein2-video-x4.mp4
# repeat 8 times, audio
ffmpeg -stream_loop 4 -i weesperplein2-audio-x1.wav -c copy weesperplein2-audio-x4.wav
# video en audio samenvoegen
ffmpeg -i weesperplein2-video-x4.mp4 -i weesperplein2-audio-x4.wav -vcodec copy weesperplein2-x4.mp4
# scale to 50%, 960 * 540
ffmpeg -i weesperplein2-x4.mp4 -vf scale=960:540 weesperplein2-x4_halfsize.mp4
```

## Muziek

Video duurt 1280 frames, dus 1280 / 30 = 42.666666666666664 seconden

## Beschrijving

Verkeer op het Weesperplein dat gedetecteerd is met AI object detectie software (Tensorflow en COCO-SSD). 