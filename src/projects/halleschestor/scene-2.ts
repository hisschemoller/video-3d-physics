import { THREE } from 'enable3d';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { GltfLibrary, MediaLibrary, ProjectSettings, TextureLibrary } from '@app/interfaces';
import { AnimatedModel } from './animated-model';
import createTween from '@app/tween';

const animatedModels: AnimatedModel[] = [];
const START_TIME = 2;
const scale = 0.85;

function addDuiven(
  projectSettings: ProjectSettings,
  textures: TextureLibrary,
  gltfs: GltfLibrary,
) {
  const { duiven2, duiven3 } = gltfs;

  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      duiven3,
      'Person6ToPigeon5Armature',
      'Person6ToPigeon5Bored',
      new THREE.Vector3(-8, 0, -13),
      new THREE.Vector3(-0.2, 0, -1).normalize(),
      0,
      scale,
    )
  );

  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      duiven3,
      'Person6ToPigeon2Armature',
      'Person6ToPigeon2WeightShift',
      new THREE.Vector3(-10, 0, -14),
      new THREE.Vector3(0.9, 0, 0).normalize(),
      0,
      scale,
    )
  );

  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      duiven3,
      'Person6ToPigeon4Armature',
      'Person6ToPigeon4LookingAround',
      new THREE.Vector3(-9, 0, -16),
      new THREE.Vector3(0.2, 0, -1).normalize(),
      0,
      scale,
    )
  );

  // animatedModels.push(
  //   new AnimatedModel(
  //     projectSettings,
  //     textures,
  //     duiven3,
  //     'Person6ToPigeon6Armature', // donkere duif
  //     'Person6ToPigeon6OrcIdle',
  //     new THREE.Vector3(-9, 0, -9),
  //     new THREE.Vector3(0.2, 0, 1).normalize(),
  //     0,
  //     scale,
  //   )
  // );

  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      duiven3,
      'Person6ToPigeon6Armature', // donkere duif 2
      'Person6ToPigeon6OrcIdle',
      new THREE.Vector3(-12, 0, -20),
      new THREE.Vector3(1, 0, -1).normalize(),
      0,
      scale,
    )
  );

  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      duiven2,
      'Pigeon5WalkingArmature',
      'Pigeon5WalkingAnimation',
      new THREE.Vector3(12, 0, -50),
      new THREE.Vector3(-0.4, 0, 1).normalize(),
      0.08,
      1.1,
    )
  );

  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      duiven2,
      'Person6ToPigeon2Armature',
      'Person6ToPigeon2Walking',
      new THREE.Vector3(-28, 0, 20),
      new THREE.Vector3(0.3, 0, -1).normalize(),
      0.08,
      scale,
    )
  );
}

function animateCamera(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
) {
  const { patternDuration, timeline } = projectSettings;
  const px = camera.position.x;
  const py = camera.position.y;
  const pz = camera.position.z;
  const ry = camera.rotation.y;

  timeline.add(createTween({
    delay: START_TIME,
    duration: patternDuration,
    ease: 'linear',
    onStart: () => {},
    onUpdate: (progress) => {
      const sine2 = Math.sin(progress * Math.PI * 2 * 2);
      const sine4 = Math.sin(progress * Math.PI * 2 * 4);
      const sine8 = Math.sin(progress * Math.PI * 2 * 8);
      // const sine16 = Math.sin(progress * Math.PI * 2 * 16);
      camera.position.x = px + sine2 * -4;
      camera.position.y = py + sine4 * 0.5;
      // camera.position.z = pz - 4 + sine4 * 4;
      camera.rotation.y = ry + sine8 * 0.6;
    },
    onComplete: () => {},
  }));
}

export async function initScene2(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
  orbitControls: OrbitControls,
  directionalLight: THREE.DirectionalLight,
  media: MediaLibrary,
  textures: TextureLibrary,
  gltfs: GltfLibrary,
) {
  const { scene3d } = projectSettings;

  // CAMERA
  camera.position.set(-2, 1.6, -16);
  const cameraTarget = new THREE.Vector3().addVectors(camera.position, new THREE.Vector3(-3.5, 0, 0));
  camera.lookAt(cameraTarget);
  camera.updateProjectionMatrix();

  // ORBIT CONTROLS
  orbitControls.target = cameraTarget;
  orbitControls.update();
  orbitControls.saveState();

  // DUIVEN
  addDuiven(projectSettings, textures, gltfs);

  // CAMERA ANIMATION
  // animateCamera(projectSettings, camera);
  
  // UPDATE
  const update = async (delta: number) => {
    animatedModels.forEach((m) => m.update(delta));
  }

  return { update };
}
