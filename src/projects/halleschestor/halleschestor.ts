import { THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { GltfLibrary, ProjectSettings, TextureLibrary } from '@app/interfaces';
import { createImageTexture } from '@app/utils';

function addGround(
  projectSettings: ProjectSettings,
  gltfOmgeving: GLTF,
) {
  const { scene3d } = projectSettings;

  const texture_m1_0 = createImageTexture('../assets/projects/halleschestor/ground_-1_0.jpg');
  const ground_m1_0 = (gltfOmgeving.scene.getObjectByName('Ground_-1_0') as THREE.Mesh);
  ground_m1_0.material = new THREE.MeshPhongMaterial({ map: texture_m1_0  });
  ground_m1_0.position.set(-25, 0, -10);
  ground_m1_0.castShadow = false;
  ground_m1_0.receiveShadow = true;
  scene3d.scene.add(ground_m1_0);

  const texture_p1_0 = createImageTexture('../assets/projects/halleschestor/ground_+1_0.jpg');
  const ground_p1_0 = (gltfOmgeving.scene.getObjectByName('Ground_+1_0') as THREE.Mesh);
  ground_p1_0.material = new THREE.MeshPhongMaterial({ map: texture_p1_0  });
  ground_p1_0.position.set(25, 0, -10);
  ground_p1_0.castShadow = false;
  ground_p1_0.receiveShadow = true;
  scene3d.scene.add(ground_p1_0);

  const texture_m1_p1 = createImageTexture('../assets/projects/halleschestor/ground_-1_+1.jpg');
  const ground_m1_p1 = (gltfOmgeving.scene.getObjectByName('Ground_-1_-1') as THREE.Mesh);
  ground_m1_p1.material = new THREE.MeshPhongMaterial({ map: texture_m1_p1  });
  ground_m1_p1.position.set(-25, 0, -10 + 50);
  ground_m1_p1.castShadow = false;
  ground_m1_p1.receiveShadow = true;
  scene3d.scene.add(ground_m1_p1);

  const texture_p1_p1 = createImageTexture('../assets/projects/halleschestor/ground_+1_+1.jpg');
  const ground_p1_p1 = (gltfOmgeving.scene.getObjectByName('Ground_+1_-1') as THREE.Mesh);
  ground_p1_p1.material = new THREE.MeshPhongMaterial({ map: texture_p1_p1  });
  ground_p1_p1.position.set(25, 0, -10 + 50);
  ground_p1_p1.castShadow = false;
  ground_p1_p1.receiveShadow = true;
  scene3d.scene.add(ground_p1_p1);
}

function addMehringplatz(
  projectSettings: ProjectSettings,
  gltfOmgeving: GLTF,
  textures: TextureLibrary,
) {
  const { scene3d } = projectSettings;
  const { omgeving1, omgeving2 } = textures;

  const building = (gltfOmgeving.scene.getObjectByName('Mehringplatz') as THREE.Mesh);
  building.material = new THREE.MeshPhongMaterial({ map: omgeving1  });
  building.position.set(0, 0, 0);
  scene3d.scene.add(building);

  building.traverse((obj3d) => {
    obj3d.castShadow = true;
    obj3d.receiveShadow = true;
  });

  const uBahn = (gltfOmgeving.scene.getObjectByName('U-Bahn') as THREE.Mesh);
  building.material = new THREE.MeshPhongMaterial({ map: omgeving2  });
  uBahn.position.set(0, 0, 0);
  uBahn.castShadow = true;
  uBahn.receiveShadow = true;
  scene3d.scene.add(uBahn);
}

function addWindschermen(
  projectSettings: ProjectSettings,
  gltfDingen: GLTF,
  textures: TextureLibrary,
) {
  const { scene3d } = projectSettings;
  const { dingen1 } = textures;

  const group = new THREE.Group();
  group.position.set(-18, 0, 0 );
  group.rotateY(Math.PI * -0.05);
  scene3d.scene.add(group);

  const scherm = (gltfDingen.scene.getObjectByName('Windscherm') as THREE.Mesh);
  scherm.material = new THREE.MeshPhongMaterial({ map: dingen1  });
  scherm.castShadow = true;
  scherm.receiveShadow = true;

  scherm.position.set(2.3, 0, 0 );
  group.add(scherm);

  const scherm2 = scherm.clone();
  scherm2.position.set(6.9, 0, 0);
  group.add(scherm2);

  const scherm3 = scherm.clone();
  scherm3.position.set(9.2, 0, -6.9);
  scherm3.rotateY(Math.PI / 2);
  group.add(scherm3);

  const scherm4 = scherm.clone();
  scherm4.position.set(6.9, 0, -9.2);
  group.add(scherm4);

  const scherm5 = scherm.clone();
  scherm5.position.set(2.3, 0, -9.2);
  group.add(scherm5);
}

function addRoundBackground(
  projectSettings: ProjectSettings,
  gltfOmgeving: GLTF,
  textures: TextureLibrary,
) {
  const { scene3d } = projectSettings;
  const { omgeving3 } = textures;

  const mesh = (gltfOmgeving.scene.getObjectByName('RoundBackground') as THREE.Mesh);
  mesh.material = new THREE.MeshBasicMaterial({ map: omgeving3, side: THREE.DoubleSide  });
  mesh.position.set(0, 11, -67);
  mesh.castShadow = false;
  mesh.receiveShadow = false;
  scene3d.scene.add(mesh);
}

function addTestGround(
  projectSettings: ProjectSettings,
) {
  const { scene3d } = projectSettings;

  const texture = createImageTexture('../assets/projects/test/testimage3d.jpg');
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(10, 0.05, 10),
    new THREE.MeshPhongMaterial({ map: texture, side: THREE.DoubleSide }),
  );
  mesh.receiveShadow = true;
  scene3d.scene.add(mesh);
}

function addTrees(
  projectSettings: ProjectSettings,
  gltfOmgeving: GLTF,
  textures: TextureLibrary,
) {
  const { scene3d } = projectSettings;
  const { omgeving3 } = textures;

  // const treesEast = (gltfOmgeving.scene.getObjectByName('Bomen_oost') as THREE.Mesh);
  // treesEast.material = new THREE.MeshBasicMaterial({ map: omgeving3, side: THREE.DoubleSide });
  // treesEast.position.set(40, 15, 16);
  // treesEast.rotation.set(0, Math.PI * (-15 / 180), 0);
  // treesEast.castShadow = false;
  // treesEast.receiveShadow = false;
  // scene3d.scene.add(treesEast);

  const treesWest = (gltfOmgeving.scene.getObjectByName('Bomen_west') as THREE.Mesh);
  treesWest.material = new THREE.MeshBasicMaterial({ map: omgeving3, side: THREE.DoubleSide });
  treesWest.position.set(-40, 15, 16);
  treesWest.rotation.set(0, Math.PI * (-165 / 180), 0);
  treesWest.castShadow = false;
  treesWest.receiveShadow = false;
  scene3d.scene.add(treesWest);

  const treesEast2 = (gltfOmgeving.scene.getObjectByName('Bomen_oost2') as THREE.Mesh);
  treesEast2.material = new THREE.MeshBasicMaterial({ map: omgeving3, side: THREE.DoubleSide });
  treesEast2.position.set(28, 20, 63);
  treesEast2.rotation.set(0, Math.PI * (-40 / 180), 0);
  treesEast2.scale.set(1.3, 1.3, 1.3);
  treesEast2.castShadow = false;
  treesEast2.receiveShadow = false;
  scene3d.scene.add(treesEast2);
}

export function initHalleschesTor(
  projectSettings: ProjectSettings,
  gltfs: GltfLibrary,
) {
  const { omgeving, dingen } = gltfs;

  const textures: TextureLibrary = {
    omgeving1: createImageTexture('../assets/projects/halleschestor/halleschestor-omgeving-1.jpg'),
    omgeving2: createImageTexture('../assets/projects/halleschestor/halleschestor-omgeving-2.jpg'),
    omgeving3: createImageTexture('../assets/projects/halleschestor/halleschestor-omgeving-3.jpg'),
    dingen1: createImageTexture('../assets/projects/halleschestor/halleschestor-dingen-1.jpg'),
  };

  // addTestGround(projectSettings);
  addGround(projectSettings, omgeving);
  addMehringplatz(projectSettings, omgeving, textures);
  addTrees(projectSettings, omgeving, textures);
  addRoundBackground(projectSettings, omgeving, textures);
  addWindschermen(projectSettings, dingen, textures);
}
