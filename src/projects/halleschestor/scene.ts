/* eslint-disable object-curly-newline */
import { THREE } from 'enable3d';
import { GltfLibrary, MediaLibrary, ProjectSettings, TextureLibrary } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import { initHalleschesTor } from './halleschestor';
import { initScene1 } from './scene-1';
import { initScene2 } from './scene-2';
import { createImageTexture } from '@app/utils';

const PROJECT_PREVIEW_SCALE = 0.25;
const BPM = 108; // 128; // was 108
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 80; // 32; // was 80
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = (SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES) + 0;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  dirLightTarget = new THREE.Object3D();

  currentScenes: { update: (delta: number) => Promise<void> }[] = [];

  constructor() {
    super();

    this.width = 1920;
    this.canvasHeight = 1080;
    this.viewportHeight = 1080;
    this.width3d = 16;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 15;
    this.captureFps = 30;
    this.captureThrottle = 15; // wait 15 raf ticks per render
    this.captureDuration = 64; // was (PATTERN_DURATION * 2) + 1;
    this.clearColor = 0xb0b8c5;
    this.captureShadowMapSize = 4096;
    this.shadowSize = 16;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = this.width;
    this.captureCanvas.height = this.viewportHeight;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;
  }

  async create() {
    await super.create();

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();
    // this.physics.setGravity(0, 0, 0);

    this.renderer.setSize(this.width, this.canvasHeight);

    // DIRECTIONAL LIGHT
    this.scene.add(this.dirLightTarget);
    this.directionalLight.intensity = 1 * Math.PI;
    this.directionalLight.position.set(-7, 30, 15);
    this.directionalLight.target = this.dirLightTarget;

    // AMBIENT LIGHT
    this.ambientLight.intensity = 0.5 * Math.PI;

    // MEDIA
    const mediaVideo = {
      fps: 30,
      height: 1080,
      scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
      width: 1920,
    };

    const getImageSrcPath = (dirName: string) => isPreview
      ? `../assets/projects/halleschestor/${dirName}_frames_preview/frame_#FRAME#.png`
      : `fs-img?dir=/Volumes/Samsung_X5/${dirName}/frames/&img=frame_#FRAME#.png`;
    
    const media: MediaLibrary = {
      halleschestor_6174: {
        ...mediaVideo,
        imgSrcPath: getImageSrcPath('halleschestor_6174'),
      },
      munt_7909_reguliers: {
        ...mediaVideo,
        imgSrcPath: getImageSrcPath('munt_7909'),
      },
      testImage1024: {
        height: 1024,
        imgSrc: '../assets/projects/test/testimage3d.jpg',
        width: 1024,
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.width * (9 / 16),
      height3d: this.width3d * (9 / 16),
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    const textures: TextureLibrary = {
      DuifTextuur1: createImageTexture('../assets/projects/halleschestor/duif-textuur1.jpg'),
      PersonTextures: createImageTexture('../assets/projects/halleschestor/person-textures.jpg'),
      PersonTextures2: createImageTexture('../assets/projects/halleschestor/person-textures2.jpg'),
    };

    const gltfMensen1 = await this.load.gltf('../assets/projects/halleschestor/halleschestor-mensen1.glb');
    const gltfMensen2 = await this.load.gltf('../assets/projects/halleschestor/halleschestor-mensen2.glb');
    const gltfOmgeving = await this.load.gltf('../assets/projects/halleschestor/halleschestor-omgeving.glb');
    const gltfDingen = await this.load.gltf('../assets/projects/halleschestor/halleschestor-dingen.glb');
    const gltfDuiven2 = await this.load.gltf('../assets/projects/halleschestor/halleschestor-duiven2.glb');
    const gltfDuiven3 = await this.load.gltf('../assets/projects/halleschestor/halleschestor-duiven3.glb');
    const gltfDuivenTest = await this.load.gltf('../assets/projects/halleschestor/halleschestor-duiven-test.glb');
    console.log('gltfDuiven2', gltfDuiven2);
    console.log('gltfDuiven3', gltfDuiven3);

    const gltfs: GltfLibrary = {
      mensen1: gltfMensen1,
      mensen2: gltfMensen2,
      omgeving: gltfOmgeving,
      dingen: gltfDingen,
      duiven2: gltfDuiven2,
      duiven3: gltfDuiven3,
      duivenTest: gltfDuivenTest,
    };
    
    initHalleschesTor(projectSettings, gltfs);

    // const s1 = await initScene1(
    //   projectSettings,
    //   this.pCamera,
    //   this.orbitControls,
    //   this.directionalLight,
    //   media,
    //   textures,
    //   gltfs,
    //   15 / this.fps,
    // );
    // this.currentScenes.push(s1);

    const s2 = await initScene2(
      projectSettings,
      this.pCamera,
      this.orbitControls,
      this.directionalLight,
      media,
      textures,
      gltfs,
    );
    this.currentScenes.push(s2);

    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    this.currentScenes.forEach((s) => s.update(delta));
    super.updateAsync(time, delta);
  }

  captureImage() {
    this.captureCanvasContext.drawImage(
      this.renderer.domElement, 0, 0, this.width, this.viewportHeight, 0, 0, this.width, this.viewportHeight,
    );
    return this.captureCanvas.toDataURL();
  }
}
