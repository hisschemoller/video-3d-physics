import { THREE } from 'enable3d';
import { GltfLibrary, MediaLibrary, ProjectSettings, TextureLibrary } from '@app/interfaces';
import { createActor } from '@app/actor';
import { getMatrix4 } from '@app/utils';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { AnimatedModel } from './animated-model';
import createTween from '@app/tween';

const animatedModels: AnimatedModel[] = [];
const START_TIME = 2;

function addPeople(
  projectSettings: ProjectSettings,
  textures: TextureLibrary,
  gltfs: GltfLibrary,
  adjustSpeedForFPS: number,
) {
  const { mensen1 ,mensen2 } = gltfs;
  const scale = 0.8;
  
  const person1x = 5.3;
  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      mensen1,
      'Person1WalkingArmature',
      'Person1Walking',
      new THREE.Vector3(person1x + 1, 0, -33),
      new THREE.Vector3(0, 0, 1).normalize(),
      0.075 * adjustSpeedForFPS,
      scale,
    )
  );
  animatedModels.push( // vrouw met grijs haar
    new AnimatedModel(
      projectSettings,
      textures,
      mensen1,
      'Person2WalkingArmature',
      'Person2Walking',
      new THREE.Vector3(3, 0, 50),
      new THREE.Vector3(0, 0, -1).normalize(),
      0.1 * adjustSpeedForFPS,
      scale,
    )
  );
  animatedModels.push( // vrouw met zonnebril
    new AnimatedModel(
      projectSettings,
      textures,
      mensen1,
      'Person3WalkingArmature',
      'Person3Walking',
      new THREE.Vector3(8, 0, -28),
      new THREE.Vector3(0, 0, 1).normalize(),
      0.1 * adjustSpeedForFPS,
      scale,
    )
  );
  animatedModels.push( // blauw petje
    new AnimatedModel(
      projectSettings,
      textures,
      mensen1,
      'Person4WalkingArmature',
      'Person4Walking',
      new THREE.Vector3(3, 0, 31),
      new THREE.Vector3(0, 0, -1).normalize(),
      0.065 * adjustSpeedForFPS,
      scale,
    )
  );

  const person5x = 15; // jongetje
  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      mensen2,
      'Person5WalkingArmature',
      'Person5Walking',
      new THREE.Vector3(person5x + 2, 0, -60),
      new THREE.Vector3(0, 0, 1).normalize(),
      0.09 * adjustSpeedForFPS,
      scale,
    )
  );
  const person6x = -9; // jongen dichtbij
  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      mensen2,
      'Person6WalkingArmature',
      'Person6Walking',
      new THREE.Vector3(person6x - 3, 0, 51),
      new THREE.Vector3(0, 0, -1).normalize(),
      0.11 * adjustSpeedForFPS,
      scale,
    )
  );
  const person7x = -3; // meisje met rode tas
  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      mensen2,
      'Person7WalkingArmature',
      'Person7Walking',
      new THREE.Vector3(person7x - 1, 0, 55),
      new THREE.Vector3(0, 0, -1).normalize(),
      0.07 * adjustSpeedForFPS,
      scale,
    ),
  );
  const person8x = 9; // grijs petje
  animatedModels.push(
    new AnimatedModel(
      projectSettings,
      textures,
      mensen2,
      'Person8WalkingArmature',
      'Person8Walking',
      new THREE.Vector3(person8x + 4, 0, 31),
      new THREE.Vector3(0, 0, -1).normalize(),
      0.065 * adjustSpeedForFPS,
      scale,
    ),
  );
}

async function addVideo(
  projectSettings: ProjectSettings,
  media: MediaLibrary,
) {
  const { patternDuration, scene3d, width, width3d } = projectSettings;
  const svgScale = width3d / width;

  const actor = await createActor(projectSettings, media.halleschestor_6174, {
    svg: { scale: svgScale, url: '../assets/projects/halleschestor/1920x1080.svg' },
    imageRect: { w: 1920, h: 1080 },
    depth: 0.05,
    color: 0xa7b9c8,
    material: 'basic',
  });

  actor.addTween({
    delay: START_TIME,
    duration: patternDuration,
    isLooped: true,
    loopDuration: 84 - 3,
    videoStart: 0,
    fromImagePosition: new THREE.Vector2(0, 0),
  });

  const scale = 1.8;
  actor.setStaticPosition(getMatrix4({ x: 15, y: 11, z: 0, ry: Math.PI / -2, sx: scale, sy: scale }));
  actor.getMesh().castShadow = false;
  actor.getMesh().receiveShadow = false;
  scene3d.scene.add(actor.getMesh());
}

function animateCamera(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
) {
  const { patternDuration, timeline } = projectSettings;
  const px = camera.position.x;
  const py = camera.position.y;
  const pz = camera.position.z;
  const ry = camera.rotation.y;

  timeline.add(createTween({
    delay: START_TIME,
    duration: patternDuration,
    ease: 'linear',
    onStart: () => {},
    onUpdate: (progress) => {
      const sine2 = Math.sin(progress * Math.PI * 2 * 2);
      const sine4 = Math.sin(progress * Math.PI * 2 * 4);
      const sine8 = Math.sin(progress * Math.PI * 2 * 8);
      // const sine16 = Math.sin(progress * Math.PI * 2 * 16);
      camera.position.x = px + sine2 * -4;
      camera.position.y = py + sine4 * 0.5;
      // camera.position.z = pz - 4 + sine4 * 4;
      camera.rotation.y = ry + sine8 * 0.6;
    },
    onComplete: () => {},
  }));
}

export async function initScene1(
  projectSettings: ProjectSettings,
  camera: THREE.PerspectiveCamera,
  orbitControls: OrbitControls,
  directionalLight: THREE.DirectionalLight,
  media: MediaLibrary,
  textures: TextureLibrary,
  gltfs: GltfLibrary,
  adjustSpeedForFPS: number,
) {
  const { scene3d } = projectSettings;

  // CAMERA
  camera.position.set(-2, 1.8, 11);
  const cameraTarget = new THREE.Vector3().addVectors(camera.position, new THREE.Vector3(3, 0, 0));
  camera.lookAt(cameraTarget);
  camera.updateProjectionMatrix();

  // LIGHT & SHADOW
  const z = 10;
  const dirLightTarget = new THREE.Object3D();
  dirLightTarget.position.z = z;
  scene3d.scene.add(dirLightTarget);
  directionalLight.target = dirLightTarget;
  directionalLight.position.z += z;

  // ORBIT CONTROLS
  orbitControls.target = cameraTarget;
  orbitControls.update();
  orbitControls.saveState();

  // VIDEO
  addVideo(projectSettings, media);

  // PEOPLE
  addPeople(projectSettings, textures, gltfs, adjustSpeedForFPS);

  // CAMERA ANIMATION
  animateCamera(projectSettings, camera);
  
  // UPDATE
  const update = async (delta: number) => {
    animatedModels.forEach((m) => m.update(delta));
  }

  return { update };
}