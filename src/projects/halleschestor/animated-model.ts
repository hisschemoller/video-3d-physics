import { THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { ProjectSettings, TextureLibrary } from '@app/interfaces';

export class AnimatedModel {

  displacement: THREE.Vector3;

  group: THREE.Group;
  
  mixer: THREE.AnimationMixer;

  constructor(
    projectSettings: ProjectSettings,
    textures: TextureLibrary,
    gltf: GLTF,
    modelName: string,
    animationName: string,
    position: THREE.Vector3,
    direction: THREE.Vector3,
    speed: number,
    scale: number = 1,
  ) {
    const { scene3d } = projectSettings;
    this.displacement = direction.clone().multiplyScalar(speed);

    const mesh = (gltf.scene.getObjectByName(modelName) as THREE.Mesh);
    mesh.position.set(0, 0, 0);
    mesh.scale.multiplyScalar(scale);
    mesh.rotation.x = Math.PI / 2;

    mesh.traverse((object3d) => {
      if (object3d.type === 'Object3D' || object3d.type === 'SkinnedMesh') {
        object3d.castShadow = true;
        object3d.receiveShadow = true;

        if (object3d.type === 'SkinnedMesh') {
          const skinnedMesh = (object3d as THREE.SkinnedMesh);
          if (Array.isArray(skinnedMesh.material)) {
            const materials = skinnedMesh.material;
            materials.forEach((m, index) => {
              const texture = textures[m.name];
              if (textures) {
                materials[index] = new THREE.MeshPhongMaterial({ map: texture });
              }
            });
          } else {
            const texture = textures[skinnedMesh.material.name];
            if (texture) {
              skinnedMesh.material = new THREE.MeshPhongMaterial({ map: texture });
            }
          }

          // (object3d as THREE.SkinnedMesh).material = new THREE.MeshPhongMaterial({ map: texture });
        }
      }
    });

    this.group = new THREE.Group();
    this.group.position.copy(position);
    this.group.add(mesh);
    this.group.lookAt(position.clone().add(direction).setY(0));
    scene3d.scene.add(this.group);
  
    const skeleton = new THREE.SkeletonHelper(mesh);
    skeleton.visible = false;
    scene3d.scene.add(skeleton);
  
    const animationClip = gltf.animations.find((a) => a.name === animationName);
  
    this.mixer = new THREE.AnimationMixer(mesh);
  
    const walkAction = this.mixer.clipAction(animationClip as THREE.AnimationClip);
    walkAction.play();
  }

  update(delta: number) {
    this.mixer.update(delta);
    this.group.position.add(this.displacement);
  }
}