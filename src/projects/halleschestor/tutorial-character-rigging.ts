import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';

let mixer: THREE.AnimationMixer;

let model: THREE.Group;

let skeleton: THREE.SkeletonHelper;

let walkAction: THREE.AnimationAction;

export async function initTutorialCharacterRigging(
  projectSettings: ProjectSettings,
  pCamera: THREE.PerspectiveCamera,
) {
  const { scene3d } = projectSettings;

  pCamera.position.set(1, 2, - 3);
  pCamera.lookAt(0, 1, 0);

  const ground = new THREE.Mesh(
    new THREE.PlaneGeometry(50, 50),
    new THREE.MeshPhongMaterial({
      color: 0xcbcbcb,
      depthWrite: false,
      map: new THREE.TextureLoader().load('../assets/projects/test/testimage3d.jpg'),
    }),
  );
  ground.rotation.x = -Math.PI / 2;
  ground.receiveShadow = true;
  scene3d.scene.add(ground);

  const gltf = await scene3d.load.gltf('../assets/projects/halleschestor/tutorial-character-rigging-2.glb');
  console.log(gltf);

  model = gltf.scene;
  scene3d.scene.add(model);

  model.traverse((object3d) => {
    if (object3d.type === 'Object3D' || object3d.type === 'SkinnedMesh') {
      object3d.castShadow = true;
    }
  });

  skeleton = new THREE.SkeletonHelper(model);
  skeleton.visible = false;
  scene3d.scene.add(skeleton);

  const animations = gltf.animations;

  mixer = new THREE.AnimationMixer(model);

  walkAction = mixer.clipAction(animations[0]);
  walkAction.play();
}

export function updateTutorialCharacterRigging(time: number, delta: number) {
  mixer.update(delta);
}
