# Hallesches Tor

Mensen met skeleton en bones, lopend, animatie.

## Tutorials

* Free and easy character rigging in Blender
  * https://www.youtube.com/watch?v=wCmwGymncEc
    * BlenderKit add-on.
    * Mixamo, https://www.mixamo.com/ (inloggen met Adobe account)

## Video's van Hallesches Tor

### Veel mensen met tassen

Berlijn Hallesches Tor 2023-09-18 6174.mov\
1920 x 1080 px\
1:24

```bash
# video naar png sequence
ffmpeg -i 'Berlijn Hallesches Tor 2023-09-18 6174.mov' '/Volumes/Samsung_X5/halleschestor_6174/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i 'Berlijn Hallesches Tor 2023-09-18 6174.mov' -vf scale=480:270 halleschestor_6174_preview.mov
# convert preview to png sequence
ffmpeg -i halleschestor_6174_preview.mov '/Volumes/Samsung_X5/halleschestor_6174/frames_preview/frame_%05d.png'
```

## Render scene 1

```bash
# png to mp4 (vanaf index 1 met 30 FPS)
ffmpeg -framerate 30 -start_number 1 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p halleschestor-scene-1-testrender.mp4
```

De eerste test render speelt veel te snel af. De reden is dat ik in scene 1 de lengte van
patternDuration gebruik, en die was bij het maken nog 40 maten van 108 BPM.<br/>
Dat is (60 / 108) * 4 * 80 = 177.77777777777777 seconden.


## Mixamo

Standing idle, Idle, Breathing idle.


### Multiple Mixamo animations

Describing how to take several mixamo animations and add them all to one character in blender. Great for exporting and using in game engines or just animating in general.

https://www.youtube.com/shorts/y1er4qFQlCw


### Test met meerdere animaties

1. Maak een mensfiguur.
2. Pas daar alle animaties op toe.
3. Verander die mens in een duif.

Maar omdat die mens en die duif hetzelfde skelet hebben:

1. Pas een animatie op de mens toe in Mixamo.
2. Maak de animatie los van de mens in Blender.
3. Voeg de animatie toe op de duif.

Non Linear Animation tracks importeren werkt niet.

* Export met NLA tracks naar GLTF gaat goed, alles verschijnt in ThreeJS.
* Import van dezelfde GLTF in Blender niet, slechts 1 animatie verschijnt.
* Import van FBX ook niet.

Ik zou het kunnen oplossen door de NLA in de definitieve Blender samen te stellen. Geen import nodig.

* Person6ToPigeon3 is duif-achtig, maar met een mens-houding. Die exporteren als FBX.
* Person6ToPigeon3 importeren in Mixamo.
* Alle gewenste animaties exporteren als FXB.
* Alle animaties importeren in Blender.
  * Person6ToPigeon3Walking.fbx
  * Person6ToPigeon3SambaDancing.fbx
  * Person6ToPigeon3HipHopDancing.fbx
  * Person6ToPigeon3WeightShift.fbx
  * Person6ToPigeon3WalkingWithShoppingBag.fbx
* Gooi die laatste Armature niet weg.
* Hernoem de Armature naar Person6ToPigeon3Armature.
* Alle animaties als NLA tracks in nieuwe Person6ToPigeon3Armature.
* Person6ToPigeon3Armature dupliceren als Person6ToPigeon4Armature.
  * Deze twee hebben hetzelfde skelet.
  * Maar de animaties moeten een andere naam krijgen, anders zijn er twee met dezelfde naam.
  * Is het mogelijk een later gemaakt animatie op allebei toe te passen?
    * Ja.
  * Person6ToPigeon4Armature veranderen in een duif, maar met armen en witte schoenen.
* Person6ToPigeon4Armature dupliceren als Person6ToPigeon5Armature.
  * De animaties een andere naam geven.
  * Veranderen in een volledige duif.
  * Volledige duif textuur geven.



### Later extra animaties toevoegen

* Wat gebeurt bij importeren in Mixamo van een FBX die al een skelet en animatie heeft?
  * Het skelet wordt herkend. Ik hoef geen punten aan te geven, er is geen wachttijd.
  * De animatie is verdwenen.