import { THREE } from "enable3d";
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader.js";
import { ProjectSettings } from "@app/interfaces";
import { AnimatedModel } from "./animated-model";
import { createImageTexture } from "@app/utils";

const models: AnimatedModel[] = [];

export async function initPeople(projectSettings: ProjectSettings, gltf: GLTF) {
  const texturePersons = createImageTexture("../assets/projects/halleschestor/person-textures.jpg");

  models.push(
    new AnimatedModel(
      projectSettings,
      gltf,
      "Person1WalkingArmature",
      texturePersons,
      0,
      new THREE.Vector3(7, 0, 0),
      new THREE.Vector3(0, 0, 1).normalize(),
      0.075,
    )
  );
  models.push(
    new AnimatedModel(
      projectSettings,
      gltf,
      "Person2WalkingArmature",
      texturePersons,
      1,
      new THREE.Vector3(4, 0, -5),
      new THREE.Vector3(0, 0, 1).normalize(),
      0.092,
    )
  );
  models.push(
    new AnimatedModel(
      projectSettings,
      gltf,
      "Person3WalkingArmature",
      texturePersons,
      2,
      new THREE.Vector3(6, 0, -8),
      new THREE.Vector3(0, 0, 1).normalize(),
      0.09,
    )
  );
  models.push(
    new AnimatedModel(
      projectSettings,
      gltf,
      "Person4WalkingArmature",
      texturePersons,
      3,
      new THREE.Vector3(8, 0, 20),
      new THREE.Vector3(0, 0, -1).normalize(),
      0.07,
    )
  );
}

export async function updatePeople(delta: number) {
  models.forEach((m) => m.update(delta));
}
