/* eslint-disable object-curly-newline */
import { Scene3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import MainScene from '@app/mainscene';
import createTimeline, { Timeline } from '@app/timeline';
import Car from './car';
import { initBackground, updateBackground } from './background';
import { initRoad, updateRoad } from './road';
import createCarMarisol from './car-marisol';
import { initCamera, updateCamera } from './camera';

const PROJECT_PREVIEW_SCALE = 0.25;
const BPM = 106;
const SECONDS_PER_BEAT = 60 / BPM;
const MEASURES = 16 * 3;
const BEATS_PER_MEASURE = 4;
const STEPS_PER_BEAT = 4;
const STEPS = STEPS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const PATTERN_DURATION = SECONDS_PER_BEAT * BEATS_PER_MEASURE * MEASURES;
const STEP_DURATION = PATTERN_DURATION / STEPS;

// eslint-disable-next-line no-console
console.log('PATTERN_DURATION', PATTERN_DURATION);
// eslint-disable-next-line no-console
console.log('STEP_DURATION', STEP_DURATION);

export default class Scene extends MainScene {
  timeline: Timeline;

  width3d: number;

  height3d: number;

  captureCanvas: HTMLCanvasElement;

  captureCanvasContext: CanvasRenderingContext2D;

  dirLightTarget = new THREE.Object3D();

  car: Car | undefined;

  constructor() {
    super();

    this.width = 1920;
    this.canvasHeight = 1750;
    this.viewportHeight = 1080;
    this.width3d = 16;
    this.height3d = (this.viewportHeight / this.width) * this.width3d;
    this.fps = 30;
    this.captureFps = 30;
    this.captureThrottle = 15; // wait 15 raf ticks
    this.captureDuration = PATTERN_DURATION * 2;
    this.clearColor = 0x5178a7;
    this.shadowSize = 32;

    this.timeline = createTimeline({
      duration: PATTERN_DURATION,
    });

    this.captureCanvas = document.createElement('canvas');
    this.captureCanvas.width = this.width;
    this.captureCanvas.height = this.viewportHeight;
    this.captureCanvasContext = this.captureCanvas.getContext('2d') as CanvasRenderingContext2D;
  }

  /**
   * Set the html viewport and canvas height. For adjusted horizon height.
   */
  adjustHorizon() {
    const canvasContainer = document.getElementById('canvas-container');
    const canvas = this.renderer.domElement;

    if (canvasContainer && canvas) {
      canvasContainer.style.height =  `${(this.viewportHeight / this.width) * 100}vw`;
      canvas.setAttribute('style', `${(this.canvasHeight / this.viewportHeight) * 100}% !important`);
    }
  }

  async create() {
    await super.create();

    const isPreview = true && !this.scene.userData.isCapture;

    // this.physics.debug?.enable();

    // this.physics.setGravity(0, 0, 0);

    this.renderer.setSize(this.width, this.canvasHeight);

    // DIRECTIONAL LIGHT
    this.directionalLight.intensity = 1.6 * Math.PI;
    this.directionalLight.position.set(10, 20, 10);

    // AMBIENT LIGHT
    this.ambientLight.intensity = 0.5 * Math.PI;

    // CAMERA
    this.pCamera.position.z -= 5;
    this.pCamera.lookAt(this.cameraTarget);
    this.pCamera.updateProjectionMatrix();
    this.orbitControls.target = this.cameraTarget;
    this.orbitControls.update();
    this.orbitControls.saveState();

    // MEDIA
    const mediaVideo = {
      fps: 30,
      height: 1080,
      scale: isPreview ? PROJECT_PREVIEW_SCALE : 1,
      width: 1920,
    };
  
    const media = {
      north: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/piazzavittoria/frames_preview_piazzavittoria_5479_north/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/piazzavittoria_5479_north/frames/&img=frame_#FRAME#.png',
      },
      park: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/piazzavittoria/frames_preview_piazzavittoria_5480_park/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/piazzavittoria_5480_park/frames/&img=frame_#FRAME#.png',
      },
      cruise: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/piazzavittoria/frames_preview_piazzavittoria_5481_cruise/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/piazzavittoria_5481_cruise/frames/&img=frame_#FRAME#.png',
      },
      boulevard: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/piazzavittoria/frames_preview_piazzavittoria_5482_boulevard/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/piazzavittoria_5482_boulevard/frames/&img=frame_#FRAME#.png',
      },
      sea: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/piazzavittoria/frames_preview_piazzavittoria_5483_sea/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/piazzavittoria_5483_sea/frames/&img=frame_#FRAME#.png',
      },
      square: {
        ...mediaVideo,
        imgSrcPath: isPreview
          ? '../assets/projects/piazzavittoria/frames_preview_piazzavittoria_5486_square/frame_#FRAME#.png'
          : 'fs-img?dir=/Volumes/Samsung_X5/piazzavittoria_5486_square/frames/&img=frame_#FRAME#.png',
      },
    };

    // PROJECT SETTINGS
    const projectSettings: ProjectSettings = {
      height: this.width * (9 / 16),
      height3d: this.width3d * (9 / 16),
      isPreview,
      measures: MEASURES,
      patternDuration: PATTERN_DURATION,
      previewScale: PROJECT_PREVIEW_SCALE,
      scene: this.scene,
      scene3d: this,
      stepDuration: STEP_DURATION,
      timeline: this.timeline,
      width: this.width,
      width3d: this.width3d,
    };

    const gltf = await this.load.gltf('../assets/projects/piazzavittoria/piazzavittoria.glb');

    await initBackground(projectSettings, media);
    await initRoad(projectSettings, gltf);
    initCamera(projectSettings, this.pCamera);
    this.car = await createCarMarisol(this as unknown as Scene3D, gltf);

    this.scene.add(this.dirLightTarget);
    this.directionalLight.target = this.dirLightTarget;
  
    this.postCreate();
  }

  async updateAsync(time: number, delta: number) {
    await this.timeline.update(time, delta);
    if (this.car) {
      this.car.update();
      const { x } = this.car.getPosition();
      updateRoad(x);
      updateBackground(x);
      updateCamera(this.car.getPosition(), time);

      this.directionalLight.position.x = x + 20;
      this.directionalLight.target.position.x = x;
    }
    super.updateAsync(time, delta);
  }

  captureImage() {
    this.captureCanvasContext.drawImage(
      this.renderer.domElement, 0, 0, this.width, this.viewportHeight, 0, 0, this.width, this.viewportHeight,
    );
    return this.captureCanvas.toDataURL();
  }
}
