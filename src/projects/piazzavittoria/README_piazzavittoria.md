# Piazza Vittoria

* Car modelling tutorial van Weesperstraat
  * Low Poly Vehicles | Easy Beginner | Blender Tutorial
    * https://www.youtube.com/watch?v=Zkg7Ol2jEjs

* Blender Low poly hands
  * How to make a Low Poly Character - Part 3 - Hands, Legs and Feet
    * https://www.youtube.com/watch?v=3JT7cCz_Yi0
  * Easy & Quick Low Poly Hand Model In Blender
    * https://www.youtube.com/watch?v=f2jGl9F5-Do
  * Tutorial: EASY Hands In Blender | Beginners
    * https://www.youtube.com/watch?v=__H-gAxCDts

```bash
# extract frame 1800 as an image
ffmpeg -i "Video's/Napels, Piazza Vittoria, 2023-05-22 5479.mov" -vf "select=eq(n\,1800)" -vframes 1 "Frame 600/Napels, Piazza Vittoria, 2023-05-22 5479 frame 1800.jpg"
```

## Video's

### De Weg naar het noorden

```bash
# from 0:10 to 2:00
ffmpeg -ss 00:00:10.0 -i 'Napels, Piazza Vittoria, 2023-05-22 5479.mov' -c copy -t 00:01:50.0 'piazzavittoria_5479_north.mov'
# convert to png sequence
ffmpeg -i 'piazzavittoria_5479_north.mov' '/Volumes/Samsung_X5/piazzavittoria_5479_north/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i piazzavittoria_5479_north.mov -vf scale=480:270 piazzavittoria_5479_north_preview.mov
# convert preview to png sequence
ffmpeg -i piazzavittoria_5479_north_preview.mov '/Volumes/Samsung_X5/piazzavittoria_5479_north/frames_preview/frame_%05d.png'
```

### Park

```bash
# from 0:03 to 1:03
ffmpeg -ss 00:00:03.0 -i 'Napels, Piazza Vittoria, 2023-05-22 5480.mov' -c copy -t 00:01:00.0 'piazzavittoria_5480_park.mov'
# convert to png sequence
ffmpeg -i 'piazzavittoria_5480_park.mov' '/Volumes/Samsung_X5/piazzavittoria_5480_park/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i piazzavittoria_5480_park.mov -vf scale=480:270 piazzavittoria_5480_park_preview.mov
# convert preview to png sequence
ffmpeg -i piazzavittoria_5480_park_preview.mov '/Volumes/Samsung_X5/piazzavittoria_5480_park/frames_preview/frame_%05d.png'
```

### Plein

```bash
# from 0:00 to 0:30
ffmpeg -ss 00:00:00.0 -i 'Napels, Piazza Vittoria, 2023-05-22 5486.mov' -c copy -t 00:00:30.0 'piazzavittoria_5486_square.mov'
# convert to png sequence
ffmpeg -i 'piazzavittoria_5486_square.mov' '/Volumes/Samsung_X5/piazzavittoria_5486_square/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i piazzavittoria_5486_square.mov -vf scale=480:270 piazzavittoria_5486_square_preview.mov
# convert preview to png sequence
ffmpeg -i piazzavittoria_5486_square_preview.mov '/Volumes/Samsung_X5/piazzavittoria_5486_square/frames_preview/frame_%05d.png'
```

### Zee met fort en cruiseboot

```bash
# from 0:00 to 1:00
ffmpeg -ss 00:00:00.0 -i 'Napels, Via Partenope, 2023-05-22 5481.mov' -c copy -t 00:01:00.0 'piazzavittoria_5481_cruise.mov'
# convert to png sequence
ffmpeg -i 'piazzavittoria_5481_cruise.mov' '/Volumes/Samsung_X5/piazzavittoria_5481_cruise/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i piazzavittoria_5481_cruise.mov -vf scale=480:270 piazzavittoria_5481_cruise_preview.mov
# convert preview to png sequence
ffmpeg -i piazzavittoria_5481_cruise_preview.mov '/Volumes/Samsung_X5/piazzavittoria_5481_cruise/frames_preview/frame_%05d.png'
```

### Boulevard

```bash
# from 0:03 to 0:33
ffmpeg -ss 00:00:03.0 -i 'Napels, Via Partenope, 2023-05-22 5482.mov' -c copy -t 00:00:30.0 'piazzavittoria_5482_boulevard.mov'
# convert to png sequence
ffmpeg -i 'piazzavittoria_5482_boulevard.mov' '/Volumes/Samsung_X5/piazzavittoria_5482_boulevard/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i piazzavittoria_5482_boulevard.mov -vf scale=480:270 piazzavittoria_5482_boulevard_preview.mov
# convert preview to png sequence
ffmpeg -i piazzavittoria_5482_boulevard_preview.mov '/Volumes/Samsung_X5/piazzavittoria_5482_boulevard/frames_preview/frame_%05d.png'
```

### Zee

```bash
# from 0:00 to 0:38
ffmpeg -ss 00:00:00.0 -i 'Napels, Via Partenope, 2023-05-22 5483.mov' -c copy -t 00:00:38.0 'piazzavittoria_5483_sea.mov'
# convert to png sequence
ffmpeg -i 'piazzavittoria_5483_sea.mov' '/Volumes/Samsung_X5/piazzavittoria_5483_sea/frames/frame_%05d.png'
# scale to 25%, 1920 * 0.25 = 480 (x 270)
ffmpeg -i piazzavittoria_5483_sea.mov -vf scale=480:270 piazzavittoria_5483_sea_preview.mov
# convert preview to png sequence
ffmpeg -i piazzavittoria_5483_sea_preview.mov '/Volumes/Samsung_X5/piazzavittoria_5483_sea/frames_preview/frame_%05d.png'
```

## Render

6520 frames totaal\
frame 6520 / 2 = 3260

```bash
# png to mp4 (from index 3260 met 30 FPS
ffmpeg -framerate 30 -start_number 3260 -i rendered/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p piazzavittoria-video-x1.mp4
# repeat 3 times, 3260 frames, video only
ffmpeg -i piazzavittoria-video-x1.mp4 -filter_complex "loop=loop=2:size=3260:start=0" piazzavittoria-video-x3-extra-frames.mp4
# An error? Static frames at end! Trim after 108.66666666666667 * 3 = 326 seconds.
ffmpeg -i piazzavittoria-video-x3-extra-frames.mp4 -vf trim=duration=326 piazzavittoria-video-x3.mp4
# repeat 3 times, audio
ffmpeg -stream_loop 2 -i piazzavittoria-audio-x1.wav -c copy piazzavittoria-audio-x3.wav
# video en audio samenvoegen
ffmpeg -i piazzavittoria-video-x3.mp4 -i piazzavittoria-audio-x3.wav -vcodec copy piazzavittoria-x3.mp4
# scale to 50%, 960 * 540
ffmpeg -i piazzavittoria-x3.mp4 -vf scale=960:540 piazzavittoria-x3_halfsize.mp4
```

## Muziek

Video duurt 3260 frames.\
Video duurt 3260 / 30 FPS = 108.66666666666667 seconden.\
Video duurt 48 maten van 4 beats = 192 beats.\
Een beat duurt 108.66666666666667 seconden / 192 beats = 0.5659722222222222 seconden.\
Het tempo is 60 / 0.5659722222222222 = 106.0122699386503 BPM\
Dat klopt wel ongeveer met de ingestelde 106 BPM.

## Beschrijving

Piazza Vittoria in Napels.
