import { ExtendedObject3D, Scene3D, THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { addBlueBus, addOrangeCar, addRedCar, addWhiteTaxiCar } from './road-cars';
import Car from './car';

type Tile = {
  obj?: ExtendedObject3D | undefined;
  pos: number;
}

const GROUND_TILE_WIDTH = 20;
const TILES_AHEAD = 2;
const TILES_BACK = 2;
const tiles: Tile[] = [];
const roadObjects: ExtendedObject3D[] = [];
const roadCars: Car[] = [];
let projectSettings: ProjectSettings | undefined;
let gltf: GLTF | undefined;
let tileModel: ExtendedObject3D | undefined;
let wallModel: ExtendedObject3D | undefined;
let streetlightModel: ExtendedObject3D | undefined;
let partsTexture: THREE.Texture | undefined;

/**
 * Add a new tile in front.
 */
async function addRoadTile(tilePos: number) {
  if (!projectSettings) {
    return;
  }

  const { scene3d } = projectSettings;
  const x = tilePos * GROUND_TILE_WIDTH;

  const tile: Tile = {
    obj: undefined,
    pos: tilePos,
  };
  tiles.push(tile);

  let textureFile = 'road.jpg';
  if (tilePos % 4 === 3) {
    textureFile = `road${(Math.floor(tilePos / 4) % 3) + 2}.jpg`;
  }

  const texture = new THREE.TextureLoader().load(`../assets/projects/piazzavittoria/${textureFile}`, () => {
    if (!tileModel) {
      return;
    }

    texture.flipY = false;
    texture.colorSpace = THREE.SRGBColorSpace;
    texture.needsUpdate = true;

    const tileClone = tileModel.clone();
    tileClone.material = new THREE.MeshPhongMaterial({ map: texture });
    tileClone.castShadow = true;
    tileClone.receiveShadow = true;
    tileClone.position.set(x, -2, -5);
    scene3d.scene.add(tileClone);
    scene3d.physics.add.existing(tileClone, {
      collisionFlags: 1, // STATIC
      mass: 0,
      shape: 'mesh',
    });
    // tileClone.body.setVelocity(-1, 0, 0);
    tile.obj = tileClone;

    if (wallModel) {
      const wallClone = wallModel.clone(); 
      wallClone.material = new THREE.MeshPhongMaterial({ map: partsTexture });
      wallClone.castShadow = true;
      wallClone.receiveShadow = true;
      tileClone.add(wallClone);

      if (streetlightModel && tilePos % 3 === 2) {
        const streetlightClone = streetlightModel.clone(); 
        streetlightClone.material = new THREE.MeshPhongMaterial({ color: 0x324331 });
        streetlightClone.castShadow = true;
        streetlightClone.receiveShadow = true;
        streetlightClone.position.set(GROUND_TILE_WIDTH / 2, 1.0, 0.3);
        streetlightClone.scale.set(0.8, 0.8, 0.8);
        wallClone.add(streetlightClone);
      }
    }
  });
}

async function addObjects(tilePos: number) {
  if (!projectSettings) {
    return;
  }

  const { scene3d } = projectSettings;
  const x = tilePos * GROUND_TILE_WIDTH;

  if (tilePos % 9 === 2) {
    const car = await addWhiteTaxiCar(scene3d as unknown as Scene3D, x + (GROUND_TILE_WIDTH / 4), -10.5); // 1);
    roadCars.push(car);
  }

  if (tilePos % 9 === 4) {
    const car = await addRedCar(scene3d as unknown as Scene3D, x + (GROUND_TILE_WIDTH / 4), -7);
    roadCars.push(car);
  }

  if (tilePos % 9 === 6) {
    const car = await addOrangeCar(scene3d as unknown as Scene3D, x + (GROUND_TILE_WIDTH / 4), -10.5);
    roadCars.push(car);
  }

  if (tilePos % 9 === 8) {
    const car = await addBlueBus(scene3d as unknown as Scene3D, x + (GROUND_TILE_WIDTH / 4), -7);
    roadCars.push(car);
  }
}

/**
 * Remove a tile.
 */
function removeRoadTile(tileIndex: number) {
  if (!projectSettings) {
    return;
  }

  const { scene3d } = projectSettings;
  const { obj } = tiles[tileIndex];

  if (obj) {
    scene3d.destroy(obj);
  }

  tiles.splice(tileIndex, 1);
}

/**
 * Remove a tile.
 */
function removeObjects() {
  if (!projectSettings) {
    return;
  }

  const { scene3d } = projectSettings;

  for(let i = roadObjects.length - 1; i >= 0; i--) {
    const obj = roadObjects[i];
    if (obj.position.y < -10) {
      scene3d.destroy(obj);
      roadObjects.splice(i, 1);
    }
  }

  for(let i = roadCars.length - 1; i >= 0; i--) {
    const car = roadCars[i];
    if (car.chassis && car.chassis.position.y < -10) {
      scene3d.destroy(car.chassis);
      roadCars.splice(i, 1);
    }
  }
}

/**
 * Init.
 */
export async function initRoad(
  p: ProjectSettings,
  g: GLTF
) {
  projectSettings = p;
  gltf = g;
  tileModel = (gltf.scene.getObjectByName('Ground') as ExtendedObject3D).clone();
  wallModel = (gltf.scene.getObjectByName('Wall') as ExtendedObject3D).clone();
  streetlightModel = (gltf.scene.getObjectByName('Lantarenpaal') as ExtendedObject3D).clone();
  partsTexture = new THREE.TextureLoader().load('../assets/projects/piazzavittoria/road-parts.jpg', (texture: THREE.Texture) => {
    texture.flipY = false;
    texture.colorSpace = THREE.SRGBColorSpace;
    texture.needsUpdate = true;
  });
}

/**
 * Check for updates each clock tick.
 */
export async function updateRoad(carX: number) {
  const currentTilePosition = Math.round(carX / GROUND_TILE_WIDTH);

  for (let i = currentTilePosition, n = currentTilePosition + TILES_AHEAD; i < n; i++) {
    const tileExists = tiles.find((t) => t.pos === i);
    if (!tileExists) {
      addRoadTile(i);
      addObjects(i);
    }
  }

  const lastTilePosition = currentTilePosition - TILES_BACK;
  for (let i = tiles.length - 1, n = 0; i >= n; i--) {
    const tileIsTooFarBack = tiles[i].pos < lastTilePosition;
    if (tileIsTooFarBack) {
      removeRoadTile(i);
      removeObjects();
    }
  }

  roadCars.forEach((car) => car.update());
}
