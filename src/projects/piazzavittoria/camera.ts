import { THREE } from 'enable3d';
import { ProjectSettings } from '@app/interfaces';
import createTween, { Ease } from '@app/tween';

let camera: THREE.PerspectiveCamera;
let patternDuration: number;
let xDist: number = 10;
let yDist: number = 1;
let zDist: number = 10;

export function initCamera(projectSettings: ProjectSettings, cam: THREE.PerspectiveCamera) {
  const { patternDuration: p, timeline } = projectSettings;
  patternDuration = p;
  camera = cam;

  const tweenParams: {
    ease?: Ease,
    onComplete?: () => void | undefined;
    onStart?: () => void | undefined;
  } = {
    ease: 'sineInOut',
    onComplete: () => {},
    onStart: () => {},
  };

  // naar dichtbij
  timeline.add(createTween({ ...tweenParams,
    delay: 3,
    duration: 17,
    onUpdate: (progress: number) => {
      xDist = 10 - (progress * 8);
      zDist = 10 - (progress * 8);
      yDist = 1 - (progress * 1);
    },
  }));

  timeline.add(createTween({ ...tweenParams,
    delay: 26,
    duration: 12,
    onUpdate: (progress: number) => {
      xDist = 2 + (progress * 8);
      zDist = 2 + (progress * 8);
    },
  }));

  // hoog in de lucht
  timeline.add(createTween({ ...tweenParams,
    delay: 40,
    duration: 12,
    onUpdate: (progress: number) => {
      xDist = 10 + (progress * 90);
      zDist = 10 + (progress * 90);
      yDist = progress * 100;
    },
  }));

  // weer terug beneden
  timeline.add(createTween({ ...tweenParams,
    delay: 60,
    duration: 12,
    onUpdate: (progress: number) => {
      xDist = 100 - (progress * 98);
      zDist = 100 - (progress * 98);
      yDist = 100 - (progress * 100);
    },
  }));

  // achter de kademuur
  timeline.add(createTween({ ...tweenParams,
    delay: 80,
    duration: 15,
    onUpdate: (progress: number) => {
      xDist = 2 + (progress * 28);
      zDist = 2 + (progress * 28);
      yDist = progress * -0.75;
    },
  }));

  timeline.add(createTween({ ...tweenParams,
    delay: 98,
    duration: 7,
    onUpdate: (progress: number) => {
      xDist = 30 - (progress * 20);
      zDist = 30 - (progress * 20);
      yDist = -0.75 + (progress * 1.75);
    },
  }));
}

export function updateCamera(carPosition: THREE.Vector3, time: number) {
  const { x, y, z } = carPosition;
  const ratio = (((time + 4) % patternDuration) / patternDuration) * Math.PI * 2 * 3;

  const lookat = carPosition.clone();
  lookat.y += 0.75;

  const camX = Math.cos(ratio) * xDist;
  const camY = 1.5 + yDist;
  const camZ = Math.sin(ratio) * zDist;
  camera.position.x = x + camX; // 6;
  camera.position.y = y + camY; // 1.5;
  camera.position.z = z + camZ; // 3;
  camera.lookAt(lookat);
}
