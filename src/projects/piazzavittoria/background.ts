import { THREE } from 'enable3d';
import { ProjectSettings, VideoData } from '@app/interfaces';
import { Actor, createActor } from '@app/actor';
import { getMatrix4 } from '@app/utils';

let group: THREE.Group;

async function createBackground(
  projectSettings: ProjectSettings,
  video: VideoData,
  isVideo: boolean,
  delay: number,
  loops: number,
  imgSrc: string,
  position: THREE.Matrix4,
  rotationY: number,
  isNorth: boolean = false,
) {
  const { stepDuration, width, width3d } = projectSettings;
  const svgScale = width3d / width;

  const mediaData = isVideo ? video : {
    width: 1920,
    height: 1080,
    imgSrc,
  };

  const actor = await createActor(projectSettings, mediaData, {
    svg: { scale: svgScale, url: '../assets/projects/piazzavittoria/sea-sky.svg' },
    imageRect: { w: 1920, h: 1080 },
    depth: 0.05,
    color: 0xa7b9c8,
  });
  
  if (isVideo) {
    if (isNorth) {
      createNorthLoop(actor, stepDuration, delay);
    } else {
      const duration = 48 / loops; // 48 is totale aantal maten
      for (let i = 0; i < loops; i++) {
        actor.addTween({
          delay: delay + (stepDuration * 16 * (duration * i)),
          duration: stepDuration * 16 * duration,
          videoStart: 0,
          fromImagePosition: new THREE.Vector2(0, 0),
        });
      }
    }
  }

  actor.setStaticPosition(position);
  actor.setStaticImage(0, 0);
  actor.getMesh().rotation.y = rotationY;
  group.add(actor.getMesh());
}

function createNorthLoop(actor: Actor, stepDuration: number, delay: number) {
  actor.addTween({
    delay: delay + (stepDuration * 16 * 0),
    duration: stepDuration * 16 * 16,
    videoStart: 0,
    fromImagePosition: new THREE.Vector2(0, 0),
  });

  actor.addTween({
    delay: delay + (stepDuration * 16 * 16),
    duration: stepDuration * 16 * 16,
    videoStart: 0,
    fromImagePosition: new THREE.Vector2(0, 0),
  });

  actor.addTween({
    delay: delay + (stepDuration * 16 * (32 + 0)),
    duration: stepDuration * 16 * (16 - 5),
    videoStart: stepDuration * 16 * 5,
    fromImagePosition: new THREE.Vector2(0, 0),
  });
}

async function createWallDrawing(projectSettings: ProjectSettings) {
  // width = 20, height = 90, x = -80, y = 46, z = 50
  const { width, width3d } = projectSettings;
  const svgScale = width3d / width;
  const scale = 10;

  const actor = await createActor(projectSettings, {
    width: 256,
    height: 1024,
    imgSrc: '../assets/projects/piazzavittoria/gevel-getekend.jpg',
  }, {
    svg: { scale: svgScale, url: '../assets/projects/piazzavittoria/wall.svg' },
    imageRect: { w: 240, h: 1080 },
    depth: 0.05,
    color: 0xa7b9c8,
  });
  actor.setStaticPosition(getMatrix4({ x: -80, y: 46, z: 50, sx: scale, sy: scale, sz: scale }));
  actor.getMesh().rotation.y = (Math.PI * 0.5);
  actor.setStaticImage(0, 0);
  group.add(actor.getMesh());
}

export async function initBackground(projectSettings: ProjectSettings, media: { [key: string]: VideoData },) {
  const { scene3d } = projectSettings;
  const isVideo = true;

  group = new THREE.Group();
  scene3d.scene.add(group);

  // SEA
  await createBackground(
    projectSettings,
    media.sea,
    true,
    2,
    4,
    '../assets/projects/piazzavittoria/Frame 600/Napels, Via Partenope, 2023-05-22 5483 2.jpg',
    getMatrix4({ x: 0, y: 70, z: -60, sx: 16, sy: 16, sz: 16 }),
    0,
  );
  
  // NORTH
  await createBackground(
    projectSettings,
    media.north,
    isVideo,
    9,
    3,
    '../assets/projects/piazzavittoria/Frame 600/Napels, Piazza Vittoria, 2023-05-22 5479 frame 1800.jpg',
    getMatrix4({ x: 50, y: 21, z: -25, sx: 4.2, sy: 4.2, sz: 4.2 }),
    Math.PI / -2,
    true,
  );
  
  // CRUISE
  await createBackground(
    projectSettings,
    media.cruise,
    isVideo,
    2,
    3,
    '../assets/projects/piazzavittoria/Frame 600/Napels, Via Partenope, 2023-05-22 5481.jpg',
    getMatrix4({ x: -80, y: 46, z: -30, sx: 10, sy: 10, sz: 10 }),
    Math.PI * 0.25,
  );
  
  // BOULEVARD
  await createBackground(
    projectSettings,
    media.boulevard,
    isVideo,
    2,
    4,
    '../assets/projects/piazzavittoria/Frame 600/Napels, Via Partenope, 2023-05-22 5482 2.jpg',
    getMatrix4({ x: -80, y: 46, z: 30, sx: 10, sy: 10, sz: 10 }),
    Math.PI * 0.5,
  );
  
  // PARK
  await createBackground(
    projectSettings,
    media.park,
    isVideo,
    2,
    3,
    '../assets/projects/piazzavittoria/Frame 600/Napels, Piazza Vittoria, 2023-05-22 5480.jpg',
    getMatrix4({ x: 60, y: 25, z: 50, sx: 4, sy: 4, sz: 4 }),
    Math.PI,
  );
  
  // SQUARE
  await createBackground(
    projectSettings,
    media.square,
    isVideo,
    2,
    4,
    '../assets/projects/piazzavittoria/Frame 600/Napels, Piazza Vittoria, 2023-05-22 5486.jpg',
    getMatrix4({ x: 0, y: 35, z: 50, sx: 5, sy: 5, sz: 5 }),
    Math.PI,
  );
  
  await createWallDrawing(projectSettings);
}

export function updateBackground(carX: number) {
  group.position.x = carX;
}
