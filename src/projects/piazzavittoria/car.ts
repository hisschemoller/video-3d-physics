import { ExtendedObject3D, THREE } from 'enable3d';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import Vehicle from './vehicle';

export type PhysicsBox = {
  width: number;
  height: number;
  depth: number;
  x: number;
  y: number;
  z: number;
}

export type CarProps = {
  axisPositionBack: number;
  axisPositionFront: number;
  bodyMass: number;
  bodyTexturePath: string;
  gltf: GLTF | undefined;
  kmhTarget: number;
  physicsBox: PhysicsBox;
  position: THREE.Vector3;
  rotationY: number;
  scale?: number;
  wheelAxisHeight: number;
  wheelDiameter: number;
  wheelHalfTrack: number;
  wheelScale?: number;
  wheelTexturePath: string;
};

function createWheel(gltf: GLTF, texturePath: string) {
  const wheel = (gltf.scene.getObjectByName('Wheel') as ExtendedObject3D).clone();
  const wheelTexture = new THREE.TextureLoader().load(texturePath, () => {
    wheelTexture.flipY = false;
    wheelTexture.colorSpace = THREE.SRGBColorSpace;
    wheelTexture.needsUpdate = true;
  });
  wheel.material = new THREE.MeshPhongMaterial({
    map: wheelTexture,
    shininess: 0,
  });
  wheel.receiveShadow = true;
  wheel.castShadow = true;
  wheel.geometry.center();
  return wheel;
}

// function createPart(
//   modelName: string,
//   texture: THREE.Texture | THREE.Texture[],
//   gltf: GLTF,
// ) {
//   const model = (gltf.scene.getObjectByName(modelName) as ExtendedObject3D).clone();
//   if (Array.isArray(texture)) {
//     model.children.forEach((child, index) => {
//       child.material = new THREE.MeshPhongMaterial({ map: texture[index] });
//       child.castShadow = true;
//       child.receiveShadow = true;
//     });
//   } else {
//     model.material = new THREE.MeshPhongMaterial({ map: texture });
//     model.receiveShadow = true;
//     model.castShadow = true;
//   }
//   return model;
// }

export default class Car extends Vehicle {
  async createCar({
    axisPositionBack,
    axisPositionFront,
    bodyMass,
    bodyTexturePath,
    gltf,
    kmhTarget,
    physicsBox,
    position,
    rotationY,
    scale = 1,
    wheelAxisHeight,
    wheelDiameter,
    wheelHalfTrack,
    wheelScale = 1,
    wheelTexturePath,
  }: CarProps) {
    if (!gltf) {
      return;
    }

    this.kmhTarget = kmhTarget;

    const body = (gltf.scene.getObjectByName('Body') as ExtendedObject3D).clone();
    const bodyTexture = new THREE.TextureLoader().load(bodyTexturePath, () => {
      bodyTexture.flipY = false;
      bodyTexture.colorSpace = THREE.SRGBColorSpace;
      bodyTexture.needsUpdate = true;
    });
    body.material = new THREE.MeshPhongMaterial({
      map: bodyTexture,
      shininess: 1,
    });
    body.receiveShadow = true;
    body.castShadow = true;
    body.position.copy(position);
    body.rotation.y = rotationY;
    body.scale.set(scale, scale, scale);
    this.scene3d.scene.add(body);
    this.scene3d.physics.add.existing(body, {
      mass: bodyMass,
      shape: 'convex',
    });

    // WHEEL
    const wScale = wheelScale * scale;
    const wheel = createWheel(gltf, wheelTexturePath);
    wheel.scale.set(wScale, wScale, wScale);

    const wheelRadius = (wheelDiameter / 2) * wScale;
    super.create(body, wheel,
      wheelRadius, wheelRadius, axisPositionBack * scale, axisPositionFront * scale,
      wheelHalfTrack * scale, wheelHalfTrack * scale,
      wheelAxisHeight * scale, wheelAxisHeight * scale);

    setTimeout(() => {
      this.isCreated = true;
    }, 100);
  }

  /**
   * Get car position.
   */
  getPosition() {
    return this.chassis ? this.chassis.position : new THREE.Vector3();
  }
}
