import MainScene from '@app/mainscene';
import setup from '@app/app';
// import scene from '@other/lissajous/scene';
import scene from '@projects/halleschestor/scene';

setup(scene as unknown as MainScene);
