export function render() {
  const html = `
    <div>
      <a href="https://vitejs.dev" target="_blank">
        [VITE LOGO]
      </a>
      <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
        [TYPESCRIPT LOGO]
      </a>
      <h1>Hello Vite!</h1>
      <h2>Dit is src/entry-server.ts</h2>
      <div class="card">
        <button id="counter" type="button"></button>
      </div>
      <p class="read-the-docs">
        Click on the Vite logo to learn more
      </p>
    </div>
  `
  return { html };
}
